import 'apricons/dist/apricons.css'
import '@/assets/styles/app.scss'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import dateFilter from "@/filters/date.filter";
import VueDatePicker from '@vuepic/vue-datepicker';
import '@vuepic/vue-datepicker/dist/main.css';

router.beforeEach(async (to, from, next) => {
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    const token = localStorage['access-token'];
    if ((requiresAuth && (!token || token === 'undefined')) || to.name === 'home') {
        delete localStorage['access-token']
        delete localStorage['refresh-token']
        delete localStorage['login']
        next('/auth');
    } else {
        next();
    }
})

const app = createApp(App)

app.config.globalProperties.$filters = {dateFilter}

app.component('VueDatePicker', VueDatePicker)
app.use(router)
app.use(store)

app.mount('#app')
