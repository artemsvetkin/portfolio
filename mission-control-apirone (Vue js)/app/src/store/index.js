import { createStore } from 'vuex'

export default createStore({
  state: {
    login: localStorage['login'],
    accessToken: localStorage['access-token'],
    refreshToken: localStorage['refresh-token'],
    serviceUrl: process.env.VUE_APP_SERVICE_URL,
    sidebarLink: ''
  },
  getters: {
  },
  mutations: {
    setToken(state, data) {
      state.accessToken = data
    },
    setSidebarLink(state, data) {
      state.sidebarLink = data
    },
  },
  actions: {
  },
  modules: {
  }
})
