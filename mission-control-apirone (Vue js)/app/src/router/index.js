import Callbacks from "@/pages/support/callbacks.vue";
import Operations from "@/pages/support/operations.vue";
import Auth from "@/pages/auth.vue";
import Home from "@/pages/home.vue";
import TransactionsIncoming from "@/pages/founder/transactions/transactions-incoming.vue";
import TransactionsOutgoing from "@/pages/founder/transactions/transactions-outgoing.vue";
import Turnover from "@/pages/founder/transactions/turnover.vue";
import FailedCallbacks from "@/pages/founder/errors/failed-callbacks.vue";
import FrozenForwards from "@/pages/founder/errors/frozen-forwards.vue";
import UnconfirmedTransactions from "@/pages/founder/errors/unconfirmed-transactions.vue";
import Domains from "@/pages/founder/information/domains.vue";
import CallbackAddress from "@/pages/founder/information/callback-address.vue";
import Balances from "@/pages/founder/information/balances.vue";
import Invoices from "@/pages/founder/information/invoices.vue";
import Registrations from "@/pages/founder/information/registrations.vue";

import {createRouter, createWebHistory} from 'vue-router'

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/operations/',
        name: 'operations',
        component: Operations,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/callbacks/',
        name: 'callbacks',
        component: Callbacks,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/auth/',
        name: 'auth',
        component: Auth
    },
    {
        path: '/transactions-incoming/',
        name: 'transactions-incoming',
        component: TransactionsIncoming
    },
    {
        path: '/transactions-outgoing/',
        name: 'transactions-outgoing',
        component: TransactionsOutgoing
    },
    {
        path: '/turnover/',
        name: 'turnover',
        component: Turnover
    },
    {
        path: '/failed-callbacks/',
        name: 'failed-callbacks',
        component: FailedCallbacks
    },
    {
        path: '/frozen-forwards/',
        name: 'frozen-forwards',
        component: FrozenForwards
    },
    {
        path: '/unconfirmed-transactions/',
        name: 'unconfirmed-transactions',
        component: UnconfirmedTransactions
    },
    {
        path: '/domains/',
        name: 'domains',
        component: Domains
    },
    {
        path: '/callback-address/',
        name: 'callback-address',
        component: CallbackAddress
    },
    {
        path: '/balances/',
        name: 'balances',
        component: Balances
    },
    {
        path: '/invoices/',
        name: 'invoices',
        component: Invoices
    },
    {
        path: '/registrations/',
        name: 'registrations',
        component: Registrations
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router

