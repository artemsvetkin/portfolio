export default {
  methods: {
    getExplorerHref(currency, type, value, abbr = '') {
      let explorer = 'blockchair.com';
      let currencyValue = currency;
      if (currency && (currency.toLowerCase().includes('tron') || currency.toLowerCase().includes('trc20'))) {
        currencyValue = '#';
        if (currency.includes('testnet') || abbr.includes('ttrx')) {
          explorer = 'shasta.tronscan.org';
        } else {
          explorer = 'tronscan.org';
        }
      }
      return `https://${explorer}/${currencyValue}/${type}/${value}?from=apirone`;
    },
  },
};
