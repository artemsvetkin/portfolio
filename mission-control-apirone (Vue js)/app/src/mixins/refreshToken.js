import jwtDecode from "jwt-decode";

export default {
    methods: {
        async callRefreshToken() {
            try {
                const response = await fetch(`${this.$store.state.serviceUrl}/api/v2/auth/refresh-token`, {
                    method: 'POST',
                    headers: {
                        "Authorization": `Bearer ${this.$store.state.refreshToken}`,
                        "Content-Type": 'application/json'
                    }
                })
                const result = await response.json()
                localStorage['login'] = this.$store.state.login = result['login']
                localStorage['access-token'] = this.$store.state.accessToken = result['access-token']
                localStorage['refresh-token'] = this.$store.state.refreshToken = result['refresh-token']
                this.$store.commit('setToken', result['access-token'])
                if (response.status === 400) {
                    delete localStorage['access-token']
                    delete localStorage['refresh-token']
                    delete localStorage['login']
                    this.$router.push('/auth')
                }
            } catch (e) {
                console.log(e)
            }
        },
        checkLifetimeToken() {
            const decodeJwt = this.decodeJwtToken(this.$store.state.accessToken)
            return Date.now() <= decodeJwt.exp * 10 ** 3;
        },
        decodeJwtToken(token) {
            return jwtDecode(token);
        }
    }
}