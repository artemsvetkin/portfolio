const { defineConfig } = require('@vue/cli-service');
const webpack = require('webpack');

const publicPath = process.env.VUE_APP_PUBLIC_PATH === '/' ? '/' : `/${process.env.VUE_APP_PUBLIC_PATH}`;

module.exports = defineConfig({
  publicPath,
  transpileDependencies: true,
  configureWebpack: {
    plugins: [
      new webpack.DefinePlugin({
        __VUE_OPTIONS_API__: JSON.stringify(true),
        __VUE_PROD_DEVTOOLS__: JSON.stringify(false),
        __VUE_PROD_HYDRATION_MISMATCH_DETAILS__: JSON.stringify(false),
      }),
    ],
  },
});
