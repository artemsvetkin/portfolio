from flask import Flask
from app.view import IndexPage, WebsiteDesign, CasesPage, AboutUsPage, ServicesPage, ImprintPage, DataProtectionPage, \
    ModalProductVisaCase, ModalQuestionnaireVisaCase, ModalSapeginAutoplatesCase, ModalSapeginKzevbCase, ModalShopCase, \
    ModalUaeCase, TechSupportPage, ProcessAutomationPage, WebshopPage, AdaptiveDesignPage, \
    LandingPage, ModalTtHomesCase, PopupAdvertisementSendEmail, LetsTalkSendEmail, ContactFormSendInTgBot

URLS = {
    '/': (IndexPage, 'index'),
    '/website_design/': (WebsiteDesign, 'website_design'),
    '/tech_support/': (TechSupportPage, 'tech_support'),
    '/process_automation/': (ProcessAutomationPage, 'process_automation'),
    '/webshop/': (WebshopPage, 'webshop'),
    '/adaptive_design/': (AdaptiveDesignPage, 'adaptive_design_page'),
    '/landing_page/': (LandingPage, 'landing_page'),
    '/cases/': (CasesPage, 'cases_page'),
    '/about_us/': (AboutUsPage, 'about_us_page'),
    '/services/': (ServicesPage, 'services_page'),
    '/imprint/': (ImprintPage, 'imprint_page'),
    '/data_protection/': (DataProtectionPage, 'data_protection_page'),
    '/modal_product_visa/': (ModalProductVisaCase, 'modal_product_visa'),
    '/modal_questionnaire_visa/': (ModalQuestionnaireVisaCase, 'modal_questionnaire_visa_case'),
    '/modal_autoplates/': (ModalSapeginAutoplatesCase, 'modal_autoplates'),
    '/modal_kzevb/': (ModalSapeginKzevbCase, 'modal_kzevb'),
    '/modal_shop/': (ModalShopCase, 'modal_shop'),
    '/modal_uae/': (ModalUaeCase, 'modal_uae'),
    '/modal_tthomes/': (ModalTtHomesCase, 'modal_tthomes'),

    '/api/advertisement/': (PopupAdvertisementSendEmail, 'advertisement-send-email'),
    '/api/lets-talk-send-email/': (LetsTalkSendEmail, 'lets-talk-send-email'),
    '/api/tg_bot/': (ContactFormSendInTgBot, 'tg_bot'),
}

def set_app_urls(app: Flask):
    for url, (view, view_name) in URLS.items():
        app.add_url_rule(url, view_func=view.as_view(view_name))
