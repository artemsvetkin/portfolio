$(function () {
    const modalTtHomesSlider1 = $('#modalTtHomesSlider1');
    const modalTtHomesSlider2 = $('#modalTtHomesSlider2');

    function autoplaySlider(slider, config = false) {
        slider.slick({
            infinite: true,
            variableWidth: true,
            pauseOnFocus: false,
            pauseOnHover: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            cssEase: 'linear',
            easing: 'linear',
            speed: 2000,
            autoplay: true,
            autoplaySpeed: 0,
            rtl: config
        });
    }

    autoplaySlider(modalTtHomesSlider1, true);
    autoplaySlider(modalTtHomesSlider2);
});
