$(function () {
    const sidebarAnimation = $('#sidebarAnimation');
    const burger = $('#burger');
    const sidebar = $('#sidebar');
    const header = $("#header");
    const maskSidebar = $('#maskSidebar');
    const sidebarHome = $('#sidebarHome');
    const sidebarServices = $('#sidebarServices');
    const sidebarCases = $('#sidebarCases');
    const sidebarAboutUs = $('#sidebarAboutUs');
    const sidebarBtnContactForm = $('#sidebarBtnContactForm');
    const window_width = $(window).width();
    const headerLinksBlock = $('#headerLinksBlock');

    if (window_width <= 950) {
        headerLinksBlock.remove();
    }

    function animationColorContainerAndOpenSidebar() {
        sidebarAnimation.css({
            width: '100vh',
            opacity: 0.5
        });
        setTimeout(() => {
            header.css({
                'z-index': -1
            });
            burger.addClass('sidebar__burger-open');
        }, 1);
        setTimeout(() => {
            if (window_width <= 550) {
                sidebarAnimation.css({
                    left: 'calc(100% - 240px)',
                    width: '240px',
                    transition: '0.6s ease-out'
                });
            } else {
                sidebarAnimation.css({
                    left: 'calc(100% - 340px)',
                    width: '340px',
                    transition: '0.6s ease-out'
                });
            }
        }, 600);
        setTimeout(() => {
            maskSidebar.fadeIn(200);
            if (window_width <= 550) {
                sidebar.css({
                    opacity: 1,
                    'z-index': 10,
                    width: '240px',
                    transition: '0.4s ease-in'
                });
            } else {
                sidebar.css({
                    opacity: 1,
                    'z-index': 10,
                    width: '340px',
                    transition: '0.4s ease-in'
                });
            }
            sidebar.fadeIn(1000);
            sidebarAnimation.css({
                opacity: 0,
                'z-index': -1
            });
        }, 1150);
        setTimeout(() => {
            sidebarHome.addClass('sidebar__show-link');
            sidebarServices.addClass('sidebar__show-link');
            sidebarCases.addClass('sidebar__show-link');
            sidebarAboutUs.addClass('sidebar__show-link');
            sidebarBtnContactForm.addClass('sidebar__show-btn');
        }, 1250);
    }

    function removeSidebarAndRestartPositionAnimationContainer() {
        burger.removeClass('sidebar__burger-open');
        header.css({
            'z-index': 10
        });
        sidebar.css({
            opacity: 0,
            'z-index': -1,
            transition: '0.3s'
        });
        sidebarAnimation.css({
            left: 0,
            width: 0,
            opacity: 0,
            transition: '0.5s ease-in-out'
        });
        sidebarHome.removeClass('sidebar__show-link');
        sidebarServices.removeClass('sidebar__show-link');
        sidebarCases.removeClass('sidebar__show-link');
        sidebarAboutUs.removeClass('sidebar__show-link');
        sidebarBtnContactForm.removeClass('sidebar__show-btn');
        maskSidebar.fadeOut(300);
    }

    sidebarBtnContactForm.on('click', removeSidebarAndRestartPositionAnimationContainer);


    burger.on('click', () => {
        if (burger.hasClass('sidebar__burger-open')) {
            removeSidebarAndRestartPositionAnimationContainer();
        } else {
            animationColorContainerAndOpenSidebar();
        }
    });

    maskSidebar.on('click', removeSidebarAndRestartPositionAnimationContainer);
});
