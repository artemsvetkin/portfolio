$(function () {
    console.log('kek');
    const window_width = $(window).width();

    function slick() {
        if (window_width <= 716) {
            $('.service-card_instrument__items').slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                variableWidth: true,
                pauseOnFocus: false,
                pauseOnHover: false
            });
        }
    }

    function adaptiveCaptcha() {
        if (window_width <= 420) {
            $('.service-captcha').attr('data-size', 'compact');
        }
    }
    slick();
    adaptiveCaptcha();
});
