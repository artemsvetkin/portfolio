import {post} from './http.js';

$(function () {
    const popupAdvertisement = $('#popupAdvertisement');

    const popupStartContent = $('#popupStartContent');
    const popupFirstFormContainer = $('#popupFirstFormContainer');
    const popupSecondFormContainer = $('#popupSecondFormContainer');

    const popupStartContainerBtn = $('.popup-advertisement__start-btn');
    const advertisementFirstFormContainerBtn = $('#advertisementFirstFormContainerBtn');
    const advertisementSecondFormContainerBtn = $('#advertisementSecondFormContainerBtn');

    const advertisementSpecifyIndustry = $('#advertisementSpecifyIndustry');
    const advertisementSelect = $('#advertisementSelect');
    const advertisementFullName = $('#advertisementFullName');
    const advertisementPhone = $('#advertisementPhone');

    const maskAdvertisement = $('#maskAdvertisement');
    const popupAdvertisementCloseBtn = $('#popupAdvertisementCloseBtn');

    const popupAdvertisementSuccessMessage = $('#popupAdvertisementSuccessMessage');
    const formAdvertisement = $('#formAdvertisement');

    const popupGetStorage = localStorage.getItem('popupGetStorage') === '2';
    if (popupGetStorage) {
        popupAdvertisement.hide();
        maskAdvertisement.hide();
    } else {
        setTimeout(() => {
            popupAdvertisement.fadeIn(300);
            maskAdvertisement.fadeIn(300);
        }, 5000);
    }

    function hidePopup(btn) {
        btn.on('click', () => {
            popupAdvertisement.hide();
            maskAdvertisement.hide();
        });
    }

    hidePopup(maskAdvertisement);
    hidePopup(popupAdvertisementCloseBtn);


    advertisementSelect.on('change', () => {
        advertisementSelect.addClass('popup-advertisement__change-select');
    });

    function toggleContent(btn, containerShow, containerHide, inputs = null) {
        btn.on('click', () => {
            if (inputs === null) {
                containerShow.show();
                containerHide.hide();
            } else {
                if (inputs[0].val() === '' || (inputs[1].val() === null || '')) {
                    btn.text('Füllen Sie alle Felder aus');
                    btn.addClass('popup-advertisement__invalid-btn');
                } else {
                    containerShow.show();
                    containerHide.hide();
                }
            }
        });

    }

    function sendEmail() {
        advertisementSecondFormContainerBtn.on('click', () => {
            if (advertisementFullName.val() === '' || advertisementPhone.val() === '') {
                advertisementSecondFormContainerBtn.text('Füllen Sie alle Felder aus');
                advertisementSecondFormContainerBtn.addClass('popup-advertisement__invalid-btn');
            } else {
                const postData = {
                    advertisementPhone: advertisementPhone.val(),
                    advertisementSpecifyIndustry: advertisementSpecifyIndustry.val(),
                    advertisementSelect: advertisementSelect.val(),
                    advertisementFullName: advertisementFullName.val()
                };
                post('/api/advertisement/', postData);
                formAdvertisement.hide();
                popupAdvertisementSuccessMessage.show();
                localStorage.setItem('popupGetStorage', '2');
                setTimeout(() => {
                    location.reload();
                }, 5000);

            }
        });
    }

    sendEmail();

    function clearInvalidClassInputs(input, btn, text) {
        input.on('click', () => {
            btn.removeClass('popup-advertisement__invalid-btn');
            btn.text(text);
        });
    }

    clearInvalidClassInputs(advertisementSpecifyIndustry, advertisementFirstFormContainerBtn, 'WEITER');
    clearInvalidClassInputs(advertisementSelect, advertisementFirstFormContainerBtn, 'WEITER');
    clearInvalidClassInputs(advertisementFullName, advertisementSecondFormContainerBtn, 'ABSENDEN');
    clearInvalidClassInputs(advertisementPhone, advertisementSecondFormContainerBtn, 'ABSENDEN');

    toggleContent(popupStartContainerBtn, popupFirstFormContainer, popupStartContent);
    toggleContent(advertisementFirstFormContainerBtn, popupSecondFormContainer, popupFirstFormContainer, [advertisementSpecifyIndustry, advertisementSelect]);
});
