$(function () {
    const casesQuestionnaireVisaContent = $('#casesQuestionnaireVisaContent'); // Блок контента сайта визовой анкеты
    const casesShopContent = $('#casesShopContent'); // Блок контента магазина пижам
    const casesProductVisaContent = $('#casesProductVisaContent'); // Блок контента продукт сайта
    const casesUaeContent = $('#casesUaeContent'); // Блок контента сайта виз в Дубаи
    const casesSapeginAutoplatesContent = $('#casesSapeginAutoplatesContent');
    const casesSapeginKzevbContent = $('#casesSapeginKzevbContent');
    const casesTtHomesContent = $('#casesTtHomesContent');

    const casesQuestionnaireVisaLink = $('#casesQuestionnaireVisaLink'); // Кнопка блока с сайтом визовой анкеты
    const casesShopLink = $('#casesShopLink'); // Кнопка блока с сайтом пижам
    const casesProductVisaLink = $('#casesProductVisaLink'); // Кнопка блока с продукт сайтом
    const casesUaeLink = $('#casesUaeLink'); // Кнопка блока сайта виз в Дубаи
    const casesSapeginAutoplatesLink = $('#casesSapeginAutoplatesLink');
    const casesUaeSlider = $('#casesUaeSlider');
    const casesSapeginKzevbLink = $('#casesSapeginKzevbLink');
    const casesTtHomesLink = $('#casesTtHomesLink');

    const window_width = $(window).width();

    /**
     * Инициализирует hover эффект, при наведении меняет цвет фона элемента
     * @param hover {Object} Объект на который идет наведение
     * @param hoverElement {Object} Объект, который меняет свое состояние
     */
    function hoverEffectCasesLink(hover, hoverElement) {
        hover.hover(
            function () {
                hoverElement.css({
                    background: 'rgba(0, 0, 0, 0.2)'
                });
            }, function () {
                hoverElement.css({
                    background: 'rgba(255, 255, 255, 0.2)'
                });
            }
        );
    }

    /**
     * Инициализирует hover эффект, при наведении меняет положение по оси x,y изменяего блока
     * @param hover {Object} Объект на который идет наведение
     * @param hoverElement {Object} Объект, который меняет свое состояние
     * @param rotate {Object} CSS строка после наведения на элемент
     */
    function transformCasesContent(hover, hoverElement, rotate) {
        if (window_width > 767) {
            hover.hover(
                function () {
                    hoverElement.addClass(rotate);
                }, function () {
                    hoverElement.removeClass(rotate);
                }
            );
        }
    }

    function sliderAdaptive(item) {

        if (window_width <= 550) {
            item.slick({
                infinite: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                variableWidth: true,
                pauseOnFocus: false,
                pauseOnHover: false
            });
        }
    }

    function hideRellaxAdaptive(item) {
        if (window_width <= 768) {
            item.removeClass('rellax');
            item.removeAttr('data-rellax-percentage');
            item.removeAttr('data-rellax-speed');
            item.addClass('cases-stop-rellax');
        }
    }

    hideRellaxAdaptive(casesUaeContent);
    hideRellaxAdaptive(casesProductVisaContent);
    hideRellaxAdaptive(casesQuestionnaireVisaContent);
    hideRellaxAdaptive(casesShopContent);
    hideRellaxAdaptive(casesSapeginKzevbContent);
    hideRellaxAdaptive(casesSapeginAutoplatesContent);
    hideRellaxAdaptive(casesTtHomesContent);

    sliderAdaptive(casesUaeSlider);

    transformCasesContent(casesQuestionnaireVisaLink, casesQuestionnaireVisaContent, 'cases-rotate');
    transformCasesContent(casesSapeginKzevbLink, casesSapeginKzevbContent, 'cases-rotate');
    transformCasesContent(casesShopLink, casesShopContent, 'cases-rotate-reverse');
    transformCasesContent(casesSapeginAutoplatesLink, casesSapeginAutoplatesContent, 'cases-rotate-reverse');
    transformCasesContent(casesProductVisaLink, casesProductVisaContent, 'cases-rotate');
    transformCasesContent(casesUaeLink, casesUaeContent, 'cases-rotate-reverse');
    transformCasesContent(casesTtHomesLink, casesTtHomesContent, 'cases-rotate-reverse');

    hoverEffectCasesLink(casesQuestionnaireVisaContent, casesQuestionnaireVisaLink);
    hoverEffectCasesLink(casesUaeContent, casesUaeLink);
    hoverEffectCasesLink(casesSapeginAutoplatesContent, casesSapeginAutoplatesLink);
    hoverEffectCasesLink(casesSapeginKzevbContent, casesSapeginKzevbLink);
});
