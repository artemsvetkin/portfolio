$(function () {
    const casesShopLink = $('#casesShopLink'); // Кнопка блока с сайтом пижам
    const casesUaeLink = $('#casesUaeLink'); // Кнопка блока сайта виз в Дубаи
    const casesProductVisaLink = $('#casesProductVisaLink'); // Кнопка блока Product сайта
    const casesQuestionnaireVisaLink = $('#casesQuestionnaireVisaLink'); // Кнопка блока сайта анкет
    const casesSapeginAutoplatesLink = $('#casesSapeginAutoplatesLink'); // Кнопка блока сайта Сапегина
    const casesSapeginKzevbLink = $('#casesSapeginKzevbLink'); // Кнопка блока сайта Сапегина
    const casesTtHomesLink = $('#casesTtHomesLink'); // Кнопка блока сайта Турков

    const casesCloseBtnContainer = $('#casesCloseBtnContainer');
    const casesCloseBtn = $('#casesCloseBtn');

    const modalContainer = $('#modalContainer');
    const maskModal = $('#maskModal'); // Маска
    const backgroundBLur = $('#backgroundBlur'); // Блюрит экран при открытии модальных окон

    /**
     * Инициализирует событие клик, переключает модальные окна, тень и блю эффект
     * @param link {Object} Кнопка
     * @param urls {String} Модальное окно
     * @param closeBtn {Object} Кнопка закрытия окон
     */
    function toggleModalWindow(link, urls, closeBtn) {
        link.on('click', () => {
            maskModal.fadeIn(300);
            $(document.body).css({'overflow-y': 'hidden'});
            backgroundBLur.addClass('background-blur');
            modalContainer.html(`<iframe class="modal-iframe" src="/${urls}/" frameborder="0"></iframe>`);
            casesCloseBtnContainer.fadeIn(300);
            if (urls === 'modal_questionnaire_visa' || urls === 'modal_shop') {
                casesCloseBtn.addClass('modal-close-black-background');
            } else {
                casesCloseBtn.removeClass('modal-close-black-background');
            }
        });
    }

    function closeModalWindow() {
        $(document.body).css({'overflow-y': 'auto'});
        maskModal.fadeOut(800);
        backgroundBLur.removeClass('background-blur');
        modalContainer.empty();
        casesCloseBtnContainer.fadeOut(10);
    }

    function initializesModalCloseEvents() {
        maskModal.on('click', closeModalWindow);
        casesCloseBtnContainer.on('click', closeModalWindow);
    }

    initializesModalCloseEvents();

    toggleModalWindow(casesShopLink, 'modal_shop');
    toggleModalWindow(casesUaeLink, 'modal_uae');
    toggleModalWindow(casesProductVisaLink, 'modal_product_visa');
    toggleModalWindow(casesQuestionnaireVisaLink, 'modal_questionnaire_visa');
    toggleModalWindow(casesSapeginAutoplatesLink, 'modal_autoplates');
    toggleModalWindow(casesSapeginKzevbLink, 'modal_kzevb');
    toggleModalWindow(casesTtHomesLink, 'modal_tthomes');
});
