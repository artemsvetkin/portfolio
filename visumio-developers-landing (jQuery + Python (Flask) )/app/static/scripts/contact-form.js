import {post} from './http.js';
$(function () {
    const contactForm = $('#contactForm'); // Контакатная форма
    const contactBtn = $('#contactBtn'); // Кнопка контакной формы
    const mask = $('#mask'); // Маска
    const backgroundBlur = $('#backgroundBlur'); // Блюрит экран при открытии модальных окон
    const discussYorProjectLink = $('#discussYorProjectLink');
    const btnContactForm = $('#btnContactForm');
    const contactFormBtn = $('#contactFormBtn');
    const serviceFormBtn = $('#serviceFormBtn');
    const contactCustomCheckbox = $('#contactCustomCheckbox');
    const checkbox = $('#checkbox');
    const serviceCustomCheckbox = $('#serviceCustomCheckbox');
    const serviceCheckbox = $('#serviceCheckbox');
    const contactFormCloseBtn = $('#contactFormCloseBtn');

    const sidebarBtnContactForm = $('#sidebarBtnContactForm');

    const contactFormFullName = $('#contactFormFullName');
    const contactFormEmail = $('#contactFormEmail');
    const contactFormPhone = $('#contactFormPhone');
    const contactFormDescription = $('#contactFormDescription');

    const serviceContactFormFullName = $('#serviceContactFormFullName');
    const serviceContactFormEmail = $('#serviceContactFormEmail');
    const serviceContactFormPhone = $('#serviceContactFormPhone');
    const serviceContactFormDescription = $('#serviceContactFormDescription');
    const window_width = $(window).width();

    if (window_width <= 550) {
        $('.contact-captcha').attr('data-size', 'compact');
    }

    class Validation {
        constructor(fullName, email, phone, description, btn) {
            this.fullName = fullName;
            this.email = email;
            this.phone = phone;
            this.description = description;
            this.btn = btn;
        }

        validate() {
            if (this.fullName.val() !== '' && this.email.val() !== '' &&
                this.phone.val() !== '' && this.description.val() !== '') {
                this.btn.addClass('--success-click');
                this.btn.removeClass('--error-click');
                this.btn.html('<span class="contact-form__success-icon"></span> Ihre Nachricht ist eingegangen.');
            } else {
                this.btn.addClass('--error-click');
                this.btn.removeClass('--success-click');
                this.btn.html('<span class="contact-form__error-icon"></span> Füllen Sie die Lücken aus.');
            }
        }

        defaultContactForm() {
            this.btn.removeClass(['--error-click', '--success-click']);
            this.btn.html('Projekt besprechen');
        }

        clickInput() {
            this.fullName.on('click', () => {
                this.defaultContactForm();
            });
            this.email.on('click', () => {
                this.defaultContactForm();
            });
            this.phone.on('click', () => {
                this.defaultContactForm();
            });
            this.description.on('click', () => {
                this.defaultContactForm();
            });
        }
    }

    const defaultStatusForm = new Validation(contactFormFullName, contactFormEmail, contactFormPhone, contactFormDescription, contactFormBtn);
    defaultStatusForm.clickInput();
    const defaultStatusServiceContactForm = new Validation(serviceContactFormFullName, serviceContactFormEmail, serviceContactFormPhone, serviceContactFormDescription, serviceFormBtn);
    defaultStatusServiceContactForm.clickInput();

    function checkedCheckbox(customCheckbox, checkbox, btn) {
        customCheckbox.on('click', () => {
            if (checkbox.is(':checked')) {
                btn.attr('disabled', true);
                btn.css({
                    cursor: 'not-allowed',
                });
                contactFormBtn.removeClass(['--error-click', '--success-click']);
                contactFormBtn.html('Projekt besprechen');
            } else {
                btn.removeAttr('disabled');
                btn.css({
                    cursor: 'pointer',
                });
            }
        });
    }

    checkedCheckbox(serviceCustomCheckbox, serviceCheckbox, serviceFormBtn);
    checkedCheckbox(contactCustomCheckbox, checkbox, contactFormBtn);

    function customValidateButton(btn, inputFullName, inputEmail, inputPhone, inputDescription) {
        return function () {
            btn.html('<span class="contact-form__loader"></span>Projekt besprechen');
            setTimeout(() => {
                const test = new Validation(inputFullName, inputEmail, inputPhone, inputDescription, btn);
                test.validate();
                if (btn.hasClass('--success-click')) {
                    setTimeout(() => {
                        post('/api/tg_bot/', {
                            fullname: inputFullName.val(),
                            email: inputEmail.val(),
                            phone: inputPhone.val(),
                            about_project: inputDescription.val()
                        });
                        setTimeout(() => {
                            location.reload();
                        }, 2000);
                    }, 500);
                }
            }, 2100);
        };
    }

    function initializesClickButtonEvents() {
        contactFormBtn.on('click', customValidateButton(contactFormBtn, contactFormFullName, contactFormEmail, contactFormPhone, contactFormDescription));
        serviceFormBtn.on('click', customValidateButton(serviceFormBtn, serviceContactFormFullName, serviceContactFormEmail, serviceContactFormPhone, serviceContactFormDescription));
    }

    initializesClickButtonEvents();

    /**
     * Показывает и скрывает контактную форму с маской
     */
    function showContactFormAndMask() {
        contactForm.fadeIn(800);
        mask.fadeIn(400);
        backgroundBlur.addClass('background-blur');
    }

    /**
     * Скрывает контактную форма с маской
     */
    function hideContactFormAndMask() {
        contactFormBtn.removeClass(['--error-click', '--success-click']);
        contactFormBtn.html('Projekt besprechen');
        contactForm.fadeOut(300);
        mask.fadeOut(300);
        backgroundBlur.removeClass('background-blur');
    }

    /**
     * Вешает обработчик событий на кнопку и маску
     */
    function initializesContactFormEvents() {
        contactBtn.on('click', showContactFormAndMask);
        sidebarBtnContactForm.on('click', showContactFormAndMask);
        discussYorProjectLink.on('click', showContactFormAndMask);
        btnContactForm.on('click', showContactFormAndMask);
        mask.on('click', hideContactFormAndMask);
        contactFormCloseBtn.on('click', hideContactFormAndMask);
    }

    initializesContactFormEvents();
});
