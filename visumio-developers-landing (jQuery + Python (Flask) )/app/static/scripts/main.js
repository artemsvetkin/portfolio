$(function () {
    const casesUaeLink = $('#casesUaeLink');
    const casesUaeVisaImage = $('#casesUaeVisaImage');
    const casesProductVisaLink = $('#casesProductVisaLink');
    const casesProductVisaImage = $('#casesProductVisaImage');
    const casesQuestionnaireVisaLink = $('#casesQuestionnaireVisaLink');
    const casesQuestionnaireVisaImage = $('#casesQuestionnaireVisaImage');
    const casesShopLink = $('#casesShopLink');
    const casesShopImage = $('#casesShopImage');
    const casesSapeginAutoplatesLink = $('#casesSapeginAutoplatesLink');
    const casesSapeginAutopltesImage = $('#casesSapeginAutopltesImage');
    const casesSapeginKzevbLink = $('#casesSapeginKzevbLink');
    const casesSapeginKzevbImage = $('#casesSapeginKzevbImage');
    const casesTtHomesLink = $('#casesTtHomesLink');
    const casesTtHomesImage = $('#casesTtHomesImage');

    const window_width = $(window).width();

    const parallax1 = $('#paralax');
    const parallax2 = $('#paralax2');
    const parallax3 = $('#paralax3');
    const aboutUsText = $('#aboutUsText');
    const elementTopParallax1 = parallax1.offset().top;
    const elementTopParallax2 = parallax2.offset().top;
    const elementTopParallax3 = parallax3.offset().top;

    $('#testSlide').slick({
        infinite: true,
        variableWidth: true,
        pauseOnFocus: false,
        pauseOnHover: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        cssEase: 'linear',
        easing: 'linear',
        speed: 1500,
        autoplay: true,
        autoplaySpeed: 0
    });

    function hoverItem(item, targetBlock) {
        item.hover(
            function () {
                targetBlock.addClass('opacity');
            },
            function () {
                targetBlock.removeClass('opacity');
            }
        );
    }

    $('.product__items').slick({
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 4,
        variableWidth: true,
        pauseOnFocus: false,
        pauseOnHover: false,
        cssEase: 'linear',
        dots: true,
        easing: 'linear',
        dotsClass: 'product__dots',
        speed: 800,
        responsive: [
            {
                breakpoint: 1215,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 925,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 550,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    speed: 300
                }
            }
        ]
    });

    hoverItem(casesUaeLink, casesUaeVisaImage);
    hoverItem(casesProductVisaLink, casesProductVisaImage);
    hoverItem(casesQuestionnaireVisaLink, casesQuestionnaireVisaImage);
    hoverItem(casesShopLink, casesShopImage);
    hoverItem(casesSapeginAutoplatesLink, casesSapeginAutopltesImage);
    hoverItem(casesSapeginKzevbLink, casesSapeginKzevbImage);
    hoverItem(casesTtHomesLink, casesTtHomesImage);

    function scrollHideParallaxItemAndTitle(itemScrollPos, item, posTrue, hideTitle = null) {
        if (itemScrollPos <= posTrue) {
            item.css({opacity: 0});
            if (hideTitle !== null) {
                hideTitle.css({opacity: 0});
            }
        } else {
            item.css({opacity: 1});
            if (hideTitle !== null) {
                hideTitle.css({opacity: 1});
            }
        }
    }

    $(window).on('scroll', () => {
        const scrollPos = $(this).scrollTop();
        const scrollPosParallax1 = elementTopParallax1 - scrollPos;
        const scrollPosParallax2 = elementTopParallax2 - scrollPos;
        const scrollPosParallax3 = elementTopParallax3 - scrollPos;

        if (window_width > 550) {
            scrollHideParallaxItemAndTitle(scrollPosParallax1, parallax1, 300);
            scrollHideParallaxItemAndTitle(scrollPosParallax2, parallax2, 300);
            scrollHideParallaxItemAndTitle(scrollPosParallax3, parallax3, 300, aboutUsText);
        } else if (window_width <= 550) {
            scrollHideParallaxItemAndTitle(scrollPosParallax1, parallax1, 0);
            scrollHideParallaxItemAndTitle(scrollPosParallax2, parallax2, 0);
            scrollHideParallaxItemAndTitle(scrollPosParallax3, parallax3, 0, aboutUsText);
        }


    });

});
