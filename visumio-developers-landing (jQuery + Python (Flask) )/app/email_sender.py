import os
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from logging import getLogger
from os.path import basename
from smtplib import SMTP

from jinja2 import Environment, FileSystemLoader

logger = getLogger()


class BaseEmailSender:
    cls_file: str = None

    smtp_host: str = None
    smtp_port: int = None
    smtp_email: str = None
    smtp_login: str = None
    smtp_password: str = None

    def send_email(self, email: str, subject: str, text: str, text_type: str = 'plain',
                   files: list[str] = None):
        message = MIMEMultipart()
        message['From'] = self.smtp_email
        message['To'] = email
        message['Subject'] = subject
        message.attach(MIMEText(text, text_type))

        if files:
            for file_path in files:
                with open(file_path, "rb") as file:
                    part = MIMEApplication(
                        file.read(),
                        Name=basename(file_path)
                    )
                part['Content-Disposition'] = 'attachment; filename="%s"' \
                                              % basename(file_path)
                message.attach(part)

        message_text = message.as_string()
        server = SMTP(self.smtp_host, self.smtp_port)
        server.starttls()
        server.login(self.smtp_login, self.smtp_password)
        server.sendmail(self.smtp_email, email, message_text)

        logger.error(f'Unknown error in RPC', exc_info=True)

        logger.info(f'Email to "{email}" with subject "{subject}" sent')

    def _render_html(self, template_name: str, context: dict) -> str:
        env = Environment(
            loader=FileSystemLoader(
                '%s/templates/' % os.path.dirname(self.cls_file)
            )
        )
        template = env.get_template(template_name)
        return template.render(**context)
