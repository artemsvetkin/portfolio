from flask.views import MethodView
from flask import render_template, url_for, request, redirect, jsonify
from app.sender import EmailSender, email_notification_target
import telegram


class IndexPage(MethodView):
    def get(self):
        return render_template('pages/index.html', header_links='home')


class LetsTalkSendEmail(MethodView):
    def post(self):
        data = request.json
        print(data)
        EmailSender().send_email(
            email=email_notification_target, subject='Message from dev landing',
            text=f"""
                Company Name: {data['name']}
                Email: {data['email']}
                Phone: {data['phone']}
                Description: {data['about_project']}
                """
        )
        return jsonify(data)


class ContactFormSendInTgBot(MethodView):
    async def post(self):
        token_dev = '1171230346:AAEm6jliaVy6RGDbWYsHK66FDyY_XePenCQ'
        token_prod = '6085949474:AAH0wCJhermMtPaEByGTjkc1eJpuAuIgHOc'
        chat_id_dev = '-1001843117435'
        chat_id_prod = '-1001574959367'
        text = request.json
        print(text)
        bot = telegram.Bot(token=token_prod)
        await bot.send_message(chat_id=chat_id_prod,
                               text=f"""
                            <b>🔥🔥🔥НОВАЯ ЗАЯВКА🔥🔥🔥</b> \n\n📌Full Name: <b>{text['fullname']}</b> \n📌Email: <b>{text['email']}</b> \n📌Phone: <b>{text['phone']}</b> \n📌Description: <b>{text['about_project']}</b>
                            """,
                               parse_mode='html')
        return 'message'


class PopupAdvertisementSendEmail(MethodView):
    def post(self):
        data = request.json
        print(data)
        EmailSender().send_email(
            email=email_notification_target, subject='Message from dev landing',
            text=f"""
                Enter your industry here: {data['advertisementSpecifyIndustry']}
                Choose your destination: {data['advertisementSelect']}
                Full Name: {data['advertisementFullName']}
                Phone: {data['advertisementPhone']}
                """
        )
        return redirect(url_for('index'))


class WebsiteDesign(MethodView):
    def get(self):
        return render_template('pages/website-design-page.html')


class TechSupportPage(MethodView):
    def get(self):
        return render_template('pages/tech-support-page.html')


class ProcessAutomationPage(MethodView):
    def get(self):
        return render_template('pages/process-automation-page.html')


class WebshopPage(MethodView):
    def get(self):
        return render_template('pages/webshop-page.html')


class AdaptiveDesignPage(MethodView):
    def get(self):
        return render_template('pages/adaptive-design-page.html')


class LandingPage(MethodView):
    def get(self):
        return render_template('pages/landing-page.html')


class CasesPage(MethodView):
    def get(self):
        return render_template('pages/cases-page.html', header_links='cases')


class AboutUsPage(MethodView):
    def get(self):
        return render_template('pages/about-us-page.html', header_links='about_us')


class ServicesPage(MethodView):
    def get(self):
        return render_template('pages/service-page.html', header_links='services')


class ImprintPage(MethodView):
    def get(self):
        return render_template('pages/imprint-page.html')


class DataProtectionPage(MethodView):
    def get(self):
        return render_template('pages/data-protection-page.html')


class ModalProductVisaCase(MethodView):
    def get(self):
        return render_template('pages/modal-product-visa-case.html')


class ModalQuestionnaireVisaCase(MethodView):
    def get(self):
        return render_template('pages/modal-questionnaire-visa-case.html')


class ModalSapeginAutoplatesCase(MethodView):
    def get(self):
        return render_template('pages/modal-sapegin-autoplates-case.html')


class ModalSapeginKzevbCase(MethodView):
    def get(self):
        return render_template('pages/modal-sapegin-kzevb-case.html')


class ModalShopCase(MethodView):
    def get(self):
        return render_template('pages/modal-shop-case.html')


class ModalUaeCase(MethodView):
    def get(self):
        return render_template('pages/modal-uae-case.html')


class ModalTtHomesCase(MethodView):
    def get(self):
        return render_template('pages/modal-tthomes-case.html')
