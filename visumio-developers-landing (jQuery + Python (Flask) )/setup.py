from setuptools import setup, find_packages

setup(
    name='visumio-landing',
    version='0.0.3',
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'Flask==2.1.1',
    ],
    packages=find_packages(),
)
