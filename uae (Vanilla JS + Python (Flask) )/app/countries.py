COUNTRIES = {
    "AIA": {
        "iso_code": "AI",
        "iso3_code": "AIA",
        "name": "Anguilla",
        "name_deu": "Anguilla",
        "needs_visa_to_emirates": True,
    },
    "ALB": {
        "iso_code": "AL",
        "iso3_code": "ALB",
        "name": "Albania",
        "name_deu": "Albanien",
        "needs_visa_to_emirates": True
    },
    "ARG": {
        "iso_code": "AR",
        "iso3_code": "ARG",
        "name": "Argentina",
        "name_deu": "Argentinien",
        "needs_visa_to_emirates": False
    },
    "ARM": {
        "iso_code": "AM",
        "iso3_code": "ARM",
        "name": "Armenia",
        "name_deu": "Armenien",
        "needs_visa_to_emirates": True,

    },
    "ATG": {
        "iso_code": "AG",
        "iso3_code": "ATG",
        "name": "Antigua and Barbuda",
        "name_deu": "Antigua und Barbuda",
        "needs_visa_to_emirates": True,

    },
    "AUS": {
        "iso_code": "AU",
        "iso3_code": "AUS",
        "name": "Australia",
        "name_deu": "Australien",
        "needs_visa_to_emirates": False
    },
    "AZE": {
        "iso_code": "AZ",
        "iso3_code": "AZE",
        "name": "Azerbaijan",
        "name_deu": "Aserbaidschan",
        "needs_visa_to_emirates": True,

    },
    "BEL": {
        "iso_code": "BE",
        "iso3_code": "BEL",
        "name": "Belgium",
        "name_deu": "Belgien",
        "needs_visa_to_emirates": False
    },
    "BEN": {
        "iso_code": "BJ",
        "iso3_code": "BEN",
        "name": "Benin",
        "name_deu": "Benin",
        "needs_visa_to_emirates": True,

    },
    "BGD": {
        "iso_code": "BD",
        "iso3_code": "BGD",
        "name": "Bangladesh",
        "name_deu": "Bangladesch",
        "needs_visa_to_emirates": True,

    },
    "BHR": {
        "iso_code": "BH",
        "iso3_code": "BHR",
        "name": "Bahrain",
        "name_deu": "Bahrain",
        "needs_visa_to_emirates": True,

    },
    "BLR": {
        "iso_code": "BY",
        "iso3_code": "BLR",
        "name": "Belarus",
        "name_deu": "Belarus",
        "needs_visa_to_emirates": True,

    },
    "BLZ": {
        "iso_code": "BZ",
        "iso3_code": "BLZ",
        "name": "Belize",
        "name_deu": "Belize",
        "needs_visa_to_emirates": True,

    },
    "BMU": {
        "iso_code": "BM",
        "iso3_code": "BMU",
        "name": "Bermuda",
        "name_deu": "Bermuda",
        "needs_visa_to_emirates": True,

    },
    "BRB": {
        "iso_code": "BB",
        "iso3_code": "BRB",
        "name": "Barbados",
        "name_deu": "Barbados",
        "needs_visa_to_emirates": False
    },
    "BTN": {
        "iso_code": "BT",
        "iso3_code": "BTN",
        "name": "Bhutan",
        "name_deu": "Bhutan",
        "needs_visa_to_emirates": True,
    },
    "BWA": {
        "iso_code": "BW",
        "iso3_code": "BWA",
        "name": "Botswana",
        "name_deu": "Botswana",
        "needs_visa_to_emirates": True,
    },
    "CAN": {
        "iso_code": "CA",
        "iso3_code": "CAN",
        "name": "Canada",
        "name_deu": "Kanada",
        "needs_visa_to_emirates": False
    },
    "CHL": {
        "iso_code": "CL",
        "iso3_code": "CHL",
        "name": "Chile",
        "name_deu": "Chile",
        "needs_visa_to_emirates": False
    },
    "CHN": {
        "iso_code": "CN",
        "iso3_code": "CHN",
        "name": "China",
        "name_deu": "China",
        "needs_visa_to_emirates": False
    },
    "CMR": {
        "iso_code": "CM",
        "iso3_code": "CMR",
        "name": "Cameroon",
        "name_deu": "Kamerun",
        "needs_visa_to_emirates": True,
    },
    "COK": {
        "iso_code": "CK",
        "iso3_code": "COK",
        "name": "Cook Islands",
        "name_deu": "Cookinseln",
        "needs_visa_to_emirates": False
    },
    "COL": {
        "iso_code": "CO",
        "iso3_code": "COL",
        "name": "Colombia",
        "name_deu": "Kolumbien",
        "needs_visa_to_emirates": False
    },
    "CUB": {
        "iso_code": "CU",
        "iso3_code": "CUB",
        "name": "Cuba",
        "name_deu": "Kuba",
        "needs_visa_to_emirates": True,
    },
    "CYP": {
        "iso_code": "CY",
        "iso3_code": "CYP",
        "name": "Cyprus",
        "name_deu": "Zypern",
        "needs_visa_to_emirates": False
    },
    "DEU": {
        "iso_code": "DE",
        "iso3_code": "DEU",
        "name": "Germany",
        "name_deu": "Deutschland",
        "needs_visa_to_emirates": False
    },
    "DNK": {
        "iso_code": "DK",
        "iso3_code": "DNK",
        "name": "Denmark",
        "name_deu": "Dänemark",
        "needs_visa_to_emirates": False
    },
    "DZA": {
        "iso_code": "DZ",
        "iso3_code": "DZA",
        "name": "Algeria",
        "name_deu": "Algerien",
        "needs_visa_to_emirates": True
    },
    "ECU": {
        "iso_code": "EC",
        "iso3_code": "ECU",
        "name": "Ecuador",
        "name_deu": "Ecuador",
        "needs_visa_to_emirates": True,
    },
    "EGY": {
        "iso_code": "EG",
        "iso3_code": "EGY",
        "name": "Egypt",
        "name_deu": "Ägypten",
        "needs_visa_to_emirates": True,
    },
    "EST": {
        "iso_code": "EE",
        "iso3_code": "EST",
        "name": "Estonia",
        "name_deu": "Estland",
        "needs_visa_to_emirates": False
    },
    "ETH": {
        "iso_code": "ET",
        "iso3_code": "ETH",
        "name": "Ethiopia",
        "name_deu": "Äthiopien",
        "needs_visa_to_emirates": True,

    },
    "FJI": {
        "iso_code": "FJ",
        "iso3_code": "FJI",
        "name": "Fiji",
        "name_deu": "Fidschi",
        "needs_visa_to_emirates": True,
    },
    "FRA": {
        "iso_code": "FR",
        "iso3_code": "FRA",
        "name": "France",
        "name_deu": "Frankreich",
        "needs_visa_to_emirates": False
    },
    "GAB": {
        "iso_code": "GA",
        "iso3_code": "GAB",
        "name": "Gabon",
        "name_deu": "Gabun",
        "needs_visa_to_emirates": True,
    },
    "GEO": {
        "iso_code": "GE",
        "iso3_code": "GEO",
        "name": "Georgia",
        "name_deu": "Georgien",
        "needs_visa_to_emirates": True,
    },
    "GIB": {
        "iso_code": "GI",
        "iso3_code": "GIB",
        "name": "Gibraltar",
        "name_deu": "Gibraltar",
        "needs_visa_to_emirates": True,
    },
    "GIN": {
        "iso_code": "GN",
        "iso3_code": "GIN",
        "name": "Guinea",
        "name_deu": "Guinea",
        "needs_visa_to_emirates": True,
    },
    "GRC": {
        "iso_code": "GR",
        "iso3_code": "GRC",
        "name": "Greece",
        "name_deu": "Griechenland",
        "needs_visa_to_emirates": False
    },
    "GRD": {
        "iso_code": "GD",
        "iso3_code": "GRD",
        "name": "Grenada",
        "name_deu": "Grenada",
        "needs_visa_to_emirates": True,
    },
    "GRL": {
        "iso_code": "GL",
        "iso3_code": "GRL",
        "name": "Greenland",
        "name_deu": "Grönland",
        "needs_visa_to_emirates": True,
    },
    "HKG": {
        "iso_code": "HK",
        "iso3_code": "HKG",
        "name": "Hong Kong",
        "name_deu": "Hongkong",
        "needs_visa_to_emirates": False
    },
    "HND": {
        "iso_code": "HN",
        "iso3_code": "HND",
        "name": "Honduras",
        "name_deu": "Honduras",
        "needs_visa_to_emirates": False
    },
    "HRV": {
        "iso_code": "HR",
        "iso3_code": "HRV",
        "name": "Croatia",
        "name_deu": "Kroatien",
        "needs_visa_to_emirates": False
    },
    "HTI": {
        "iso_code": "HT",
        "iso3_code": "HTI",
        "name": "Haiti",
        "name_deu": "Haiti",
        "needs_visa_to_emirates": True,
    },
    "IND": {
        "iso_code": "IN",
        "iso3_code": "IND",
        "name": "India",
        "name_deu": "Indien",
        "needs_visa_to_emirates": True,
    },
    "IRL": {
        "iso_code": "IE",
        "iso3_code": "IRL",
        "name": "Ireland",
        "name_deu": "Irland",
        "needs_visa_to_emirates": False
    },
    "IRQ": {
        "iso_code": "IQ",
        "iso3_code": "IRQ",
        "name": "Iraq",
        "name_deu": "Irak",
        "needs_visa_to_emirates": True,
    },
    "ITA": {
        "iso_code": "IT",
        "iso3_code": "ITA",
        "name": "Italy",
        "name_deu": "Italien",
        "needs_visa_to_emirates": False
    },
    "JOR": {
        "iso_code": "JO",
        "iso3_code": "JOR",
        "name": "Jordan",
        "name_deu": "Jordanien",
        "needs_visa_to_emirates": True,
    },
    "KEN": {
        "iso_code": "KE",
        "iso3_code": "KEN",
        "name": "Kenya",
        "name_deu": "Kenia",
        "needs_visa_to_emirates": True,
    },
    "KHM": {
        "iso_code": "KH",
        "iso3_code": "KHM",
        "name": "Cambodia",
        "name_deu": "Kambodscha",
        "needs_visa_to_emirates": True,
    },
    "KIR": {
        "iso_code": "KI",
        "iso3_code": "KIR",
        "name": "Kiribati",
        "name_deu": "Kiribati",
        "needs_visa_to_emirates": False
    },
    "KWT": {
        "iso_code": "KW",
        "iso3_code": "KWT",
        "name": "Kuwait",
        "name_deu": "Kuwait",
        "needs_visa_to_emirates": True,
    },
    "LBR": {
        "iso_code": "LR",
        "iso3_code": "LBR",
        "name": "Liberia",
        "name_deu": "Liberia",
        "needs_visa_to_emirates": True,
    },
    "LCA": {
        "iso_code": "LC",
        "iso3_code": "LCA",
        "name": "Saint Lucia",
        "name_deu": "St. Lucia",
        "needs_visa_to_emirates": True,
    },
    "LSO": {
        "iso_code": "LS",
        "iso3_code": "LSO",
        "name": "Lesotho",
        "name_deu": "Lesotho",
        "needs_visa_to_emirates": True,
    },
    "LTU": {
        "iso_code": "LT",
        "iso3_code": "LTU",
        "name": "Lithuania",
        "name_deu": "Litauen",
        "needs_visa_to_emirates": False
    },
    "LVA": {
        "iso_code": "LV",
        "iso3_code": "LVA",
        "name": "Latvia",
        "name_deu": "Lettland",
        "needs_visa_to_emirates": False
    },
    "MAC": {
        "iso_code": "MO",
        "iso3_code": "MAC",
        "name": "Macao",
        "name_deu": "Macau",
        "needs_visa_to_emirates": False
    },
    "MCO": {
        "iso_code": "MC",
        "iso3_code": "MCO",
        "name": "Monaco",
        "name_deu": "Monaco",
        "needs_visa_to_emirates": False
    },
    "MDG": {
        "iso_code": "MG",
        "iso3_code": "MDG",
        "name": "Madagascar",
        "name_deu": "Madagaskar",
        "needs_visa_to_emirates": True,
    },
    "MEX": {
        "iso_code": "MX",
        "iso3_code": "MEX",
        "name": "Mexico",
        "name_deu": "Mexiko",
        "needs_visa_to_emirates": True,
    },
    "MLI": {
        "iso_code": "ML",
        "iso3_code": "MLI",
        "name": "Mali",
        "name_deu": "Mali",
        "needs_visa_to_emirates": True,
    },
    "MMR": {
        "iso_code": "MM",
        "iso3_code": "MMR",
        "name": "Myanmar",
        "name_deu": "Myanmar (Burma)",
        "needs_visa_to_emirates": True,
    },
    "MOZ": {
        "iso_code": "MZ",
        "iso3_code": "MOZ",
        "name": "Mozambique",
        "name_deu": "Mosambik",
        "needs_visa_to_emirates": True,
    },
    "MRT": {
        "iso_code": "MR",
        "iso3_code": "MRT",
        "name": "Mauritania",
        "name_deu": "Mauretanien",
        "needs_visa_to_emirates": True,
    },
    "MSR": {
        "iso_code": "MS",
        "iso3_code": "MSR",
        "name": "Montserrat",
        "name_deu": "Montserrat",
        "needs_visa_to_emirates": True,
    },
    "MWI": {
        "iso_code": "MW",
        "iso3_code": "MWI",
        "name": "Malawi",
        "name_deu": "Malawi",
        "needs_visa_to_emirates": True,
    },
    "MYS": {
        "iso_code": "MY",
        "iso3_code": "MYS",
        "name": "Malaysia",
        "name_deu": "Malaysia",
        "needs_visa_to_emirates": False
    },
    "NGA": {
        "iso_code": "NG",
        "iso3_code": "NGA",
        "name": "Nigeria",
        "name_deu": "Nigeria",
        "needs_visa_to_emirates": True,
    },
    "CPV": {
        "iso_code": "CV",
        "iso3_code": "CPV",
        "name": "Cabo Verde",
        "name_deu": "Kap Verde",
        "needs_visa_to_emirates": True,
    },
    "GMB": {
        "iso_code": "GM",
        "iso3_code": "GMB",
        "name": "Gambia",
        "name_deu": "Gambia",
        "needs_visa_to_emirates": True,
    },
    "CZE": {
        "iso_code": "CZ",
        "iso3_code": "CZE",
        "name": "Czechia",
        "name_deu": "Tschechien",
        "needs_visa_to_emirates": False
    },
    "COG": {
        "iso_code": "CG",
        "iso3_code": "COG",
        "name": "Congo",
        "name_deu": "Republik Kongo",
        "needs_visa_to_emirates": True,
    },
    "ISR": {
        "iso_code": "IL",
        "iso3_code": "ISR",
        "name": "Israel",
        "name_deu": "Israel",
        "needs_visa_to_emirates": False
    },
    "ABW": {
        "iso_code": "AW",
        "iso3_code": "ABW",
        "name": "Aruba",
        "name_deu": "Aruba",
        "needs_visa_to_emirates": True,

    },
    "AGO": {
        "iso_code": "AO",
        "iso3_code": "AGO",
        "name": "Angola",
        "name_deu": "Angola",
        "needs_visa_to_emirates": True,
    },
    "ARE": {
        "iso_code": "AE",
        "iso3_code": "ARE",
        "name": "United Arab Emirates",
        "name_deu": "Vereinigte Arabische Emirate",
        "needs_visa_to_emirates": False
    },
    "BDI": {
        "iso_code": "BI",
        "iso3_code": "BDI",
        "name": "Burundi",
        "name_deu": "Burundi",
        "needs_visa_to_emirates": True,
    },
    "BGR": {
        "iso_code": "BG",
        "iso3_code": "BGR",
        "name": "Bulgaria",
        "name_deu": "Bulgarien",
        "needs_visa_to_emirates": False
    },
    "BIH": {
        "iso_code": "BA",
        "iso3_code": "BIH",
        "name": "Bosnia and Herzegovina",
        "name_deu": "Bosnien und Herzegowina",
        "needs_visa_to_emirates": True,

    },
    "BRA": {
        "iso_code": "BR",
        "iso3_code": "BRA",
        "name": "Brazil",
        "name_deu": "Brasilien",
        "needs_visa_to_emirates": False
    },
    "KOS": {
        "iso_code": "XK",
        "iso3_code": "KOS",
        "name": "Kosovo",
        "name_deu": "Kosovo",
        "needs_visa_to_emirates": True,
    },
    "CHE": {
        "iso_code": "CH",
        "iso3_code": "CHE",
        "name": "Switzerland",
        "name_deu": "Schweiz",
        "needs_visa_to_emirates": False
    },
    "CIV": {
        "iso_code": "CI",
        "iso3_code": "CIV",
        "name": "Côte d'Ivoire",
        "name_deu": "Elfenbeinküste",
        "needs_visa_to_emirates": True,
    },
    "COM": {
        "iso_code": "KM",
        "iso3_code": "COM",
        "name": "Comoros",
        "name_deu": "Komoren",
        "needs_visa_to_emirates": True,
    },
    "CYM": {
        "iso_code": "KY",
        "iso3_code": "CYM",
        "name": "Cayman Islands",
        "name_deu": "Cayman Islands",
        "needs_visa_to_emirates": True,
    },
    "DJI": {
        "iso_code": "DJ",
        "iso3_code": "DJI",
        "name": "Djibouti",
        "name_deu": "Dschibuti",
        "needs_visa_to_emirates": True,
    },
    "ERI": {
        "iso_code": "ER",
        "iso3_code": "ERI",
        "name": "Eritrea",
        "name_deu": "Eritrea",
        "needs_visa_to_emirates": True,
    },
    "ESP": {
        "iso_code": "ES",
        "iso3_code": "ESP",
        "name": "Spain",
        "name_deu": "Spanien",
        "needs_visa_to_emirates": False
    },
    "FIN": {
        "iso_code": "FI",
        "iso3_code": "FIN",
        "name": "Finland",
        "name_deu": "Finnland",
        "needs_visa_to_emirates": False
    },
    "GBR": {
        "iso_code": "GB",
        "iso3_code": "GBR",
        "name": "United Kingdom",
        "name_deu": "Vereinigtes Königreich",
        "needs_visa_to_emirates": True,
    },
    "GHA": {
        "iso_code": "GH",
        "iso3_code": "GHA",
        "name": "Ghana",
        "name_deu": "Ghana",
        "needs_visa_to_emirates": True,
    },
    "GLP": {
        "iso_code": "GP",
        "iso3_code": "GLP",
        "name": "Guadeloupe",
        "name_deu": "Guadeloupe",
        "needs_visa_to_emirates": True,
    },
    "GTM": {
        "iso_code": "GT",
        "iso3_code": "GTM",
        "name": "Guatemala",
        "name_deu": "Guatemala",
        "needs_visa_to_emirates": True,
    },
    "GUY": {
        "iso_code": "GY",
        "iso3_code": "GUY",
        "name": "Guyana",
        "name_deu": "Guyana",
        "needs_visa_to_emirates": True,
    },
    "IDN": {
        "iso_code": "ID",
        "iso3_code": "IDN",
        "name": "Indonesia",
        "name_deu": "Indonesien",
        "needs_visa_to_emirates": True,
    },
    "JAM": {
        "iso_code": "JM",
        "iso3_code": "JAM",
        "name": "Jamaica",
        "name_deu": "Jamaika",
        "needs_visa_to_emirates": True,
    },
    "JPN": {
        "iso_code": "JP",
        "iso3_code": "JPN",
        "name": "Japan",
        "name_deu": "Japan",
        "needs_visa_to_emirates": False
    },
    "KGZ": {
        "iso_code": "KG",
        "iso3_code": "KGZ",
        "name": "Kyrgyzstan",
        "name_deu": "Kirgistan",
        "needs_visa_to_emirates": True,
    },
    "KNA": {
        "iso_code": "KN",
        "iso3_code": "KNA",
        "name": "Saint Kitts and Nevis",
        "name_deu": "St. Kitts und Nevis",
        "needs_visa_to_emirates": True,
    },
    "LBY": {
        "iso_code": "LY",
        "iso3_code": "LBY",
        "name": "Libya",
        "name_deu": "Libyen",
        "needs_visa_to_emirates": True,
    },
    "LUX": {
        "iso_code": "LU",
        "iso3_code": "LUX",
        "name": "Luxembourg",
        "name_deu": "Luxemburg",
        "needs_visa_to_emirates": False
    },
    "MDV": {
        "iso_code": "MV",
        "iso3_code": "MDV",
        "name": "Maldives",
        "name_deu": "Malediven",
        "needs_visa_to_emirates": False
    },
    "MHL": {
        "iso_code": "MH",
        "iso3_code": "MHL",
        "name": "Marshall Islands",
        "name_deu": "Marshallinseln",
        "needs_visa_to_emirates": False
    },
    "NOR": {
        "iso_code": "NO",
        "iso3_code": "NOR",
        "name": "Norway",
        "name_deu": "Norwegen",
        "needs_visa_to_emirates": False
    },
    "NPL": {
        "iso_code": "NP",
        "iso3_code": "NPL",
        "name": "Nepal",
        "name_deu": "Nepal",
        "needs_visa_to_emirates": True,
    },
    "NRU": {
        "iso_code": "NR",
        "iso3_code": "NRU",
        "name": "Nauru",
        "name_deu": "Nauru",
        "needs_visa_to_emirates": False
    },
    "OMN": {
        "iso_code": "OM",
        "iso3_code": "OMN",
        "name": "Oman",
        "name_deu": "Oman",
        "needs_visa_to_emirates": True,
    },
    "PAK": {
        "iso_code": "PK",
        "iso3_code": "PAK",
        "name": "Pakistan",
        "name_deu": "Pakistan",
        "needs_visa_to_emirates": True,
    },
    "PAN": {
        "iso_code": "PA",
        "iso3_code": "PAN",
        "name": "Panama",
        "name_deu": "Panama",
        "needs_visa_to_emirates": True,
    },
    "PER": {
        "iso_code": "PE",
        "iso3_code": "PER",
        "name": "Peru",
        "name_deu": "Peru",
        "needs_visa_to_emirates": False
    },
    "PNG": {
        "iso_code": "PG",
        "iso3_code": "PNG",
        "name": "Papua New Guinea",
        "name_deu": "Papua-Neuguinea",
        "needs_visa_to_emirates": True,
    },
    "POL": {
        "iso_code": "PL",
        "iso3_code": "POL",
        "name": "Poland",
        "name_deu": "Polen",
        "needs_visa_to_emirates": False
    },
    "PRT": {
        "iso_code": "PT",
        "iso3_code": "PRT",
        "name": "Portugal",
        "name_deu": "Portugal",
        "needs_visa_to_emirates": False
    },
    "QAT": {
        "iso_code": "QA",
        "iso3_code": "QAT",
        "name": "Qatar",
        "name_deu": "Katar",
        "needs_visa_to_emirates": False
    },
    "RWA": {
        "iso_code": "RW",
        "iso3_code": "RWA",
        "name": "Rwanda",
        "name_deu": "Ruanda",
        "needs_visa_to_emirates": True,
    },
    "SEN": {
        "iso_code": "SN",
        "iso3_code": "SEN",
        "name": "Senegal",
        "name_deu": "Senegal",
        "needs_visa_to_emirates": True,
    },
    "SGP": {
        "iso_code": "SG",
        "iso3_code": "SGP",
        "name": "Singapore",
        "name_deu": "Singapur",
        "needs_visa_to_emirates": False
    },
    "SLB": {
        "iso_code": "SB",
        "iso3_code": "SLB",
        "name": "Solomon Islands",
        "name_deu": "Salomonen",
        "needs_visa_to_emirates": False
    },
    "SLE": {
        "iso_code": "SL",
        "iso3_code": "SLE",
        "name": "Sierra Leone",
        "name_deu": "Sierra Leone",
        "needs_visa_to_emirates": True,
    },
    "SLV": {
        "iso_code": "SV",
        "iso3_code": "SLV",
        "name": "El Salvador",
        "name_deu": "El Salvador",
        "needs_visa_to_emirates": False
    },
    "SOM": {
        "iso_code": "SO",
        "iso3_code": "SOM",
        "name": "Somalia",
        "name_deu": "Somalia",
        "needs_visa_to_emirates": True,
    },
    "SSD": {
        "iso_code": "SS",
        "iso3_code": "SSD",
        "name": "South Sudan",
        "name_deu": "Südsudan",
        "needs_visa_to_emirates": True,
    },
    "SUR": {
        "iso_code": "SR",
        "iso3_code": "SUR",
        "name": "Suriname",
        "name_deu": "Suriname",
        "needs_visa_to_emirates": True,
    },
    "SVN": {
        "iso_code": "SI",
        "iso3_code": "SVN",
        "name": "Slovenia",
        "name_deu": "Slowenien",
        "needs_visa_to_emirates": False
    },
    "SWE": {
        "iso_code": "SE",
        "iso3_code": "SWE",
        "name": "Sweden",
        "name_deu": "Schweden",
        "needs_visa_to_emirates": False
    },
    "TCA": {
        "iso_code": "TC",
        "iso3_code": "TCA",
        "name": "Turks and Caicos Islands",
        "name_deu": "Turks- und Caicosinseln",
        "needs_visa_to_emirates": True,
    },
    "TCD": {
        "iso_code": "TD",
        "iso3_code": "TCD",
        "name": "Chad",
        "name_deu": "Tschad",
        "needs_visa_to_emirates": True,
    },
    "TGO": {
        "iso_code": "TG",
        "iso3_code": "TGO",
        "name": "Togo",
        "name_deu": "Togo",
        "needs_visa_to_emirates": True,
    },
    "TON": {
        "iso_code": "TO",
        "iso3_code": "TON",
        "name": "Tonga",
        "name_deu": "Tonga",
        "needs_visa_to_emirates": True,
    },
    "TUN": {
        "iso_code": "TN",
        "iso3_code": "TUN",
        "name": "Tunisia",
        "name_deu": "Tunesien",
        "needs_visa_to_emirates": True,
    },
    "TUR": {
        "iso_code": "TR",
        "iso3_code": "TUR",
        "name": "Turkey",
        "name_deu": "Türkei",
        "needs_visa_to_emirates": True,
    },
    "TUV": {
        "iso_code": "TV",
        "iso3_code": "TUV",
        "name": "Tuvalu",
        "name_deu": "Tuvalu",
        "needs_visa_to_emirates": True,
    },
    "UGA": {
        "iso_code": "UG",
        "iso3_code": "UGA",
        "name": "Uganda",
        "name_deu": "Uganda",
        "needs_visa_to_emirates": True,
    },
    "UZB": {
        "iso_code": "UZ",
        "iso3_code": "UZB",
        "name": "Uzbekistan",
        "name_deu": 'Usbekistan',
        "needs_visa_to_emirates": True,
    },
    "VUT": {
        "iso_code": "VU",
        "iso3_code": "VUT",
        "name": "Vanuatu",
        "name_deu": "Vanuatu",
        "needs_visa_to_emirates": True,
    },
    "WSM": {
        "iso_code": "WS",
        "iso3_code": "WSM",
        "name": "Samoa",
        "name_deu": "Samoa",
        "needs_visa_to_emirates": True,
    },
    "YEM": {
        "iso_code": "YE",
        "iso3_code": "YEM",
        "name": "Yemen",
        "name_deu": "Jemen",
        "needs_visa_to_emirates": True,
    },
    "ZMB": {
        "iso_code": "ZM",
        "iso3_code": "ZMB",
        "name": "Zambia",
        "name_deu": "Sambia",
        "needs_visa_to_emirates": True,
    },
    "BRN": {
        "iso_code": "BN",
        "iso3_code": "BRN",
        "name": "Brunei Darussalam",
        "name_deu": "Brunei",
        "needs_visa_to_emirates": False
    },
    "VNM": {
        "iso_code": "VN",
        "iso3_code": "VNM",
        "name": "Vietnam",
        "name_deu": "Vietnam",
        "needs_visa_to_emirates": True,
    },
    "SRB": {
        "iso_code": "RS",
        "iso3_code": "SRB",
        "name": "Serbia",
        "name_deu": "Serbien",
        "needs_visa_to_emirates": False
    },
    "USA": {
        "iso_code": "US",
        "iso3_code": "USA",
        "name": "United States",
        "name_deu": "Vereinigte Staaten",
        "needs_visa_to_emirates": False
    },
    "MNG": {
        "iso_code": "MN",
        "iso3_code": "MNG",
        "name": "Mongolia",
        "name_deu": "Mongolei",
        "needs_visa_to_emirates": True,
    },
    "MUS": {
        "iso_code": "MU",
        "iso3_code": "MUS",
        "name": "Mauritius",
        "name_deu": "Mauritius",
        "needs_visa_to_emirates": False
    },
    "NAM": {
        "iso_code": "NA",
        "iso3_code": "NAM",
        "name": "Namibia",
        "name_deu": "Namibia",
        "needs_visa_to_emirates": True,
    },
    "NER": {
        "iso_code": "NE",
        "iso3_code": "NER",
        "name": "Niger",
        "name_deu": "Niger",
        "needs_visa_to_emirates": True,
    },
    "NIC": {
        "iso_code": "NI",
        "iso3_code": "NIC",
        "name": "Nicaragua",
        "name_deu": "Nicaragua",
        "needs_visa_to_emirates": True,
    },
    "MKD": {
        "iso_code": "MK",
        "iso3_code": "MKD",
        "name": "Macedonia, Republic of",
        "name_deu": "Mazedonien",
        "needs_visa_to_emirates": True,
    },
    "LAO": {
        "iso_code": "LA",
        "iso3_code": "LAO",
        "name": "Lao People's Democratic Republic",
        "name_deu": "Laos",
        "needs_visa_to_emirates": True,
    },
    "BHS": {
        "iso_code": "BS",
        "iso3_code": "BHS",
        "name": "Bahamas",
        "name_deu": "Bahamas",
        "needs_visa_to_emirates": False
    },
    "NLD": {
        "iso_code": "NL",
        "iso3_code": "NLD",
        "name": "Netherlands",
        "name_deu": "Niederlande",
        "needs_visa_to_emirates": False
    },
    "NZL": {
        "iso_code": "NZ",
        "iso3_code": "NZL",
        "name": "New Zealand",
        "name_deu": "Neuseeland",
        "needs_visa_to_emirates": False
    },
    "SYR": {
        "iso_code": "SY",
        "iso3_code": "SYR",
        "name": "Syria",
        "name_deu": "Syrien",
        "needs_visa_to_emirates": True,
    },
    "BOL": {
        "iso_code": "BO",
        "iso3_code": "BOL",
        "name": "Bolivia",
        "name_deu": "Bolivien",
        "needs_visa_to_emirates": True,

    },
    "PLW": {
        "iso_code": "PW",
        "iso3_code": "PLW",
        "name": "Palau",
        "name_deu": "Palau",
        "needs_visa_to_emirates": True,
    },
    "PRI": {
        "iso_code": "PR",
        "iso3_code": "PRI",
        "name": "Puerto Rico",
        "name_deu": "Puerto Rico",
        "needs_visa_to_emirates": True,
    },
    "ROU": {
        "iso_code": "RO",
        "iso3_code": "ROU",
        "name": "Romania",
        "name_deu": "Rumänien",
        "needs_visa_to_emirates": False
    },
    "AFG": {
        "iso_code": "AF",
        "iso3_code": "AFG",
        "name": "Afghanistan",
        "name_deu": "Afghanistan",
        "needs_visa_to_emirates": True,
    },
    "AND": {
        "iso_code": "AD",
        "iso3_code": "AND",
        "name": "Andorra",
        "name_deu": "Andorra",
        "needs_visa_to_emirates": False
    },
    "AUT": {
        "iso_code": "AT",
        "iso3_code": "AUT",
        "name": "Austria",
        "name_deu": "Österreich",
        "needs_visa_to_emirates": False
    },
    "BFA": {
        "iso_code": "BF",
        "iso3_code": "BFA",
        "name": "Burkina Faso",
        "name_deu": "Burkina Faso",
        "needs_visa_to_emirates": True,
    },
    "CRI": {
        "iso_code": "CR",
        "iso3_code": "CRI",
        "name": "Costa Rica",
        "name_deu": "Costa Rica",
        "needs_visa_to_emirates": False
    },
    "DMA": {
        "iso_code": "DM",
        "iso3_code": "DMA",
        "name": "Dominica",
        "name_deu": "Dominica",
        "needs_visa_to_emirates": True,
    },
    "FRO": {
        "iso_code": "FO",
        "iso3_code": "FRO",
        "name": "Faroe Islands",
        "name_deu": "Färöer Inseln",
        "needs_visa_to_emirates": True,
    },
    "GNB": {
        "iso_code": "GW",
        "iso3_code": "GNB",
        "name": "Guinea-Bissau",
        "name_deu": "Guinea-Bissau",
        "needs_visa_to_emirates": True,
    },
    "GUF": {
        "iso_code": "GF",
        "iso3_code": "GUF",
        "name": "French Guiana",
        "name_deu": "Französisch-Guayana",
        "needs_visa_to_emirates": True,
    },
    "HUN": {
        "iso_code": "HU",
        "iso3_code": "HUN",
        "name": "Hungary",
        "name_deu": "Ungarn",
        "needs_visa_to_emirates": False
    },
    "ISL": {
        "iso_code": "IS",
        "iso3_code": "ISL",
        "name": "Iceland",
        "name_deu": "Island",
        "needs_visa_to_emirates": False
    },
    "KAZ": {
        "iso_code": "KZ",
        "iso3_code": "KAZ",
        "name": "Kazakhstan",
        "name_deu": "Kasachstan",
        "needs_visa_to_emirates": False
    },
    "LBN": {
        "iso_code": "LB",
        "iso3_code": "LBN",
        "name": "Lebanon",
        "name_deu": "Libanon",
        "needs_visa_to_emirates": True,
    },
    "LKA": {
        "iso_code": "LK",
        "iso3_code": "LKA",
        "name": "Sri Lanka",
        "name_deu": "Sri Lanka",
        "needs_visa_to_emirates": True,
    },
    "MAR": {
        "iso_code": "MA",
        "iso3_code": "MAR",
        "name": "Morocco",
        "name_deu": "Marokko",
        "needs_visa_to_emirates": True,
    },
    "MLT": {
        "iso_code": "MT",
        "iso3_code": "MLT",
        "name": "Malta",
        "name_deu": "Malta",
        "needs_visa_to_emirates": False
    },
    "MNE": {
        "iso_code": "ME",
        "iso3_code": "MNE",
        "name": "Montenegro",
        "name_deu": "Montenegro",
        "needs_visa_to_emirates": False
    },
    "KOR": {
        "iso_code": "KR",
        "iso3_code": "KOR",
        "name": "Korea, Republic of",
        "name_deu": "Südkorea",
        "needs_visa_to_emirates": False
    },
    "ASM": {
        "iso_code": "AS",
        "iso3_code": "ASM",
        "name": "American Samoa",
        "name_deu": "Amerikanisch-Samoa",
        "needs_visa_to_emirates": True,
    },
    "CAF": {
        "iso_code": "CF",
        "iso3_code": "CAF",
        "name": "Central African Republic",
        "name_deu": "Zentralafrikanische Republik",
        "needs_visa_to_emirates": True,
    },
    "CUW": {
        "iso_code": "CW",
        "iso3_code": "CUW",
        "name": "Curaçao",
        "name_deu": "Curacao",
        "needs_visa_to_emirates": True,
    },
    "DOM": {
        "iso_code": "DO",
        "iso3_code": "DOM",
        "name": "Dominican Republic",
        "name_deu": "Dominikanische Republik",
        "needs_visa_to_emirates": True,
    },
    "FLK": {
        "iso_code": "FK",
        "iso3_code": "FLK",
        "name": "Falkland Islands (Malvinas)",
        "name_deu": "Falklandinseln",
        "needs_visa_to_emirates": True,
    },
    "GNQ": {
        "iso_code": "GQ",
        "iso3_code": "GNQ",
        "name": "Equatorial Guinea",
        "name_deu": "Äquatorialguinea",
        "needs_visa_to_emirates": True,

    },
    "LIE": {
        "iso_code": "LI",
        "iso3_code": "LIE",
        "name": "Liechtenstein",
        "name_deu": "Liechtenstein",
        "needs_visa_to_emirates": False
    },
    "PHL": {
        "iso_code": "PH",
        "iso3_code": "PHL",
        "name": "Philippines",
        "name_deu": "Philippinen",
        "needs_visa_to_emirates": True,
    },
    "PRY": {
        "iso_code": "PY",
        "iso3_code": "PRY",
        "name": "Paraguay",
        "name_deu": "Paraguay",
        "needs_visa_to_emirates": False
    },
    "SAU": {
        "iso_code": "SA",
        "iso3_code": "SAU",
        "name": "Saudi Arabia",
        "name_deu": "Saudi Arabien",
        "needs_visa_to_emirates": False
    },
    "SVK": {
        "iso_code": "SK",
        "iso3_code": "SVK",
        "name": "Slovakia",
        "name_deu": "Slowakei",
        "needs_visa_to_emirates": False
    },
    "SYC": {
        "iso_code": "SC",
        "iso3_code": "SYC",
        "name": "Seychelles",
        "name_deu": "Seychellen",
        "needs_visa_to_emirates": False
    },
    "TJK": {
        "iso_code": "TJ",
        "iso3_code": "TJK",
        "name": "Tajikistan",
        "name_deu": "Tadschikistan",
        "needs_visa_to_emirates": True,
    },
    "URY": {
        "iso_code": "UY",
        "iso3_code": "URY",
        "name": "Uruguay",
        "name_deu": "Uruguay",
        "needs_visa_to_emirates": False
    },
    "VGB": {
        "iso_code": "VG",
        "iso3_code": "VGB",
        "name": "Virgin Islands, British",
        "name_deu": "Britische Jungferninseln",
        "needs_visa_to_emirates": False
    },
    "ZWE": {
        "iso_code": "ZW",
        "iso3_code": "ZWE",
        "name": "Zimbabwe",
        "name_deu": "Simbabwe",
        "needs_visa_to_emirates": True,
    },
    "RUS": {
        "iso_code": "RU",
        "iso3_code": "RUS",
        "name": "Russia",
        "name_deu": "Russland",
        "needs_visa_to_emirates": False
    },
    "MDA": {
        "iso_code": "MD",
        "iso3_code": "MDA",
        "name": "Moldova, Republic of",
        "name_deu": "Moldawien (Moldau)",
        "needs_visa_to_emirates": True,
    },
    "SDN": {
        "iso_code": "SD",
        "iso3_code": "SDN",
        "name": "Sudan",
        "name_deu": "Sudan",
        "needs_visa_to_emirates": True,
    },
    "SMR": {
        "iso_code": "SM",
        "iso3_code": "SMR",
        "name": "San Marino",
        "name_deu": "San Marino",
        "needs_visa_to_emirates": False
    },
    "THA": {
        "iso_code": "TH",
        "iso3_code": "THA",
        "name": "Thailand",
        "name_deu": "Thailand",
        "needs_visa_to_emirates": True,
    },
    "TKM": {
        "iso_code": "TM",
        "iso3_code": "TKM",
        "name": "Turkmenistan",
        "name_deu": "Turkmenistan",
        "needs_visa_to_emirates": True,
    },
    "TTO": {
        "iso_code": "TT",
        "iso3_code": "TTO",
        "name": "Trinidad and Tobago",
        "name_deu": "Trinidad und Tobago",
        "needs_visa_to_emirates": True,
    },
    "UKR": {
        "iso_code": "UA",
        "iso3_code": "UKR",
        "name": "Ukraine",
        "name_deu": "Ukraine",
        "needs_visa_to_emirates": False
    },
    "VCT": {
        "iso_code": "VC",
        "iso3_code": "VCT",
        "name": "Saint Vincent and the Grenadines",
        "name_deu": "St. Vincent und die Grenadinen",
        "needs_visa_to_emirates": False
    },
    "ZAF": {
        "iso_code": "ZA",
        "iso3_code": "ZAF",
        "name": "South Africa",
        "name_deu": "Südafrika",
        "needs_visa_to_emirates": True,
    },
    "TWN": {
        "iso_code": "TW",
        "iso3_code": "TWN",
        "name": "Taiwan, Province of China",
        "name_deu": "Taiwan",
        "needs_visa_to_emirates": True,
    },
    "VAT": {
        "iso_code": "VA",
        "iso3_code": "VAT",
        "name": "Holy See (Vatican City State)",
        "name_deu": "Vatikanstadt",
        "needs_visa_to_emirates": False
    },
    "STP": {
        "iso_code": "ST",
        "iso3_code": "STP",
        "name": "Sao Tome and Principe",
        "name_deu": "Sao Tome und Principe",
        "needs_visa_to_emirates": True,
    },
    "PSE": {
        "iso_code": "PS",
        "iso3_code": "PSE",
        "name": "Palestine, State of",
        "name_deu": "Palästina",
        "needs_visa_to_emirates": True,
    },
    "VEN": {
        "iso_code": "VE",
        "iso3_code": "VEN",
        "name": "Venezuela",
        "name_deu": "Venezuela",
        "needs_visa_to_emirates": True,
    },
    "COD": {
        "iso_code": "CD",
        "iso3_code": "COD",
        "name": "Congo",
        "name_deu": "Demokratische Republik Kongo",
        "needs_visa_to_emirates": True,
    },
    "IRN": {
        "iso_code": "IR",
        "iso3_code": "IRN",
        "name": "Iran",
        "name_deu": "Iran",
        "needs_visa_to_emirates": True,
    },
    "PYF": {
        "iso_code": "PF",
        "iso3_code": "PYF",
        "name": "French Polynesia",
        "name_deu": "Französisch-Polynesien",
        "needs_visa_to_emirates": True,
    },
    "PRK": {
        "iso_code": "KP",
        "iso3_code": "PRK",
        "name": "Korea, Democratic People's Republic of",
        "name_deu": "Nordkorea",
        "needs_visa_to_emirates": True,
    },
    "TZA": {
        "iso_code": "TZ",
        "iso3_code": "TZA",
        "name": "Tanzania",
        "name_deu": "Tansania",
        "needs_visa_to_emirates": True,
    }
}


