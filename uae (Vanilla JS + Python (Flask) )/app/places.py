sheikh_zayed_grand_mosque = dict(
    url = '/places/sheikh_zayed_grand_mosque/',
    title = 'Sheikh Zayed Grand Mosque',
    text = 'Die Sheikh Zayed Grand Mosqueist beeindruckend, inspirierend und eine der größten Moscheen der Welt. Nur hier können Sie das einzigartige Zusammenspiel zwischen dem Islam und den Kulturen auf der ganzen Welt sehen. Der Gründer der VAE, Sheikh Zayed bin Sultan Al Nahyan, hatte eine ganz besondere Vision für die Moschee: Sie sollte Architekturstile verschiedener muslimischer Zivilisationen integrieren. Es sollte ein Zufluchtsort sein, der von Grund auf einladend und inspirierend ist und die kulturelle Vielfalt feiert. Die Architekten des Gebetshauses waren Briten, Italiener und VAE, die sich von Bauten in der Türkei, Marokko, Pakistan, Ägypten und anderen islamischen Ländern inspirieren ließen. Das Ergebnis ist ein atemberaubendes architektonisches Juwel.',
    image = '/static/images/places/sheikh_zayed_grand_mosque.jpeg'
)

wb_sten_safari_dubai = dict(
    url = '/places/wb_sten_safari_dubai/',
    title = 'Wüstensafari Dubai',
    text = 'Dubais größte Sanddüne beeindruckt nicht nur mit ihrer roten Farbe, sondern bietet auch jede Menge Aktivitäten für Abenteurer. Sie haben die Möglichkeit, Sandboarding auszuprobieren und einen herrlichen Sonnenaufgang über der beeindruckenden Landschaft zu genießen. Anschließend können Sie auf einem Kamel durch die Wüste reiten ooder Sie entscheiden sich für eine Nachtsafari. In diesem Fall können Sie sich in den Dünen austoben und anschließend ein köstliches arabisches Essen in einem traditionellen Beduinenlager genießen.',
    image = '/static/images/places/wastensafari_dubai.jpeg'
)

die_dubai_fountains = dict(
    url = '/places/die_dubai_fountains/',
    title = 'Die Dubai Fountains',
    text = 'Besuchen Sie die Innenstadt von Dubai und bewundern Sie die Wasserspiele des höchsten Springbrunnens der Welt. Die Dubai Fountains faszinieren durch das Zusammenspiel von Wasser, Musik und Licht. Die mächtigen Wasserstrahlen am 120.000 Quadratmeter großen Burj-See neben der Dubai Mall wurden sorgfältig positioniert, um mehr als 83.000 Liter Wasser pro Show bis zu 140 Meter hoch in den Himmel zu schießen.',
    image = '/static/images/places/die_dubai_fountains.jpeg'
)

dubai_mall = dict(
    url = '/places/dubai_mall/',
    title = 'Dubai Mall',
    text = 'Das größte Einkaufszentrum der Welt bietet unzählige Einkaufs- und Unterhaltungsmöglichkeiten. Mit mehr als 1.200 Geschäften, zwei großen Kaufhausketten und Hunderten von Bistros, Cafés und Restaurants umfasst die Dubai Mall mehr als 1 Million Quadratmeter, was 200 Fußballfeldern entspricht. ',
    image = '/static/images/places/dubai_mall.jpeg'
)

dubai_miracle_garden = dict(
    url = '/places/dubai_miracle_garden/',
    title = 'Dubai Miracle Garden',
    text = 'Der größte Garten der Welt, Dubai Miracle Garden, erwartet Sie in einer Welt blumiger Düfte. Der 72.000 Quadratmeter große Park bietet eine beeindruckende Sammlung von Repliken berühmter Gebäude und Kunstwerke, die in farbenfrohe Blumenarrangements umgewandelt wurden. Schlendern Sie den herzförmigen Weg entlang oder erkunden Sie die Blumenschlösser und lebensgroßen Häuser, die nachts beleuchtet werden. Lassen Sie sich von unzähligen Kreationen in einem Meer aus über 50 Millionen Blumen mitreißen. Zahlreiche Attraktionen und verschiedene ganz besondere Arrangements erwarten Sie.',
    image = '/static/images/places/dubai_miracle_garden.jpeg'
)

etihad_towers = dict(
    url = '/places/etihad_towers/',
    title = 'Etihad Towers',
    text = 'Der ikonische Gebäudekomplex an der Abu Dhabi Corniche besteht aus fünf Türmen in einem architektonischen Stil, der Einwohner und Reisende einlädt, an einem Ort zu leben, zu arbeiten, zu schlafen, einzukaufen und zu speisen. Mit drei Wohntürmen, einem Geschäftsturm und dem 280 Meter hohen Fünf-Sterne-Hotel Jumeirah in den Etihad Towers bietet das Gebäude einen atemberaubenden Blick auf die Stadt und den Arabischen Golf und definiert das Wort Luxus völlig neu. Im 74. Stock des zweiten Turms bietet das Observation Deck at 300 eine der besten Aussichten der Stadt. Etihad Towers ist ein großartiges Einkaufsziel und beherbergt einige der luxuriösesten und exklusivsten Boutiquen der Welt. Viele verfügen auch über private Räume, in denen VIPs in absoluter Privatsphäre einkaufen können.',
    image = '/static/images/places/etihad_towers.jpeg'
)

louvre_abu_dhabi = dict(
    url = '/places/louvre_abu_dhabi/',
    title = 'Louvre Abu Dhabi',
    text = 'Der legendäre Louvre Abu Dhabi ist das erste kosmopolitische Museum der arabischen Welt, das Weltoffenheit und Weltoffenheit zwischen verschiedenen Kulturen fördert. Das Museum befindet sich im Herzen des Kulturbezirks Saadiyat auf der Insel Saadiyat und ist eine der führenden kulturellen Institutionen des Landes, die Werke von historischer, kultureller und soziologischer Bedeutung für alle Altersgruppen von der Antike bis zur Neuzeit ausstellt.',
    image = '/static/images/places/louvre_abu_dhabi.jpeg'
)

palm_jumeirah = dict(
    url = '/places/palm_jumeirah/',
    title = 'Palm Jumeirah',
    text = 'Dubai ist weltbekannt für seine bahnbrechenden Projekte, darunter die beeindruckende Palm Jumeirah. Von oben ähnelt die künstliche Insel einer stilisierten Palme. Palm Jumeirah beherbergt die luxuriösesten Hotels in Dubai wie Atlantis The Palm, Jumeirah Zabeel Saray, One&Only The Palm und mehr. Auch wenn Sie nicht in Palme wohnen, sind die Restaurants hier, wie das Nobu oder die 101 Dining Lounge, auf jeden Fall einen Besuch wert.',
    image = '/static/images/places/palm_jumeirah.jpeg'
)

dubai_aquarium_and_underwater_zoo = dict(
    url = '/places/dubai_aquarium_and_underwater_zoo/',
    title = 'Dubai Aquarium & Underwater Zoo',
    text = 'Möchten Sie eines der größten Aquarien der Welt erkunden? Dann sollten Sie das Dubai Aquarium and Underwater Zoo besuchen, das Ihnen mit einem Fassungsvermögen von 10 Millionen Litern und mehr als 33.000 Arten von Meereslebewesen einen Einblick in die faszinierende Unterwasserwelt gibt. Sie können dort auch die weltweit größte Kolonie von Sandtigerhaien sehen. Das Aquarium, das sich bis in die dritte Etage der Dubai Mall erstreckt, beherbergt mehr als 140 Arten von Meereslebewesen und bietet viele Möglichkeiten, die Unterwasserwelt zu erkunden. Schlendern Sie durch den 48 Meter langen Glastunnel, der Sie direkt durch das Aquarium führt. Von dort aus haben Sie einen fantastischen Unterwasserblick und bestaunen Sandtigerhaie und Mantarochen, die von unten über Sie hinweggleiten.',
    image = '/static/images/places/dubai_aquarium_underwater_zoo.jpeg'
)
