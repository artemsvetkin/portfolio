from flask import Flask

from app.view import IndexPage, NeedVisaPage, AllEventsPage, AllPlacePage, VisaInformationPage, \
    FaqPage, PricePage, SuccessEmailPage, \
    EventDpWorldTourPage, EventDowntownDesignPage, EventIntarnationalBoatShowPage, EventEidAlFitrPage, \
    EventElectricVehicleInnovationSummitPage, EventEmiratesAirlineDubaiRugbySevensPage, EventHrTechnologySummitPage, \
    EventInternationalIndianFilmPage, EventAdihexPage, PlaceSheikhZayedGrandMosquePage, PlaceWBStenSafariDubaiPage, \
    PlaceDieDubaiFountainsPage, PlaceDubaiMallPage, PlaceDubaiMiracleGardenPage, PlaceEtihadTowersPage, \
    PlaceLouvreAbuDhabiPage, PlacePalmJumeirahPage, PlaceDubaiAquariumAndUnderwaterZooPage, FalseNeedVisaPage


def set_app_routes(app: Flask):
    app.add_url_rule(
        '/',
        view_func=IndexPage.as_view('index')
    )

    app.add_url_rule(
        '/true_need_visa/',
        view_func=NeedVisaPage.as_view('true_need_visa')
    )

    app.add_url_rule(
        '/event/dp_world_tour/',
        view_func=EventDpWorldTourPage.as_view('event_dp_world_tour')
    )

    app.add_url_rule(
        '/event/downtown_design/',
        view_func=EventDowntownDesignPage.as_view('event_downtown_design_tour')
    )

    app.add_url_rule(
        '/event/international_boat_show/',
        view_func=EventIntarnationalBoatShowPage.as_view('international_boat_show')
    )

    app.add_url_rule(
        '/event/eid_al_fitr/',
        view_func=EventEidAlFitrPage.as_view('eid_al_fitr')
    )

    app.add_url_rule(
        '/event/electric_vehicle_innovation_summit/',
        view_func=EventElectricVehicleInnovationSummitPage.as_view('electric_vehicle_innovation_summit')
    )

    app.add_url_rule(
        '/event/emirates_airline_dubai_rugby_sevens/',
        view_func=EventEmiratesAirlineDubaiRugbySevensPage.as_view('emirates_airline_dubai_rugby_sevens')
    )

    app.add_url_rule(
        '/event/hr_technology_summit/',
        view_func=EventHrTechnologySummitPage.as_view('hr_technology_summit')
    )

    app.add_url_rule(
        '/event/international_indian_film_academy/',
        view_func=EventInternationalIndianFilmPage.as_view('international_indian_film_academy')
    )

    app.add_url_rule(
        '/event/adihex/',
        view_func=EventAdihexPage.as_view('adihex')
    )

    app.add_url_rule(
        '/places/sheikh_zayed_grand_mosque/',
        view_func=PlaceSheikhZayedGrandMosquePage.as_view('sheikh_zayed_grand_mosque')
    )

    app.add_url_rule(
        '/places/wb_sten_safari_dubai/',
        view_func=PlaceWBStenSafariDubaiPage.as_view('wb_sten_safari_dubai')
    )

    app.add_url_rule(
        '/places/die_dubai_fountains/',
        view_func=PlaceDieDubaiFountainsPage.as_view('die_dubai_fountains')
    )

    app.add_url_rule(
        '/places/dubai_mall/',
        view_func=PlaceDubaiMallPage.as_view('dubai_mall')
    )

    app.add_url_rule(
        '/places/dubai_miracle_garden/',
        view_func=PlaceDubaiMiracleGardenPage.as_view('dubai_miracle_garden')
    )

    app.add_url_rule(
        '/places/etihad_towers/',
        view_func=PlaceEtihadTowersPage.as_view('etihad_towers')
    )

    app.add_url_rule(
        '/places/louvre_abu_dhabi/',
        view_func=PlaceLouvreAbuDhabiPage.as_view('louvre_abu_dhabi')
    )

    app.add_url_rule(
        '/places/palm_jumeirah/',
        view_func=PlacePalmJumeirahPage.as_view('palm_jumeirah')
    )

    app.add_url_rule(
        '/places/dubai_aquarium_and_underwater_zoo/',
        view_func=PlaceDubaiAquariumAndUnderwaterZooPage.as_view('dubai_aquarium_and_underwater_zoo')
    )

    app.add_url_rule(
        '/all_events/',
        view_func=AllEventsPage.as_view('all_events')
    )

    app.add_url_rule(
        '/all_place/',
        view_func=AllPlacePage.as_view('all_place')
    )

    app.add_url_rule(
        '/visa_information/',
        view_func=VisaInformationPage.as_view('visa_information')
    )

    app.add_url_rule(
        '/faq/',
        view_func=FaqPage.as_view('faq')
    )

    app.add_url_rule(
        '/price/',
        view_func=PricePage.as_view('price')
    )

    app.add_url_rule(
        '/emailing/',
        view_func=SuccessEmailPage.as_view('emailing')
    )

    app.add_url_rule(
        '/false_need_visa/',
        view_func=FalseNeedVisaPage.as_view('false_need_visa')
    )
