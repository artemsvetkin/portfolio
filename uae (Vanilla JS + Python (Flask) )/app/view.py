from flask import render_template, redirect, url_for, session, request, jsonify
from flask.views import MethodView

from app.events import dp_world_tour, downtown_design, international_boat_show, eid_al_fitr, \
    electric_vehicle_innovation_summit, emirates_airline_dubai_rugby_sevens, hr_technology_summit, \
    international_indian_film_academy, adihex
from app.places import sheikh_zayed_grand_mosque, wb_sten_safari_dubai, die_dubai_fountains, dubai_mall, \
    dubai_miracle_garden, etihad_towers, louvre_abu_dhabi, palm_jumeirah, dubai_aquarium_and_underwater_zoo
from app.countries import COUNTRIES


render_title_index = 'Alles verstanden?'
render_title_faq = 'Alles klar?'
render_title_place = 'Jetzt online Visum für Event bentragen'
render_subtitle_index = 'Dann können Sie jetzt in 5 Minuten Ihr Visum beantragen. Wählen Sie dazu Ihre Staatsangehörigkeit und die gewünsche Bearbeitungsdauer.'
render_subtitle_place = 'In 5 Minuten können Sie Ihr Visum beantragen. Wählen Sie dazu Ihre Staatsangehörigkeit und die gewünsche Bearbeitungsdauer.'
all_clear = 'all-clear'
decorate_container = 'all-clear__decorative-container'


class IndexPage(MethodView):
    def get(self):
        return render_template('index.html', render_title_index=render_title_index, render_subtitle_index=render_subtitle_index, countries=COUNTRIES)

# class SelectCountryView(MethodView):
    def post(self):
        # if 'country' not in request.form:
        #     raise ...
        country = request.form.get('country')
        session['country'] = request.form['country']
        session['persons'] = [
            dict(country=country)
        ]

        for key in COUNTRIES.values():
            if session['country'] == key['iso3_code']:
                if key['needs_visa_to_emirates'] == True:
                    return redirect(url_for('true_need_visa'))
                else:
                    return redirect(url_for('false_need_visa'))

class FalseNeedVisaPage(MethodView):
    def get(self):
        return render_template('dont_need_visa.html')

class NeedVisaPage(MethodView):
    def get(self):
        session.get('country')
        return render_template('need_visa_page.html', render_title_faq=render_title_faq, render_subtitle_index=render_subtitle_index, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES)

class RequestOneFormView(MethodView):
    def post(self):
        btn_pay = request.form.get('btn_pay')
        data = request.json
        persons = session['persons']
        persons[0] = dict(
            **session['persons'][0],
            **data,
        )
        session['persons'] = persons
        print(session)
        return jsonify(dict(ok=True))
        # return redirect(url_for('emailing', btn_pay=btn_pay))

class RequestTwoFormView(MethodView):
    ...

class GeneratePdfView(MethodView):
    ...

class SuccessEmailPage(MethodView):
    def get(self):
        return render_template('emailing.html')

class EventDpWorldTourPage(MethodView):
    def get(self):
        return render_template('event.html', render_title_place=render_title_place, render_subtitle_place=render_subtitle_place, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES, events=dp_world_tour)

class EventDowntownDesignPage(MethodView):
    def get(self):
        return render_template('event.html', render_title_place=render_title_place, render_subtitle_place=render_subtitle_place, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES, events=downtown_design)

class EventIntarnationalBoatShowPage(MethodView):
    def get(self):
        return render_template('event.html', render_title_place=render_title_place, render_subtitle_place=render_subtitle_place, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES, events=international_boat_show)

class EventEidAlFitrPage(MethodView):
    def get(self):
        return render_template('event.html', render_title_place=render_title_place, render_subtitle_place=render_subtitle_place, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES, events=eid_al_fitr)

class EventElectricVehicleInnovationSummitPage(MethodView):
    def get(self):
        return render_template('event.html', render_title_place=render_title_place, render_subtitle_place=render_subtitle_place, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES, events=electric_vehicle_innovation_summit)

class EventEmiratesAirlineDubaiRugbySevensPage(MethodView):
    def get(self):
        return render_template('event.html', render_title_place=render_title_place, render_subtitle_place=render_subtitle_place, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES, events=emirates_airline_dubai_rugby_sevens)

class EventHrTechnologySummitPage(MethodView):
    def get(self):
        return render_template('event.html', render_title_place=render_title_place, render_subtitle_place=render_subtitle_place, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES, events=hr_technology_summit)

class EventInternationalIndianFilmPage(MethodView):
    def get(self):
        return render_template('event.html', render_title_place=render_title_place, render_subtitle_place=render_subtitle_place, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES, events=international_indian_film_academy)

class EventAdihexPage(MethodView):
    def get(self):
        return render_template('event.html', render_title_place=render_title_place, render_subtitle_place=render_subtitle_place, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES, events=adihex)

class PlaceSheikhZayedGrandMosquePage(MethodView):
    def get(self):
        return render_template('place.html', render_title_place=render_title_place, render_subtitle_place=render_subtitle_place, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES, events=sheikh_zayed_grand_mosque)

class PlaceWBStenSafariDubaiPage(MethodView):
    def get(self):
        return render_template('place.html', render_title_place=render_title_place, render_subtitle_place=render_subtitle_place, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES, events=wb_sten_safari_dubai)

class PlaceDieDubaiFountainsPage(MethodView):
    def get(self):
        return render_template('place.html', render_title_place=render_title_place, render_subtitle_place=render_subtitle_place, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES, events=die_dubai_fountains)

class PlaceDubaiMallPage(MethodView):
    def get(self):
        return render_template('place.html', render_title_place=render_title_place, render_subtitle_place=render_subtitle_place, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES, events=dubai_mall)

class PlaceDubaiMiracleGardenPage(MethodView):
    def get(self):
        return render_template('place.html', render_title_place=render_title_place, render_subtitle_place=render_subtitle_place, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES, events=dubai_miracle_garden)

class PlaceEtihadTowersPage(MethodView):
    def get(self):
        return render_template('place.html', render_title_place=render_title_place, render_subtitle_place=render_subtitle_place, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES, events=etihad_towers)

class PlaceLouvreAbuDhabiPage(MethodView):
    def get(self):
        return render_template('place.html', render_title_place=render_title_place, render_subtitle_place=render_subtitle_place, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES, events=louvre_abu_dhabi)

class PlacePalmJumeirahPage(MethodView):
    def get(self):
        return render_template('place.html', render_title_place=render_title_place, render_subtitle_place=render_subtitle_place, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES, events=palm_jumeirah)

class PlaceDubaiAquariumAndUnderwaterZooPage(MethodView):
    def get(self):
        return render_template('place.html', render_title_place=render_title_place, render_subtitle_place=render_subtitle_place, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES, events=dubai_aquarium_and_underwater_zoo)

class AllEventsPage(MethodView):
    def get(self):
        return render_template('all_events.html', render_title_place=render_title_place, render_subtitle_place=render_subtitle_place, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES)

class AllPlacePage(MethodView):
    def get(self):
        return render_template('all_place.html', render_title_place=render_title_place, render_subtitle_place=render_subtitle_place, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES)

class VisaInformationPage(MethodView):
    def get(self):
        return render_template('visa_information_page.html', render_title_faq=render_title_faq, render_subtitle_index=render_subtitle_index, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES)

class FaqPage(MethodView):
    def get(self):
        return render_template('faq.html', render_title_faq=render_title_faq, render_subtitle_index=render_subtitle_index, countries=COUNTRIES)

class PricePage(MethodView):
    def get(self):
        return render_template('price_page.html', render_title_faq=render_title_faq, render_subtitle_index=render_subtitle_index, all_clear=all_clear, decorate_container=decorate_container, countries=COUNTRIES)
