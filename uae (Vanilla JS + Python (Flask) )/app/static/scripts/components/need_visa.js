$(function () {
    const documentButton = $('#documentButton'); // Кнопка переключения на информацию с документами
    const electronicVisaButton = $('#electronicVisaButton'); // Кнопка переключения на информацию о электронной визе
    const covidButton = $('#covidButton'); // Кнопка переключения на информацию по ковиду
    const titleOneItem = $('#titleOneItem'); // Заголовок первого итема
    const titleTwoItem = $('#titleTwoItem'); // Заголовок второго итема
    const titleThreeItem = $('#titleThreeItem'); // Заголовок третьего итема
    const imgOneItem = $('#imgOneItem'); // Картинка первого итема
    const imgTwoItem = $('#imgTwoItem'); // Картинка второго итема
    const imgThreeItem = $('#imgThreeItem'); // Картинка третьего итема
    const textOneItem = $('#textOneItem'); // Текст первого итема
    const textTwoItem = $('#textTwoItem'); // Текст второго итема
    const textThreeItem = $('#textThreeItem'); // Текст третьего итема

    /**
     * Переключение на итемы с информацией документах
     */
    function toggleVisaDocument() {
        titleOneItem.html('REISEPASS <br> SCAN');
        imgOneItem.html('<img src="/static/images/apply-for-visa-1.svg" alt="">');
        textOneItem.html('Klarer Scan oder Bild Ihres Reisepasses mit mindestens 6 Monaten Gültigkeit.');
        titleTwoItem.html('DIGITALES <br> PASSBILD');
        imgTwoItem.html('<img src="/static/images/apply-for-visa-2.svg" alt="">');
        textTwoItem.html('Klares Farbfoto im Pass-Stil oder ein Selfie mit weißem Hintergrund.');
        titleThreeItem.html('ANGABEN <br> ZUR REISE');
        imgThreeItem.html('<img src="/static/images/apply-for-visa-3.svg" alt="">');
        textThreeItem.html('Einreisedatum in die Vereinigten  Arabischen Emirate.');
        documentButton.addClass('--active-apply-btn');
        electronicVisaButton.removeClass('--active-apply-btn');
        covidButton.removeClass('--active-apply-btn');
    }

    /**
     * Переключение на итемы с информацией о электронных визах
     */
    function toggleElectronicVisa() {
        titleOneItem.html('GÜLTIGKEIT <br> UND EINREISE');
        imgOneItem.html('<img src="/static/images/apply-for-visa-4.svg" alt="">');
        textOneItem.html('Das eVisum ist für 90 Tage gültig, d.h. die Einreise muss <b>spätestens</b> 90 Tage nach Ausstellung erfolgen.');
        titleTwoItem.html('AUFENTHALTSDAUER <br> <br>');
        imgTwoItem.html('<img src="/static/images/apply-for-visa-5.svg" alt="">');
        textTwoItem.html('Die Aufenthaltsdauer beginnt mit dem Tag der Einreise in VAE.');
        titleThreeItem.html('DOKUMENTE <br> BEI EINREISEN');
        imgThreeItem.html('<img src="/static/images/apply-for-visa-6.svg" alt="">');
        textThreeItem.html('Einreisedatum in die Vereinigten  Arabischen Emirate.');
        electronicVisaButton.addClass('--active-apply-btn');
        documentButton.removeClass('--active-apply-btn');
        covidButton.removeClass('--active-apply-btn');
    }

    /**
     * Переключение на информацию с информацией о ковиде
     */
    function toggleCovid() {
        titleOneItem.html('PCR-TEST <br>');
        imgOneItem.html('<img src="/static/images/apply-for-visa-7.svg" alt="">');
        textOneItem.html('Ein negativer PCR-COVID-19-Nachweis (nicht älter als 48 Stunden) muss ausgedruckt auf Englisch oder Arabisch vorliegen.');
        titleTwoItem.html('BEI EINREISE <br>');
        imgTwoItem.html('<img src="/static/images/apply-for-visa-8.svg" alt="">');
        textTwoItem.html('Bei allen ankommenden Passagieren wird mittels thermischer Screening-Geräte die Temperatur gemessen.');
        titleThreeItem.html('MASKENPFLICHT <br>');
        imgThreeItem.html('<img src="/static/images/apply-for-visa-9.svg" alt="">');
        textThreeItem.html('Tragen Sie eine Maske, wahren Sie zwei Meter Abstand zu Ihren Mitmenschen und waschen Sie sich regelmäßig die Hände.');
        covidButton.addClass('--active-apply-btn');
        documentButton.removeClass('--active-apply-btn');
        electronicVisaButton.removeClass('--active-apply-btn');
    }

    /**
     * Вешает обработчик событий на кнопки
     */
    function initializesApplyForVisaEvents() {
        documentButton.on('click', toggleVisaDocument);
        electronicVisaButton.on('click', toggleElectronicVisa);
        covidButton.on('click', toggleCovid);
    }

    initializesApplyForVisaEvents();
});

