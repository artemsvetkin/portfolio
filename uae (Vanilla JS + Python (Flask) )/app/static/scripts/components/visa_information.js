$(function () {
    // Переменные для адаптива
    const textVisaOnArrivalAdaptive = $('#textVisaOnArrivalAdaptive'); // Текст с  визой по прибытию
    const textProvisionalVisaAdaptive = $('#textProvisionalVisaAdaptive'); // Текст с  предварительной визой
    const textContactInfoAdaptive = $('#textContactInfoAdaptive'); // Текст с контактной информацией
    const textCovidAdaptive = $('#textCovidAdaptive'); // Текст с ковидом
    const buttonVisaOnArrivalAdaptive = $('#buttonVisaOnArrivalAdaptive'); // Кнопка визы по прибытию
    const buttonProvisionalVisaAdaptive = $('#buttonProvisionalVisaAdaptive'); // Кнопка предварительной визы
    const buttonContactInfoAdaptive = $('#buttonContactInfoAdaptive'); // Кнопка контактной информации
    const buttonCovidAdaptive = $('#buttonCovidAdaptive'); // Кнопка ковида
    const titleVisaOnArrivalAdaptive = $('#titleVisaOnArrivalAdaptive'); // Заголовок визы по прибытию
    const titleProvisionalVisaAdaptive = $('#titleProvisionalVisaAdaptive'); // Заголовок предварительной визы
    const titleContactInfoAdaptive = $('#titleContactInfoAdaptive'); // Заголовок контактной информации
    const titleCovidAdaptive = $('#titleCovidAdaptive'); // Заголовок ковида
    const svgVisaOnArrivalAdaptive = $('#svgVisaOnArrivalAdaptive'); // SVG визы по прибытию
    const svgProvisionalVisaAdaptive = $('#svgProvisionalVisaAdaptive'); // SVG предварительной визы
    const svgContactInfoAdaptive = $('#svgContactInfoAdaptive'); // SVG контактной информации
    const svgCovidAdaptive = $('#svgCovidAdaptive'); // SVG ковида
    // Переменные для компьютерной версии
    const visaInformationText = $('#visaInformationText'); // HTML-контейнер, где меняется текст
    const buttonVisaOnArrival = $('#buttonVisaOnArrival'); // Кнопка визы по прибытию
    const buttonProvisionalVisa = $('#buttonProvisionalVisa'); // Кнопка предварительной визы
    const buttonContactInfo = $('#buttonContactInfo'); // Кнопка контактной информации
    const buttonCovid = $('#buttonCovid'); // Кнопка ковида
    const textVisaOnArrival = '<span>30‑tägige Besuchsberechtigung</span>\n' +
        '                <br><br>\n' +
        '                Wenn Sie Inhaber eines Reisepasses des untenstehenden Landes oder Territoriums sind, benötigen\n' +
        '                Sie für den Besuch der VAE kein Vorabvisum. Sie verlassen lediglich Ihr Flugzeug am Dubai\n' +
        '                International Airport und begeben sich zur Passkontrolle. Dort erhalten Sie kostenlos ein\n' +
        '                Besuchervisum, das 30 Tage gültig ist und in Ihren Pass gestempelt wird.\n' +
        '\n' +
        '                <br><br><br>\n' +
        '                Andorra, Australien, Brunei, Kanada, China, Hongkong, Japan, Kasachstan, Macao, Malaysia,\n' +
        '                Mauritius, Monaco, Neuseeland, Republik Irland, <br>\n' +
        '                San Marino, Singapur, Ukraine, Vereinigtes Königreich und Nordirland, USA, Vatikanstadt.\n' +
        '\n' +
        '                <br><br><br>\n' +
        '                <span>90‑Tage‑Besuchsberechtigung</span> <br>\n' +
        '                Wenn Sie Inhaber eines Reisepasses eines Landes oder Gebiets sind, das in der nachstehenden\n' +
        '                Liste verzeichnet ist, wird Ihr Reisepass mit einem 90‑Tage‑Visum für mehrmalige Einreise\n' +
        '                gestempelt, das 6 Monate ab dem Stempeldatum gültig ist und zu einem Aufenthalt von insgesamt 90\n' +
        '                Tagen berechtigt. Bürger der untenstehenden europäischen Länder sind auch berechtigt, ein im\n' +
        '                Voraus vereinbartes Besuchsvisum zu beantragen, wenn ihr 90‑Tage‑Visum bei der Einreise\n' +
        '                vollständig genutzt wurde.\n' +
        '\n' +
        '                <br><br><br>\n' +
        '                Argentinien, Österreich, Bahamas Inseln, Barbados, Belgien, Brasilien, Bulgarien, Chile,\n' +
        '                Kolumbien, Costa Rica, Kroatien, Zypern, Tschechische Republik, Dänemark, El Salvador, Estland,\n' +
        '                Finnland, Frankreich, Deutschland, Griechenland, Honduras, Ungarn, Island, Italien, Kiribati,\n' +
        '                Lettland, Liechtenstein, Litauen, Luxemburg, Malediven, Malta, Montenegro, Nauru, Niederlande,\n' +
        '                Norwegen, Paraguay, Peru, Polen, Portugal, Rumänien, Russische Föderation, St. Vincent und die\n' +
        '                Grenadinen, San Marino, Serbien, Seychellen, Slowakei, Slowenien, Salomonen,Südkorea, Spanien,\n' +
        '                Schweden, Schweiz, Uruguay.\n' +
        '\n' +
        '                <br><br><br>\n' +
        '                <span>180‑Tage‑Besuchsberechtigung</span> <br>\n' +
        '                Wenn Sie im Besitz eines mexikanischen Reisepasses sind, haben Sie Anspruch auf ein 180‑tägiges\n' +
        '                Besuchsvisum mit mehrfacher Einreise, das für 6 Monate ab Ausstellungsdatum und für einen\n' +
        '                Aufenthalt von insgesamt 180 Tagen gültig ist.\n' +
        '\n' +
        '                <br><br><br>\n' +
        '                <span>Einreiseverfahren in Dubai</span> <br>\n' +
        '                Als zusätzliche Sicherheitsmaßnahme führen die Einwanderungsbehörden am Dubai International\n' +
        '                Airport jetzt bei Dubai‑Besuchern stichprobenartig Augenscans durch.\n' +
        '\n' +
        '                <br><br><br>\n' +
        '                Besucher, die zum Augenscan aufgefordert werden, müssen einen Ausdruck ihres Visums oder eine\n' +
        '                ausgedruckte Version der Visanummer‑Bestätigungsseite am Augenscanschalter vorlegen. Falls Sie\n' +
        '                keinen Ausdruck bei sich haben, fällt eine Gebühr von 30 AED pro Kopie an – zahlbar nur in AED.\n' +
        '\n' +
        '                <br><br><br>\n' +
        '                <span>Wichtige Informationen</span> <br>\n' +
        '                Bevor Sie in die VAE reisen, prüfen Sie bitte die für Sie gültigen Visabestimmungen und stellen\n' +
        '                Sie, falls nötig, sicher, dass Sie ein gültiges Visum besitzen bzw. ob Ihr Reisepass noch gültig\n' +
        '                ist. Für Inhaber von nicht standardisierten Pässen und Reisedokumenten können ebenfalls\n' +
        '                unterschiedliche Einreisebestimmungen gelten: Erkundigen Sie sich, ob Sie ein Visum beantragen\n' +
        '                müssen und ob andere Reisepassvorschriften gelten.\n' +
        '\n' +
        '                <br><br><br>\n' +
        '                Ab 29. April 2016 müssen Inhaber einer GCC‑Aufenthaltserlaubnis vor Ihrer Ankunft in Dubai ein\n' +
        '                Visum für die Vereinigten Arabischen Emirate beantragen.\n' +
        '\n' +
        '                <br><br><br>\n' +
        '                Ab 1. Mai 2017 können indische Staatsbürger mit einem normalen Reisepass, der mindestens sechs\n' +
        '                Monate ab Ankunftsdatum gültig ist, und einem Besuchsvisum oder einer Green Card für die USA,\n' +
        '                die für mindestens sechs Monate gültig ist, oder Personen mit Wohnsitz in Großbritannien oder\n' +
        '                der EU ein Visum bei der Ankunft für einen maximalen Aufenthalt von 14 Tagen gegen eine Gebühr\n' +
        '                von 120 AED (Änderungen vorbehalten) erhalten. Sie können einen Antrag auf Verlängerung ihres\n' +
        '                Aufenthalts um 14 Tage gegen eine Gebühr von 250 AED (Änderungen vorbehalten) stellen.';

    const textPrevisionalVisa = '<span>Im Voraus beantragte VAE-Visa</span>\n' +
        '                <br><br>\n' +
        '                Erforderliche Unterlagen <br>\n' +
        '                Farbfoto des Antragstellers <br>\n' +
        '                Farbkopie des Reisepasses des Antragstellers. Der Reisepass muss mindestens drei Monate gültig sein. <br>\n' +
        '                Das Original‑Anmeldeformular (erhältlich in allen Ticketverkaufsstellen von Emirates) ist je nach\n' +
        '                Staatsangehörigkeit des Antragstellers in Arabisch oder Englisch verfügbar. Laden Sie das Antragsformular in\n' +
        '                Englisch﻿ oder Arabisch﻿ herunter. <br>\n' +
        '                Kopie des Emirates‑Tickets\n' +
        '                <br><br>\n' +
        '                <span>Für Besucher einiger Länder sind zusätzliche Dokumente erforderlich</span> <br>\n' +
        '                Abhängig von Ihrem Wohnsitzland können zusätzliche Dokumente erforderlich sein. Sie können unsere\n' +
        '                Suchfunktion für Visa oben nutzen oder unsere Seite über die Visumpflicht besuchen, um weitere Informationen\n' +
        '                zu erhalten.\n' +
        '                <br><br><br>\n' +
        '\n' +
        '                <span>Regeln und Bedingungen</span> <br>\n' +
        '                Die Visagebühren sind auf Antrag zu entrichten. <br>\n' +
        '                Es fallen zusätzliche Visumbearbeitungsgebühren an. <br>\n' +
        '                Der Fluggast darf nur mit Emirates oder Codeshare‑Flügen nach Dubai einreisen und von Dubai ausreisen. <br>\n' +
        '                Visagebühren können nicht erstattet werden. <br>\n' +
        '                Das Ticket ist nicht erstattbar, wenn das Visum ausgestellt und benutzt wurde. <br>\n' +
        '                Die Bearbeitungszeit für Visa beträgt ca. 3 bis 4 Werktage. <br>\n' +
        '                Die Einreise in die Vereinigten Arabischen Emirate muss von den Einreisebehörden genehmigt werden. <br>\n' +
        '                Alle Visa sind für die Einreise nach Dubai für 60 Tage ab Ausstellungsdatum gültig, mit Ausnahme der 96‑ und\n' +
        '                48‑Stunden‑Visa, die für 30 Tage ab Ausstellungsdatum gültig sind.\n' +
        '                <br><br><br>\n' +
        '\n' +
        '                <span>Wie und wo Sie Ihr vorab vereinbartes Visum für die VAE beantragen können</span> <br>\n' +
        '                Reichen Sie den Antrag über „Buchung verwalten“ ein <br>\n' +
        '                Mobiler DVPC‑Antrag\n' +
        '                <br><br><br>\n' +
        '\n' +
        '                <span>Online‑Antrag</span> <br>\n' +
        '                Staatsangehörige von Ländern, für die ein im Voraus zu beantragendes Visum erforderlich ist, können Ihre\n' +
        '                Visa nun online über emirates.com beantragen und bezahlen.\n' +
        '                <br><br><br>\n' +
        '\n' +
        '                Falls Sie nach Dubai reisen oder einen Zwischenstopp in Dubai einlegen, können Sie das Einreichen des\n' +
        '                Antrags und den Empfang des VAE‑Visums vollständig online erledigen – ohne Ihren Reisepass zum Stempeln\n' +
        '                vorlegen zu müssen. Sie können Ihr Visum für die VAE über „Buchung verwalten“ beantragen, nachdem Sie Ihren\n' +
        '                Flug nach Emirates gebucht haben. Es fallen VFS‑Bearbeitungsgebühren an. Weitere Informationen finden Sie in\n' +
        '                VFS Global.\n' +
        '                <br><br><br>\n' +
        '\n' +
        '                Der Online‑Service für Visumanträge ist für Staatsangehörige und Personen mit permanentem Wohnsitz vieler\n' +
        '                Länder rund um die Welt verfügbar. Hier können Sie herausfinden, ob Sie ein VAE‑Visum beantragen können. Der\n' +
        '                Reisepass des Antragsstellers muss noch mindestens sechs Monate nach der Einreise in Dubai gültig sein und\n' +
        '                es muss sich um einen maschinenlesbaren Reisepass handeln. Von Hand ausgefüllte Reisepässe werden nicht\n' +
        '                akzeptiert.\n' +
        '                <br><br><br>\n' +
        '\n' +
        '                Dieser Service steht nur Fluggästen mit einer bestätigten Emirates‑Buchung zur Verfügung, wenn das Emirates\n' +
        '                Ticket bereits ausgestellt wurde und die Flugreisedauer die gewünschte Aufenthaltszeit in Dubai umfasst. Der\n' +
        '                Reiseplan nach und von Dubai muss auf einem Ticket (PNR) ausgewiesen sein und diese Flüge müssen\n' +
        '                Emirates‑Flüge sein. Beachten Sie, dass Reisepläne, die Flüge nach oder von Dubai mit anderen\n' +
        '                Fluggesellschaften beinhalten, nicht für den Online‑Visa‑Service berechtigt sind.\n' +
        '\n' +
        '                <br><br><br>\n' +
        '                Mindestens vier internationale Werktage sind vor der Ankunft in Dubai erforderlich. Emirates empfiehlt aber,\n' +
        '                den Online‑Antrag so frühzeitig wie möglich einzureichen.';

    const textContactInfo = 'kontakt';
    const textCovid = 'covid';

    /**
     * Изменяет текст
     * @param {String} textInformation Текст с информацией
     */
    function textChange(textInformation) {
        return function () {
            visaInformationText.html(textInformation);
        };
    }

    /**
     * Вешает обработчик событий на кнопки
     */
    function initializesVisaEvents() {
        buttonVisaOnArrival.on('click', textChange(textVisaOnArrival));
        buttonProvisionalVisa.on('click', textChange(textPrevisionalVisa));
        buttonContactInfo.on('click', textChange(textContactInfo));
        buttonCovid.on('click', textChange(textCovid));
    }

    /**
     * В адаптивной версии переключает кнопку, цвет заголовка, цвет svg и текста с информацией
     * @param {Object} btn
     * @param {Object} title
     * @param {Object} svgTitle
     * @param {Object} textInformation
     */
    function toggleButtonAndText(btn, title, svgTitle, textInformation) {
        return function () {
            btn.toggleClass('--close');
            title.toggleClass('--color-turquoise');
            svgTitle.toggleClass('--fill-turquoise');
            if ($(textInformation).css('display') === 'none') {
                $(textInformation).animate({height: 'show'}, 300);
            } else {
                $(textInformation).animate({height: 'hide'}, 300);
            }
        };
    }

    /**
     * В адаптивной версии вешает обработчик событий на кнопки
     */
    function initializesButtonEvents() {
        buttonVisaOnArrivalAdaptive.on('click', toggleButtonAndText(buttonVisaOnArrivalAdaptive, titleVisaOnArrivalAdaptive, svgVisaOnArrivalAdaptive, textVisaOnArrivalAdaptive));
        buttonProvisionalVisaAdaptive.on('click', toggleButtonAndText(buttonProvisionalVisaAdaptive, titleProvisionalVisaAdaptive, svgProvisionalVisaAdaptive, textProvisionalVisaAdaptive));
        buttonContactInfoAdaptive.on('click', toggleButtonAndText(buttonContactInfoAdaptive, titleContactInfoAdaptive, svgContactInfoAdaptive, textContactInfoAdaptive));
        buttonCovidAdaptive.on('click', toggleButtonAndText(buttonCovidAdaptive, titleCovidAdaptive, svgCovidAdaptive, textCovidAdaptive));
    }

    initializesButtonEvents();
    initializesVisaEvents();


});
