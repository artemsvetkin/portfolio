class CountdownTimer {
    constructor(deadline, cbChange, cbComplete) {
        this._deadline = deadline;
        this._cbChange = cbChange;
        this._cbComplete = cbComplete;
        this._timerId = null;
        this._out = {
            days: '', hours: '', minutes: '', seconds: '',
            daysTitle: '', hoursTitle: '', minutesTitle: '', secondsTitle: ''
        };
        this._start();
    }

    static declensionNum(num, words) {
        return words[(num % 100 > 4 && num % 100 < 20) ? 2 : [2, 0, 1, 1, 1, 2][(num % 10 < 5) ? num % 10 : 5]];
    }

    _start() {
        this._calc();
        this._timerId = setInterval(this._calc.bind(this), 1000);
    }

    _calc() {
        const diff = this._deadline - new Date();
        const days = diff > 0 ? Math.floor(diff / 1000 / 60 / 60 / 24) : 0;
        const hours = diff > 0 ? Math.floor(diff / 1000 / 60 / 60) % 24 : 0;
        const minutes = diff > 0 ? Math.floor(diff / 1000 / 60) % 60 : 0;
        const seconds = diff > 0 ? Math.floor(diff / 1000) % 60 : 0;
        this._out.days = days < 10 ? '0' + days : days;
        this._out.hours = hours < 10 ? '0' + hours : hours;
        this._out.minutes = minutes < 10 ? '0' + minutes : minutes;
        this._out.seconds = seconds < 10 ? '0' + seconds : seconds;
        this._out.daysTitle = CountdownTimer.declensionNum(days, ['день', 'дня', 'дней']);
        this._out.hoursTitle = CountdownTimer.declensionNum(hours, ['час', 'часа', 'часов']);
        this._out.minutesTitle = CountdownTimer.declensionNum(minutes, ['минута', 'минуты', 'минут']);
        this._out.secondsTitle = CountdownTimer.declensionNum(seconds, ['секунда', 'секунды', 'секунд']);
        this._cbChange ? this._cbChange(this._out) : null;
        if (diff <= 0) {
            clearInterval(this._timerId);
            this._cbComplete ? this._cbComplete() : null;
        }
    }
}

const elDaysDpWorldTour = document.querySelector('.timer__days__dp_world_tour');
const elDaysDowntownDesign = document.querySelector('.timer__days__downtown_design');
const elDaysInternationalBoatShow = document.querySelector('.timer__days__international_boat_show');
const elDaysEidAlFitr = document.querySelector('.timer__days__eid_al_fitr');
const elDaysElectricVehicleInnovationSummit = document.querySelector('.timer__days__electric_vehicle_innovation_summit');
const elDaysEmiratesAirlineDubaiRugbySevens = document.querySelector('.timer__days__emirates_airline_dubai_rugby_sevens');
const elDaysHrTechnologySummit = document.querySelector('.timer__days__hr_technology_summit');
const elDaysInternationalIndianFilmAcademy = document.querySelector('.timer__days__international_indian_film_academy');
const elDaysAdihex = document.querySelector('.timer__days__adihex');

document.addEventListener('DOMContentLoaded', () => {
    if (elDaysDpWorldTour) {
        // DP World Tour
        const elHoursDpWorldTour = document.querySelector('.timer__hours__dp_world_tour');
        const elMinutesDpWorldTour = document.querySelector('.timer__minutes__dp_world_tour');
        const elSecondsDpWorldTour = document.querySelector('.timer__seconds__dp_world_tour');
        const deadlineDpWorldTour = new Date(new Date().getFullYear(), new Date().getMonth() + 8, 17);
        new CountdownTimer(deadlineDpWorldTour, (timer) => {
            elDaysDpWorldTour.textContent = timer.days;
            elHoursDpWorldTour.textContent = timer.hours;
            elMinutesDpWorldTour.textContent = timer.minutes;
            elSecondsDpWorldTour.textContent = timer.seconds;
            elDaysDpWorldTour.dataset.title = timer.daysTitle;
            elHoursDpWorldTour.dataset.title = timer.hoursTitle;
            elMinutesDpWorldTour.dataset.title = timer.minutesTitle;
            elSecondsDpWorldTour.dataset.title = timer.secondsTitle;
        });
    } else if (elDaysDowntownDesign) {
        // Downtown Design
        const elHoursDowntownDesign = document.querySelector('.timer__hours__downtown_design');
        const elMinutesDowntownDesign = document.querySelector('.timer__minutes__downtown_design');
        const elSecondsDowntownDesign = document.querySelector('.timer__seconds__downtown_design');
        const deadlineDowntownDesign = new Date(new Date().getFullYear(), new Date().getMonth() + 8, 9);
        new CountdownTimer(deadlineDowntownDesign, (timer) => {
            elDaysDowntownDesign.textContent = timer.days;
            elHoursDowntownDesign.textContent = timer.hours;
            elMinutesDowntownDesign.textContent = timer.minutes;
            elSecondsDowntownDesign.textContent = timer.seconds;
            elDaysDowntownDesign.dataset.title = timer.daysTitle;
            elHoursDowntownDesign.dataset.title = timer.hoursTitle;
            elMinutesDowntownDesign.dataset.title = timer.minutesTitle;
            elSecondsDowntownDesign.dataset.title = timer.secondsTitle;
        });
    } else if (elDaysInternationalBoatShow) {
        // Downtown Design
        const elHoursInternationalBoatShow = document.querySelector('.timer__hours__international_boat_show');
        const elMinutesInternationalBoatShow = document.querySelector('.timer__minutes__international_boat_show');
        const elSecondsInternationalBoatShow = document.querySelector('.timer__seconds__international_boat_show');
        const deadlineInternationalBoatShow = new Date(new Date().getFullYear(), new Date().getMonth() + 1, -22);
        new CountdownTimer(deadlineInternationalBoatShow, (timer) => {
            elDaysInternationalBoatShow.textContent = timer.days;
            elHoursInternationalBoatShow.textContent = timer.hours;
            elMinutesInternationalBoatShow.textContent = timer.minutes;
            elSecondsInternationalBoatShow.textContent = timer.seconds;
            elDaysInternationalBoatShow.dataset.title = timer.daysTitle;
            elHoursInternationalBoatShow.dataset.title = timer.hoursTitle;
            elMinutesInternationalBoatShow.dataset.title = timer.minutesTitle;
            elSecondsInternationalBoatShow.dataset.title = timer.secondsTitle;
        });
    } else if (elDaysEidAlFitr) {
        // Downtown Design
        const elHoursEidAlFitr = document.querySelector('.timer__hours__eid_al_fitr');
        const elMinutesEidAlFitr = document.querySelector('.timer__minutes__eid_al_fitr');
        const elSecondsEidAlFitr = document.querySelector('.timer__seconds__eid_al_fitr');
        const deadlineEidAlFitr = new Date(new Date().getFullYear(), new Date().getMonth() + 2, 2);
        new CountdownTimer(deadlineEidAlFitr, (timer) => {
            elDaysEidAlFitr.textContent = timer.days;
            elHoursEidAlFitr.textContent = timer.hours;
            elMinutesEidAlFitr.textContent = timer.minutes;
            elSecondsEidAlFitr.textContent = timer.seconds;
            elDaysEidAlFitr.dataset.title = timer.daysTitle;
            elHoursEidAlFitr.dataset.title = timer.hoursTitle;
            elMinutesEidAlFitr.dataset.title = timer.minutesTitle;
            elSecondsEidAlFitr.dataset.title = timer.secondsTitle;
        });
    } else if (elDaysElectricVehicleInnovationSummit) {
        // Downtown Design
        const elHoursElectricVehicleInnovationSummit = document.querySelector('.timer__hours__electric_vehicle_innovation_summit');
        const elMinutesElectricVehicleInnovationSummit = document.querySelector('.timer__minutes__electric_vehicle_innovation_summit');
        const elSecondsElectricVehicleInnovationSummit = document.querySelector('.timer__seconds__electric_vehicle_innovation_summit');
        const deadlineElectricVehicleInnovationSummit = new Date(new Date().getFullYear(), new Date().getMonth() + 2, 23);
        new CountdownTimer(deadlineElectricVehicleInnovationSummit, (timer) => {
            elDaysElectricVehicleInnovationSummit.textContent = timer.days;
            elHoursElectricVehicleInnovationSummit.textContent = timer.hours;
            elMinutesElectricVehicleInnovationSummit.textContent = timer.minutes;
            elSecondsElectricVehicleInnovationSummit.textContent = timer.seconds;
            elDaysElectricVehicleInnovationSummit.dataset.title = timer.daysTitle;
            elHoursElectricVehicleInnovationSummit.dataset.title = timer.hoursTitle;
            elMinutesElectricVehicleInnovationSummit.dataset.title = timer.minutesTitle;
            elSecondsElectricVehicleInnovationSummit.dataset.title = timer.secondsTitle;
        });
    } else if (elDaysEmiratesAirlineDubaiRugbySevens) {
        // Downtown Design
        const elHoursEmiratesAirlineDubaiRugbySevens = document.querySelector('.timer__hours__emirates_airline_dubai_rugby_sevens');
        const elMinutesEmiratesAirlineDubaiRugbySevens = document.querySelector('.timer__minutes__emirates_airline_dubai_rugby_sevens');
        const elSecondsEmiratesAirlineDubaiRugbySevens = document.querySelector('.timer__seconds__emirates_airline_dubai_rugby_sevens');
        const deadlineEmiratesAirlineDubaiRugbySevens = new Date(new Date().getFullYear(), new Date().getMonth() + 9, 1);
        new CountdownTimer(deadlineEmiratesAirlineDubaiRugbySevens, (timer) => {
            elDaysEmiratesAirlineDubaiRugbySevens.textContent = timer.days;
            elHoursEmiratesAirlineDubaiRugbySevens.textContent = timer.hours;
            elMinutesEmiratesAirlineDubaiRugbySevens.textContent = timer.minutes;
            elSecondsEmiratesAirlineDubaiRugbySevens.textContent = timer.seconds;
            elDaysEmiratesAirlineDubaiRugbySevens.dataset.title = timer.daysTitle;
            elHoursEmiratesAirlineDubaiRugbySevens.dataset.title = timer.hoursTitle;
            elMinutesEmiratesAirlineDubaiRugbySevens.dataset.title = timer.minutesTitle;
            elSecondsEmiratesAirlineDubaiRugbySevens.dataset.title = timer.secondsTitle;
        });
    } else if (elDaysHrTechnologySummit) {
        // Downtown Design
        const elHoursHrTechnologySummit = document.querySelector('.timer__hours__hr_technology_summit');
        const elMinutesHrTechnologySummit = document.querySelector('.timer__minutes__hr_technology_summit');
        const elSecondsHrTechnologySummit = document.querySelector('.timer__seconds__hr_technology_summit');
        const deadlineHrTechnologySummit = new Date(new Date().getFullYear(), new Date().getMonth() + 7, 17);
        new CountdownTimer(deadlineHrTechnologySummit, (timer) => {
            elDaysHrTechnologySummit.textContent = timer.days;
            elHoursHrTechnologySummit.textContent = timer.hours;
            elMinutesHrTechnologySummit.textContent = timer.minutes;
            elSecondsHrTechnologySummit.textContent = timer.seconds;
            elDaysHrTechnologySummit.dataset.title = timer.daysTitle;
            elHoursHrTechnologySummit.dataset.title = timer.hoursTitle;
            elMinutesHrTechnologySummit.dataset.title = timer.minutesTitle;
            elSecondsHrTechnologySummit.dataset.title = timer.secondsTitle;
        });
    } else if (elDaysInternationalIndianFilmAcademy) {
        // Downtown Design
        const elHoursInternationalIndianFilmAcademy = document.querySelector('.timer__hours__international_indian_film_academy');
        const elMinutesInternationalIndianFilmAcademy = document.querySelector('.timer__minutes__international_indian_film_academy');
        const elSecondsInternationalIndianFilmAcademy = document.querySelector('.timer__seconds__international_indian_film_academy');
        const deadlineInternationalIndianFilmAcademy = new Date(new Date().getFullYear(), new Date().getMonth() + 2, 20);
        new CountdownTimer(deadlineInternationalIndianFilmAcademy, (timer) => {
            elDaysInternationalIndianFilmAcademy.textContent = timer.days;
            elHoursInternationalIndianFilmAcademy.textContent = timer.hours;
            elMinutesInternationalIndianFilmAcademy.textContent = timer.minutes;
            elSecondsInternationalIndianFilmAcademy.textContent = timer.seconds;
            elDaysInternationalIndianFilmAcademy.dataset.title = timer.daysTitle;
            elHoursInternationalIndianFilmAcademy.dataset.title = timer.hoursTitle;
            elMinutesInternationalIndianFilmAcademy.dataset.title = timer.minutesTitle;
            elSecondsInternationalIndianFilmAcademy.dataset.title = timer.secondsTitle;
        });
    } else if (elDaysAdihex) {
        // Downtown Design
        const elHoursAdihex = document.querySelector('.timer__hours__adihex');
        const elMinutesAdihex = document.querySelector('.timer__minutes__adihex');
        const elSecondsAdihex = document.querySelector('.timer__seconds__adihex');
        const deadlineAdihex = new Date(new Date().getFullYear(), new Date().getMonth() + 6, 26);
        new CountdownTimer(deadlineAdihex, (timer) => {
            elDaysAdihex.textContent = timer.days;
            elHoursAdihex.textContent = timer.hours;
            elMinutesAdihex.textContent = timer.minutes;
            elSecondsAdihex.textContent = timer.seconds;
            elDaysAdihex.dataset.title = timer.daysTitle;
            elHoursAdihex.dataset.title = timer.hoursTitle;
            elMinutesAdihex.dataset.title = timer.minutesTitle;
            elSecondsAdihex.dataset.title = timer.secondsTitle;
        });
    }


});
