/**
 * Конфиги слайдера
 * @param {jQuery} sliderElem Слайдер
 * @param {object} params Объект с конфигами
 * @param {jQuery} params.leftArrow Левая стрелка слайдера
 * @param {jQuery} params.rightArrow Правая стрелка слайдера
 * @param {boolean} params.dots Показывать ли точки переключения между слайдами
 * @param {string} params.dotsClass CSS-класс у dots
 */
$(function () {
    const window_width = $(window).width();
    function initSlider(sliderElem, {leftArrow, rightArrow, dots, dotsClass, slidesShow}) {
        sliderElem.slick({
            infinite: true,
            slidesToShow: slidesShow,
            slidesToScroll: 1,
            prevArrow: leftArrow,
            nextArrow: rightArrow,
            centerMode: true,
            variableWidth: true,
            dots: dots,
            dotsClass: dotsClass,
        });
    }

    initSlider($('.customer-ask__slider'), {
        dots: true,
        dotsClass: 'customer-ask__dots',
        slidesToShow: 3,
        leftArrow: $('.customer-ask__arrow-left'),
        rightArrow: $('.customer-ask__arrow-right'),
    });

    initSlider($('.popular-place__slider'), {
        slidesToShow: 3,
        leftArrow: $('.popular-place__arrow-left'),
        rightArrow: $('.popular-place__arrow-right'),
    });

    initSlider($('.upcoming-events__slider'), {
        slidesToShow: 3,
        leftArrow: $('.upcoming-events__arrow-left'),
        rightArrow: $('.upcoming-events__arrow-right'),
    });

    initSlider($('.event__slider'), {
        slidesToShow: 3,
        leftArrow: $('.event__arrow-left'),
        rightArrow: $('.event__arrow-right'),
    });

    initSlider($('.place__slider'), {
        slidesToShow: 3,
        leftArrow: $('.place__arrow-left'),
        rightArrow: $('.place__arrow-right'),
    });

    if (window_width <= 767) {
        initSlider($('.all-events-main__list'), {
            dots: true,
            dotsClass: 'going-the-moment__dots',
            slidesToShow: 3,
            leftArrow: $('.all-events-main__arrow-left'),
            rightArrow: $('.all-events-main__arrow-right'),
        });

        initSlider($('.all-place__list'), {
            dots: true,
            dotsClass: 'going-the-moment__dots',
            slidesToShow: 3,
            leftArrow: $('.all-place__arrow-left'),
            rightArrow: $('.all-place__arrow-right'),
        });
    }
});




