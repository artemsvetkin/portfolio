$(function () {
    const sidebar = $('#sidebar'); // Сайдбар
    const sidebarBurger = $('#sidebarBurger'); // Кнопка бургер
    const logoContainer = $('#logoContainer'); // Контейнер с лого и бургером
    const darkMask = $('#darkMask'); // Черная маска

    /**
     * Переключает sidebar
     */
    function toggleSidebar() {
        sidebar.toggleClass('open-sidebar');
        logoContainer.toggleClass('container-fixed');
        darkMask.toggleClass(['mask', 'page-mask']);
        sidebarBurger.toggleClass('close-sidebar');
    }

    /**
     * Скрывает sidebar
     */
    function hideSidebar() {
        sidebar.removeClass('open-sidebar');
        logoContainer.removeClass('container-fixed');
        sidebarBurger.removeClass('close-sidebar');
    }

    /**
     * Вешает обработчики событий на кнопки
     */
    function initializesSidebarEvents() {
        sidebarBurger.on('click', toggleSidebar);
        darkMask.on('click', hideSidebar);
    }

    initializesSidebarEvents();
});

