$(function () {
    const priceTitleTourismStandard = $('#priceTitleTourismStandard'); // Заголовок цены стандартной туристической визы
    const priceTitleTourismExpress = $('#priceTitleTourismExpress'); // Заголовок цены быстрой туристической визы
    const priceTitleBusinessStandard = $('#priceTitleBusinessStandard'); // Заголовок цены стандартной деловой визы
    const priceTitleBusinessExpress = $('#priceTitleBusinessExpress'); // Заголовок цены быстрой деловой визы
    const priceDescriptionTourismStandard = $('#priceDescriptionTourismStandard'); // Описание цены стандартной туристической визы
    const priceDescriptionTourismExpress = $('#priceDescriptionTourismExpress'); // Описание цены быстрой туристической визы
    const priceDescriptionBusinessStandard = $('#priceDescriptionBusinessStandard'); // Описание цены стандартной деловой визы
    const priceDescriptionBusinessExpress = $('#priceDescriptionBusinessExpress'); // Описание цены быстрой деловой визы
    const priceArrowTourismStandard = $('#priceArrowTourismStandard'); // Стрелка цены стандартной туристической визы
    const priceArrowTourismExpress = $('#priceArrowTourismExpress'); // Стрелка цены быстрой туристической визы
    const priceArrowBusinessStandard = $('#priceArrowBusinessStandard'); // Стрелка цены стандартной деловой визы
    const priceArrowBusinessExpress = $('#priceArrowBusinessExpress'); // Стрелка цены быстрой деловой визы

    /**
     * Переключает описание визы в адаптиве
     * @param description {Object} Описание визы
     * @param arrow {Object} Стрелка переключения
     */
    function togglePriceAdaptive(description, arrow) {
        return function () {
            if ($(description).css('display') === 'none') {
                $(description).animate({height: 'show'}, 300);
                arrow.addClass('price__open-arrow');
            } else {
                $(description).animate({height: 'hide'}, 300);
                arrow.removeClass('price__open-arrow');
            }
        };
    }

    /**
     * Вешает событие на блок с ценой в адаптиве
     */
    function initializesPriceAdaptiveEvents() {
        const window_width = $(window).width();
        if (window_width <= 550) {
            priceTitleTourismStandard.on('click', togglePriceAdaptive(priceDescriptionTourismStandard, priceArrowTourismStandard));
            priceTitleTourismExpress.on('click', togglePriceAdaptive(priceDescriptionTourismExpress, priceArrowTourismExpress));
            priceTitleBusinessStandard.on('click', togglePriceAdaptive(priceDescriptionBusinessStandard, priceArrowBusinessStandard));
            priceTitleBusinessExpress.on('click', togglePriceAdaptive(priceDescriptionBusinessExpress, priceArrowBusinessExpress));
        }
    }

    initializesPriceAdaptiveEvents();
});

