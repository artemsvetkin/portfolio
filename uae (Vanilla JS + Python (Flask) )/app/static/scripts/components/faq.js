$(function () {
    const button1 = $('#button1');
    const button2 = $('#button2');
    const button3 = $('#button3');
    const button4 = $('#button4');
    const button5 = $('#button5');

    const blockText1 = $('#blockText1');
    const blockText2 = $('#blockText2');
    const blockText3 = $('#blockText3');
    const blockText4 = $('#blockText4');
    const blockText5 = $('#blockText5');

    const circle1 = $('#circle1');
    const circle2 = $('#circle2');
    const circle3 = $('#circle3');
    const circle4 = $('#circle4');
    const circle5 = $('#circle5');

    const square1 = $('#square1');
    const square2 = $('#square2');
    const square3 = $('#square3');
    const square4 = $('#square4');
    const square5 = $('#square5');

    /**
     * Переключает блок текста, анмимирует квадрат и круг
     * @param btn {Object} Кнопка блока
     * @param blockText {Object} Блок с текстом
     * @param square {Object} Квадрат
     * @param circle {Object} Круг
     */
    function toggleQuickAnswer(btn, blockText, square, circle) {
        return function () {
            btn.toggleClass('--close');
            if ($(blockText).css('display') === 'none') {
                $(blockText).animate({height: 'show'}, 300);
                $(square).animate({height: 'show'}, 500);
                $(circle).animate({height: 'show'}, 500);
            } else {
                $(blockText).animate({height: 'hide'}, 300);
                $(square).animate({height: 'hide'}, 300);
                $(circle).animate({height: 'hide'}, 300);
            }
        };
    }

    /**
     * Вешает событие на блок с быстрыми ответами
     */
    function initializesQuickAnswerEvents() {
        button1.on('click', toggleQuickAnswer(button1, blockText1, circle1, square1));
        button2.on('click', toggleQuickAnswer(button2, blockText2, circle2, square2));
        button3.on('click', toggleQuickAnswer(button3, blockText3, circle3, square3));
        button4.on('click', toggleQuickAnswer(button4, blockText4, circle4, square4));
        button5.on('click', toggleQuickAnswer(button5, blockText5, circle5, square5));
    }

    initializesQuickAnswerEvents();
});
