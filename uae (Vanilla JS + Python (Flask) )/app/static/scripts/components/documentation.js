$(function () {
    const imprintText = $('#imprintText'); // Текст Impressum
    const cancellationPolicyText = $('#cancellationPolicyText'); // Текст Widerrufsbelehrung
    const dataProtectionText = $('#dataProtectionText'); // Текст Datenshutz
    const agbText = $('#agbText'); // Текст AGB

    const inputImprint = $('#inputImprint'); // Кнопка Impressum
    const inputCancellationPolicy = $('#inputCancellationPolicy'); // Кнопка Widerrufsbelehrung
    const inputDataProtection = $('#inputDataProtection'); // Кнопка Datenshutz
    const inputAgb = $('#inputAgb'); // Кнопка AGB

    /**
     * Переключает текст
     * @param showText {Object} Показывает текст соответсвующей кнопки
     * @param hideOneText {Object} Скрывает первый текст
     * @param hideTwoText {Object} Скрывает второй текст
     * @param hideThreeText {Object} Скрывает третий текст
     */
    function toggleText(showText, hideOneText, hideTwoText, hideThreeText) {
        return function () {
            showText.css({
                display: 'block'
            });
            hideOneText.css({
                display: 'none'
            });
            hideTwoText.css({
                display: 'none'
            });
            hideThreeText.css({
                display: 'none'
            });
        };
    }

    /**
     * Проверяет наличие аттрибута checked у инпута
     * @param input {Object} Input кнопки
     * @param showText {Object} Показывает текст соответсвующей кнопки
     * @param hideOneText {Object} Скрывает первый текст
     * @param hideTwoText {Object} Скрывает второй текст
     * @param hideThreeText {Object} Скрывает третий текст
     */
    function checkedInput(input, showText, hideOneText, hideTwoText, hideThreeText) {
        if (input.attr('checked')) {
            showText.css({
                display: 'block'
            });
            hideOneText.css({
                display: 'none'
            });
            hideTwoText.css({
                display: 'none'
            });
            hideThreeText.css({
                display: 'none'
            });
        }
    }

    /**
     * Вешает обработчик событий на кнопки
     */
    function initializesInputEvents() {
        inputImprint.on('change', toggleText(imprintText, cancellationPolicyText, dataProtectionText, agbText));
        inputCancellationPolicy.on('change', toggleText(cancellationPolicyText, imprintText, dataProtectionText, agbText));
        inputAgb.on('change', toggleText(agbText, cancellationPolicyText, dataProtectionText, imprintText));
        inputDataProtection.on('change', toggleText(dataProtectionText, cancellationPolicyText, imprintText, agbText));
    }

    initializesInputEvents();
    checkedInput(inputImprint, imprintText, cancellationPolicyText, dataProtectionText, agbText);
    checkedInput(inputCancellationPolicy, cancellationPolicyText, imprintText, dataProtectionText, agbText);
    checkedInput(inputAgb, agbText, cancellationPolicyText, dataProtectionText, imprintText);
    checkedInput(inputDataProtection, dataProtectionText, cancellationPolicyText, imprintText, agbText);


});
