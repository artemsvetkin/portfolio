$(function () {
    const priceSelect = $('#priceSelect'); // Селект с выбором страны
    const priceButton = $('#priceButton'); // Кнопка селекта
    const priceInformationVisa = $('#priceInformationVisa'); // Информация о визе
    const priceForm = $('#priceForm'); // Форма

    /**
     * Инициализация select2
     */
    priceSelect.select2({
        selectionCssClass: 'page-price__custom-select',
        placeholder: 'Bitte auswählen',
        dropdownParent: priceForm,
        dropdownCssClass: 'page-price__custom-option',
        dropdownPosition: 'below'
    });

    /**
     * Убирает аттрибут disabled на кнопке, также убирает класс disabled
     */
    function removeAttributeAndRemoveClassInButton() {
        priceButton.removeClass('--page-price-close-btn');
        priceButton.removeAttr('disabled');
    }

    /**
     * Вешает обработчик событий на кнопку формы
     */
    priceSelect.on('change', removeAttributeAndRemoveClassInButton);

    /**
     * Показывает информацию о визе
     */
    function showInformationVisa() {
        priceButton.on('click', e => {
            priceInformationVisa.show();
        });
    }

    showInformationVisa();
});
