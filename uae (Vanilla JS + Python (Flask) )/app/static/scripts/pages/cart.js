$(function () {
    const cartButton = $('#cart'); // Кнопка корзины
    const cart = $('#modalStartPage'); // Корзина
    const mask = $('#darkMask'); // Черная маска

    /**
     * Переключает корзину
     */
    function toggleCart() {
        cartButton.on('click', e => {
            cart.css({
                right: 0,
            });
            mask.addClass('page-mask');
            mask.addClass('mask');
        });
    }

    toggleCart();
});
