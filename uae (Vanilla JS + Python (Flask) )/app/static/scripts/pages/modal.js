$(function () {
    const modalStartContainer = $('#modalStartContainer'); // HTML-контейнер с контентом в модальном окне
    const modalStartPage = $('#modalStartPage'); // Стартовая страница модального окна
    const modalQuestionnaire = $('#modalQuestionnaire'); // Страница с анкетой
    const modalAddApplicant = $('#modalAddApplicant'); // Страница с добавлением нового пользователя
    const modalPay = $('#modalPay'); // Страница с оплатой

    const startPageNext = $('#startPageNext'); // Кнпока next на стартовой странице
    const questionnaireNextButton = $('#questionnaireNextButton'); // Кнопка next на странице с анкетой
    const buttonNextAddApplicant = $('#buttonNextAddApplicant'); // Кнопка next на странице с добавлением нового пользователя

    const questionnaireBackButton = $('#questionnaireBackButton'); // Кнопка назад на странице с анкетой
    const buttonBackAddApplicant = $('#buttonBackAddApplicant'); // Кнопка назад на странице добавления нового пользователя
    const backModalPay = $('#backModalPay'); // Кнопка назад на странице с оплатой

    const startPageCancel = $('#startPageCancel'); // Кнопка cancel на стартовой странице
    const questionnaireCancelButton = $('#questionnaireCancelButton'); // Кнопка cancel на странице с анкетой
    const buttonCancelAddApplicant = $('#buttonCancelAddApplicant'); // Кнопка cancel на странице с добавлением нового пользователя

    const closeModal = $('#closeModal'); // Кнпока закрытия модального окна
    const loader = $('#loader'); // Лоадер между страницами
    const buttonAddApplicant = $('#buttonAddApplicant'); // Кнпока добавления нового пользователя
    const select = $('#select'); // Селект
    const options = $('#options'); // Все options
    const darkMask = $('#darkMask'); // Черная маска

    const stylizedRadioTourismStandard = $('#stylizedRadioTourismStandard'); // Стилизованная радио кнпока стандартной туристической визы
    const stylizedRadioTourismExpress = $('#stylizedRadioTourismExpress'); // Стилизованная радио кнпока быстрой туристической визы
    const stylizedRadioBusinessStandard = $('#stylizedRadioBusinessStandard'); // Стилизованная радио кнпока стандартной деловой визы
    const stylizedRadioBusinessExpress = $('#stylizedRadioBusinessExpress'); // Стилизованная радио кнпока быстрой деловой визы

    const radioTourismStandard = $('#radioTourismStandard'); // Радио кнпока стандартной туристической визы
    const radioTourismExpress = $('#radioTourismExpress'); // Радио кнпока быстрой туристической визы
    const radioBusinessStandard = $('#radioBusinessStandard'); // Радио кнпока стандартной деловой визы
    const radioBusinessExpress = $('#radioBusinessExpress'); // Радио кнпока быстрой деловой визы

    const totalPrice = $('#totalPrice'); // Цена заполненных анкет
    const radioTypeVisa = $('[name=radioTypeVisa]'); // HTML-радио инпут

    const titleTourismNormal = $('#titleTourismNormal'); // Заголовок стандартной туристической визы
    const titleTourismExpress = $('#titleTourismExpress'); // Заголовок быстрой туристической визы
    const titleBusinessNormal = $('#titleBusinessNormal');  // Заголовок стандартной деловой визы
    const titleBusinessExpress = $('#titleBusinessExpress'); // Заголовок быстрой деловой визы

    const modalPriceTourismNormal = $('#modalPriceTourismNormal'); // Цена станадартной туристической визы в модальном окне
    const modalPriceTourismExpress = $('#modalPriceTourismExpress'); // Цена быстрой туристической визы в модальном окне
    const modalPriceBusinessNormal = $('#modalPriceBusinessNormal'); // Цена станадартной деловой визы в модальном окне
    const modalPriceBusinessExpress = $('#modalPriceBusinessExpress'); // Цена быстрой деловой визы в модальном окне

    const passportImage = $('#passportImg'); // Фото пспорта
    const photoPassportImage = $('#photoPassportImg'); // Фото фотографии на визу
    const contentFilePassport = $('#filePassport'); // HTML-контейнер паспорта
    const contentFilePhotoPassport = $('#filePhotoPassport'); // HTML-контейнер фото на визу
    const passportInput = $('#passportInput'); // Инпут паспорта
    const photoPassportInput = $('#photoPassportInput'); // Инпут фото на паспорт

    /**
     * Активирует радио кнпоку стандартной туристической визы
     * Деактивирует остальные радикнопки
     * Добавляет соответствующую цену в итоговую стоимость
     */
    function checkedRadioTourismStandard() {
        stylizedRadioTourismStandard.addClass('checked');
        radioTourismStandard.prop('checked', true);
        stylizedRadioTourismExpress.removeClass('checked');
        stylizedRadioBusinessStandard.removeClass('checked');
        stylizedRadioBusinessExpress.removeClass('checked');
        totalPrice.text('149 EUR');
    }

    /**
     * Активирует радио кнпоку быстрой туристической визы
     * Деактивирует остальные радикнопки
     * Добавляет соответствующую цену в итоговую стоимость
     */
    function checkedRadioTourismExpress() {
        stylizedRadioTourismExpress.addClass('checked');
        radioTourismExpress.prop('checked', true);
        stylizedRadioTourismStandard.removeClass('checked');
        stylizedRadioBusinessStandard.removeClass('checked');
        stylizedRadioBusinessExpress.removeClass('checked');
        totalPrice.text('220 EUR');
    }

    /**
     * Активирует радио кнпоку стандартной деловой визы
     * Деактивирует остальные радикнопки
     * Добавляет соответствующую цену в итоговую стоимость
     */
    function checkedRadioBusinessStandard() {
        stylizedRadioBusinessStandard.addClass('checked');
        radioBusinessStandard.prop('checked', true);
        stylizedRadioTourismStandard.removeClass('checked');
        stylizedRadioTourismExpress.removeClass('checked');
        stylizedRadioBusinessExpress.removeClass('checked');
        totalPrice.text('199 EUR');
    }

    /**
     * Активирует радио кнпоку быстрой деловой визы
     * Деактивирует остальные радикнопки
     * Добавляет соответствующую цену в итоговую стоимость
     */
    function checkedRadioBusinessExpress() {
        stylizedRadioBusinessExpress.addClass('checked');
        radioBusinessExpress.prop('checked', true);
        stylizedRadioTourismStandard.removeClass('checked');
        stylizedRadioTourismExpress.removeClass('checked');
        stylizedRadioBusinessStandard.removeClass('checked');
        totalPrice.text('350 EUR');
    }

    /**
     * Вешает обработчик событий на радикнопки
     */
    function initializesRadioEvents() {
        stylizedRadioTourismStandard.on('click', checkedRadioTourismStandard);
        stylizedRadioTourismExpress.on('click', checkedRadioTourismExpress);
        stylizedRadioBusinessStandard.on('click', checkedRadioBusinessStandard);
        stylizedRadioBusinessExpress.on('click', checkedRadioBusinessExpress);
    }

    /**
     * Загружает фотографию на страницу
     * @param input {Object} Инпут
     * @param fileContainer {Object} Контейнер, куда кладется фотография
     * @param image {Object} Фотография
     */
    function loadsImg(input, fileContainer, image) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                image.attr('src', e.target.result);
            };
            fileContainer.removeAttr('style').hide();
            reader.readAsDataURL(input.files[0]);
            image.show();
        }
    }

    /**
     * Вешает обработчики событий на инпут с фотографиями
     */
    function initializesImgEvents() {
        passportInput.change(function () {
            loadsImg(this, contentFilePassport, passportImage);
        });

        photoPassportInput.change(function () {
            loadsImg(this, contentFilePhotoPassport, photoPassportImage);
        });
    }

    /**
     * Скрывает маску, модалку, options
     */
    function hideModalAndMaskAndOption() {
        darkMask.removeClass('page-mask');
        darkMask.removeClass('mask');
        modalStartPage.css({
            right: '-1000px'
        });
        select.removeClass('open-select');
        options.removeClass('show-select');
        transitionModalStart();
    }

    /**
     * Вешает обработчик событий на маску
     */
    function initializesMaskEvents() {
        darkMask.on('click', hideModalAndMaskAndOption);
    }

    /**
     * Кодирование изображений в Base64
     * @param file {Object} Фотографии
     */
    async function getBase64(file) {
        return new Promise(((resolve, reject) => {
            const reader = new FileReader();
            let blob = new Blob([file], {type: 'text/plain'});
            reader.readAsDataURL(blob);
            reader.onload = function () {
                resolve(reader.result);
            };
            reader.onerror = function (error) {
                reject(error);
            };
        }));
    }

    /**
     * Делает HTTP-запрос страницы с анкетой
     */
    async function makeRequestQuestionnaire() {
        const fillingDate = $('#fillingDate');
        const departureDate = $('#departureDate');
        const firstAndLastName = $('#firstAndLastName');
        const streetName = $('#streetName');
        const houseNumber = $('#houseNumber');
        const postCode = $('#postCode');
        const city = $('#city');
        const email = $('#email');

        let data = {
            'fillingDate': fillingDate.value,
            'departureDate': departureDate.value,
            'firstAndLastName': firstAndLastName.value,
            'streetName': streetName.value,
            'houseNumber': houseNumber.value,
            'postCode': postCode.value,
            'city': city.value,
            'email': email.value
        };

        let response = await fetch('/true_need_visa/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(data)
        });

        let result = await response.json();
        console.log(result);
    }

    /**
     * Вешает обработчик событий на кпоку next страницы с анкетой
     */
    function initializesRequestQuestionnaireEvents() {
        questionnaireNextButton.on('click', makeRequestQuestionnaire);
    }

    /**
     * Делает HTTP-запрс на стартовой странице
     */
    async function makeRequestModalStartPage() {
        let data = {};
        if (radioTypeVisa[0].checked) {
            data.visaName = titleTourismNormal.innerText;
            data.price = modalPriceTourismNormal.innerText;
        } else if (radioTypeVisa[1].checked) {
            data.visaName = titleTourismExpress.innerText;
            data.price = modalPriceTourismExpress.innerText;
        } else if (radioTypeVisa[2].checked) {
            data.visaName = titleBusinessNormal.innerText;
            data.price = modalPriceBusinessNormal.innerText;
        } else if (radioTypeVisa[3].checked) {
            data.visaName = titleBusinessExpress.innerText;
            data.price = modalPriceBusinessExpress.innerText;
        }

        data.passport = await getBase64(passportInput);
        data.photo = await getBase64(photoPassportInput);


        let response = await fetch('/true_need_visa/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(data)
        });

        const result = await response.json();
        console.log(result);
        console.log(data);
    }

    /**
     * Вешает обработчик событий на кнопку next стартовой страницы
     */
    function initializesStartPageEvents() {
        startPageNext.on('click', makeRequestModalStartPage);
    }

    /**
     * Закрывает модальное окно, скрывает маску, меняет текст в селекте на дефолтный
     */
    function cancelModal() {
        modalStartPage.css({
            right : '-1000px'
        });
        darkMask.removeClass('page-mask');
        transitionModalStart();
        select.text('Select your visa now');
    }

    /**
     * Вешает обработчик событий на кнопку cancel
     */
    function initializesCancelButtonEvents() {
        startPageCancel.on('click', cancelModal);
        questionnaireCancelButton.on('click', cancelModal);
        buttonCancelAddApplicant.on('click', cancelModal);
        closeModal.on('click', cancelModal);
    }

    /**
     * Возращает на стартовую страницу модального окна
     */
    function transitionModalStart() {
        modalStartContainer.css({
            display: 'flex'
        });
        modalQuestionnaire.css({
            display: 'none'
        });
        modalAddApplicant.css({
            display: 'none'
        });
        modalPay.css({
            display: 'none'
        });
    }

    /**
     * Делает задержку лоадеру
     * @param ms {number} Время задержки
     */
    function sleepLoader(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    /**
     * Скрывает модальное окно и переходит к следующему или переходит к предыдущему
     * @param modalClose {Object} Модальное окно, которое скрывается
     * @param modalShow {Object} Модальное окно, которое показывается
     */
    function nextAndBackModalPage(modalClose, modalShow) {
        return async function () {
            modalClose.css({
                display: 'none'
            });
            loader.css({
                display: 'flex'
            });
            await sleepLoader(200);
            modalShow.css({
                display: 'flex'
            });
            loader.css({
                display: 'none'
            });
        };
    }

    /**
     * Вешает обработчик событий на кнпоки next и back
     */
    function initializesNextAndBackButtonEvents() {
        startPageNext.on('click', nextAndBackModalPage(modalStartContainer, modalQuestionnaire));
        questionnaireNextButton.on('click', nextAndBackModalPage(modalQuestionnaire, modalAddApplicant));
        buttonNextAddApplicant.on('click', nextAndBackModalPage(modalAddApplicant, modalPay));
        questionnaireBackButton.on('click', nextAndBackModalPage(modalQuestionnaire, modalStartContainer));
        buttonBackAddApplicant.on('click', nextAndBackModalPage(modalAddApplicant, modalQuestionnaire));
        backModalPay.on('click', nextAndBackModalPage(modalPay, modalAddApplicant));
        buttonAddApplicant.on('click', nextAndBackModalPage(modalAddApplicant, modalStartContainer));
    }

    /**
     * Добавляет нового пользователя на странице с добавлением нового пользователя
     */
    function addPersons() {
        const addApplicantList = $('#addApplicantList');
        const addPerson = $('<div id="addPerson"></div>');
        const photoPerson = $('<img src="" alt="" id="photoPerson">');
        const typeVisa = $('<span id="typeVisa"></span>');
        const priceVisa = $('<span id="priceVisa"></span>');
        const btnEdit = $('<span id="btnEdit"></span>');
        const textContainer = $('<div id="textContainer"></div>');
        const leftContainer = $('<div id="leftContainer"></div>');
        textContainer.addClass('modal-add-applicant__item-container-text');
        addPerson.addClass('modal-add-applicant__item');
        photoPerson.addClass('modal-add-applicant__photo');
        typeVisa.addClass('modal-add-applicant__type-visa');
        btnEdit.addClass('modal-add-applicant__link');
        priceVisa.addClass('modal-add-applicant__price');
        leftContainer.addClass('modal-add-applicant__left-container');
        photoPerson.attr('src', '/static/images/modal-add-applicant-photo.svg');
        if (radioTypeVisa[0].checked) {
            typeVisa.text(titleTourismNormal.text());
            priceVisa.text(modalPriceTourismNormal.text());
        } else if (radioTypeVisa[1].checked) {
            typeVisa.text(titleTourismExpress.text());
            priceVisa.text(modalPriceTourismExpress.text());
        } else if (radioTypeVisa[2].checked) {
            typeVisa.text(titleBusinessNormal.text());
            priceVisa.text(modalPriceBusinessNormal.text());
        } else if (radioTypeVisa[3].checked) {
            typeVisa.text(titleBusinessExpress.text());
            priceVisa.text(modalPriceBusinessExpress.text());
        }
        btnEdit.text('ändern');
        leftContainer.append(photoPerson);
        textContainer.append(typeVisa);
        textContainer.append(btnEdit);
        leftContainer.append(textContainer);
        addPerson.append(leftContainer);
        addPerson.append(priceVisa);
        addApplicantList.prepend(addPerson);
        addPersonsPay();
    }

    /**
     * Добавляет нового пользователя на страницу с оплатой
     */
    function addPersonsPay() {
        const personsPayList = $('#personsPayList');
        const addPersonPay = $('<div id="addPersonPay"></div>');
        const photoPersonPay = $('<img src="" alt="" id="photoPersonPay">');
        const typeVisaPay = $('<span id="typeVisaPay"></span>');
        const priceVisaPay = $('<span id="priceVisaPay"></span>');
        const btnDelete = $('<a id="btnDelete"></a>');

        addPersonPay.addClass('modal-pay__card-product');
        photoPersonPay.addClass('modal-add-applicant__photo');
        typeVisaPay.addClass('modal-pay__name-visa');
        priceVisaPay.addClass('modal-pay__price');
        btnDelete.addClass('modal-pay__link-delete');
        photoPersonPay.attr('src', '/static/images/modal-add-applicant-photo.svg');
        if (radioTypeVisa[0].checked) {
            typeVisaPay.html(titleTourismNormal.text() + '<span>7 Werktage Bearbeitung</span>');
            priceVisaPay.text('€149');
        } else if (radioTypeVisa[1].checked) {
            typeVisaPay.html(titleTourismExpress.text() + '<span>2 Werktage Bearbeitung</span>');
            priceVisaPay.text('€220');
        } else if (radioTypeVisa[2].checked) {
            typeVisaPay.html(titleBusinessNormal.text() + '<span>7 Werktage Bearbeitung</span>');
            priceVisaPay.text('€199');
        } else if (radioTypeVisa[3].checked) {
            typeVisaPay.html(titleBusinessExpress.text() + '<span>2 Werktage Bearbeitung</span>');
            priceVisaPay.text('€350');
        }
        addPersonPay.append(photoPersonPay);
        addPersonPay.append(typeVisaPay);
        addPersonPay.append(priceVisaPay);
        addPersonPay.append(btnDelete);
        personsPayList.append(addPersonPay);
    }

    /**
     * Вешает обработчик событий на кнопку next страницы с анкетой
     */
    function initializesAddPersonEvents() {
        questionnaireNextButton.on('click', addPersons);
    }

    initializesRadioEvents();
    initializesImgEvents();
    initializesMaskEvents();
    initializesRequestQuestionnaireEvents();
    initializesStartPageEvents();
    initializesCancelButtonEvents();
    initializesNextAndBackButtonEvents();
    initializesAddPersonEvents();
})



