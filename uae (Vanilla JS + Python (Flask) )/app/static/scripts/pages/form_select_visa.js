$(function () {
    const mainSelectVisa = $('#mainSelectVisa'); // Селект
    const modalStartPage = $('#modalStartPage'); // Стартовая страница модального окна
    const totalPrice = $('#totalPrice'); // Полная цена в модальном окне
    const stylizedRadioTourismStandard = $('#stylizedRadioTourismStandard'); // Стилизованная радио кнпока стандартной туристической визы
    const stylizedRadioTourismExpress = $('#stylizedRadioTourismExpress'); // Стилизованная радио кнпока быстрой туристической визы
    const stylizedRadioBusinessStandard = $('#stylizedRadioBusinessStandard'); // Стилизованная радио кнпока стандартной деловой визы
    const stylizedRadioBusinessExpress = $('#stylizedRadioBusinessExpress'); // Стилизованная радио кнпока быстрой деловой визы
    const radioTourismStandard = $('#radioTourismStandard'); // Радио кнпока стандартной туристической визы
    const radioTourismExpress = $('#radioTourismExpress'); // Радио кнпока быстрой туристической визы
    const radioBusinessStandard = $('#radioBusinessStandard'); // Радио кнпока стандартной деловой визы
    const radioBusinessExpress = $('#radioBusinessExpress'); // Радио кнпока быстрой деловой визы
    const darkMask = $('#darkMask'); // Черная маска
    const priceTourismStandard = $('#priceTourismStandard'); // Блок с ценой стандартной туристической визы
    const priceTourismExpress = $('#priceTourismExpress'); // Блок с ценой быстрой туристической визы
    const priceBusinessStandard = $('#priceBusinessStandard'); // Блок с ценой стандартной деловой визы
    const priceBusinessExpress = $('#priceBusinessExpress'); // Блок с ценой быстрой деловой визы
    const mainSelectVisaContainer = $('#select2-mainSelectVisa-container'); // контейнер select2
    const stylizedSelect = $('[aria-controls="select2-mainSelectVisa-container"]'); // Стилизованный селект

    /**
     * Переключает стилизованный селект и показывает маску
     */
    function toggleStylizedSelectAndShowMask() {
        mainSelectVisaContainer.toggleClass('main-toggle-options');
        darkMask.addClass('page-mask');
    }

    /**
     * Вешает обработчик событий на стилизованный селект
     */
    function initializesStylizedSelectEvents() {
        stylizedSelect.on('click', toggleStylizedSelectAndShowMask);
    }

    /**
     * Скрывает стилизованный селект и возвращает текст селекта в дефолтное состояние
     */
    function hideStylizedSelect() {
        mainSelectVisaContainer.removeClass('main-toggle-options');
        mainSelectVisaContainer.text('Select your visa now');
    }

    /**
     * Вешает обработчик событий на маску
     */
    function initializesMaskEvents() {
        darkMask.on('click', hideStylizedSelect);
    }

    /**
     * Показывает модальное окно, маску и ставит значение радио на соответсвующие выбранному в селекте
     */
    function ShowModalAndMask() {
        modalStartPage.css({
            right: 0
        });
        darkMask.addClass('mask');
        mainSelectVisaContainer.removeClass('main-toggle-options');
        if (mainSelectVisaContainer.text() === 'Tourismus Visum') {
            radioTourismStandard.prop('checked', true);
            stylizedRadioTourismStandard.addClass('checked');
            stylizedRadioTourismExpress.removeClass('checked');
            stylizedRadioBusinessStandard.removeClass('checked');
            stylizedRadioBusinessExpress.removeClass('checked');
            totalPrice.text('149 EUR');

        } else if (mainSelectVisaContainer.text() === 'Tourismus - Express Visum') {
            radioTourismExpress.prop('checked', true);
            stylizedRadioTourismExpress.addClass('checked');
            stylizedRadioTourismStandard.removeClass('checked');
            stylizedRadioBusinessStandard.removeClass('checked');
            stylizedRadioBusinessExpress.removeClass('checked');
            totalPrice.text('220 EUR');

        } else if (mainSelectVisaContainer.text() === 'Business Visum') {
            radioBusinessStandard.prop('checked', true);
            stylizedRadioBusinessStandard.addClass('checked');
            stylizedRadioTourismExpress.removeClass('checked');
            stylizedRadioTourismStandard.removeClass('checked');
            stylizedRadioBusinessExpress.removeClass('checked');
            totalPrice.text('199 EUR');

        } else if (mainSelectVisaContainer.text() === 'Business - Express Visum') {
            radioBusinessExpress.prop('checked', true);
            stylizedRadioBusinessExpress.addClass('checked');
            stylizedRadioTourismExpress.removeClass('checked');
            stylizedRadioBusinessStandard.removeClass('checked');
            stylizedRadioTourismStandard.removeClass('checked');
            totalPrice.text('350 EUR');
        }
    }

    /**
     * Вешает обработчик событий на селект
     */
    function initializesSelectEvents() {
        mainSelectVisa.on('change', ShowModalAndMask);
    }

    /**
     * Показывает модальное окно, маску и ставит значение радио на соответсвующие выбранному в селекте
     */
    function hideOptionAndShowModalWindow(radio, customRadioOne, customRadioTwo, customRadioThree, customRadioFour) {
        return function () {
            modalStartPage.css({
                right: 0
            });
            mainSelectVisaContainer.text('Select your visa now');
            darkMask.addClass('mask');
            darkMask.addClass('page-mask');
            radio.prop('checked', true);
            customRadioOne.addClass('checked');
            customRadioTwo.removeClass('checked');
            customRadioThree.removeClass('checked');
            customRadioFour.removeClass('checked');
        };
    }

    /**
     * Вешает обработчик событий на блок с ценой
     */
    function initializesPriceEvents() {
        priceTourismStandard.on('click', hideOptionAndShowModalWindow(radioTourismStandard, stylizedRadioTourismStandard, stylizedRadioTourismExpress, stylizedRadioBusinessStandard, stylizedRadioBusinessExpress));
        priceTourismExpress.on('click', hideOptionAndShowModalWindow(radioTourismExpress, stylizedRadioTourismExpress, stylizedRadioTourismStandard, stylizedRadioBusinessStandard, stylizedRadioBusinessExpress));
        priceBusinessStandard.on('click', hideOptionAndShowModalWindow(radioBusinessStandard, stylizedRadioBusinessStandard, stylizedRadioTourismStandard, stylizedRadioTourismExpress, stylizedRadioBusinessExpress));
        priceBusinessExpress.on('click', hideOptionAndShowModalWindow(radioBusinessExpress, stylizedRadioBusinessExpress, stylizedRadioTourismStandard, stylizedRadioTourismExpress, stylizedRadioBusinessStandard));
    }

    initializesStylizedSelectEvents();
    initializesMaskEvents();
    initializesSelectEvents();
    initializesPriceEvents();
});
