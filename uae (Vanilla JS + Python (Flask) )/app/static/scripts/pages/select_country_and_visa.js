$(function () {
    const selectCountry = $('#selectCountry'); // Селект с выбором страны
    const stylizedSelectCountry = $('[aria-controls="select2-selectCountry-container"]'); // Стилизованный селект с выбором страны
    const selectVisa = $('#selectVisa'); // Селект с выбором типа визы
    const stylizedSelectVisa = $('[aria-controls="select2-selectVisa-container"]'); // Стилизованный селект с выбором типа визы
    const selectButtonCountryAndVisa = $('#selectButtonCountryAndVisa'); // Кнопка селекта
    const darkMask = $('#darkMask');
    const sectionSelectCountryAndVisa = $('#sectionSelectCountryAndVisa');
    const sectionInnerSelectCountryAndVisa = $('#sectionInnerSelectCountryAndVisa');

    /**
     * Переключает стилизованный селект и показывает маску
     */
    function toggleStylizedSelectCountryAndShowMask() {
        stylizedSelectCountry.toggleClass('toggle-options');
        darkMask.addClass('page-mask');
        sectionSelectCountryAndVisa.removeAttr('data-select2-id');
        sectionInnerSelectCountryAndVisa.removeAttr('data-select2-id');
    }

    /**
     * Вешает обработчик событий на стилизованный селект с страной
     */
    function initializesStylizedSelectCountryEvents() {
        stylizedSelectCountry.on('click', toggleStylizedSelectCountryAndShowMask);
    }

    /**
     * Скрывает стилизованный селект, открывает селект с типом визы и скрывает маску
     */
    function hideStylizedSelectCountryAndOpenSelectVisaAndHideMask() {
        stylizedSelectCountry.removeClass('toggle-options');
        stylizedSelectVisa.addClass('show-options');
        selectVisa.removeAttr('disabled');
        darkMask.removeClass('page-mask');
        initializesStylizedSelectVisaAndMaskEvents();
    }

    /**
     * Вешает обработчик событий на селект с страной
     */
    function initializesSelectCountryEvents() {
        selectCountry.on('change', hideStylizedSelectCountryAndOpenSelectVisaAndHideMask);
    }

    /**
     * Скрывает стилизованный селект стран
     */
    function hideStylizedSelectCountry() {
        stylizedSelectCountry.removeClass('toggle-options');
        stylizedSelectVisa.toggleClass('toggle-options-visa');
        darkMask.addClass('page-mask');
    }

    /**
     * Вешает обработчик событий на стилизованный селект с типом визы и маску
     */
    function initializesStylizedSelectVisaAndMaskEvents() {
        stylizedSelectVisa.on('click', hideStylizedSelectCountry);
    }

    /**
     * Открывает возможность действия с кнопкой
     */
    function openButton () {
        selectButtonCountryAndVisa.removeAttr('disabled');
        selectButtonCountryAndVisa.removeClass('--closed-btn');
        stylizedSelectVisa.removeClass('toggle-options-visa');
        darkMask.removeClass('page-mask');
    }
    /**
     * Вешает обработчик событий на селект с типом визы
     */
    function initializesSelectVisaEvents() {
        selectVisa.on('change', openButton);
    }

    function hideStylizedSelectCountryAndVisa() {
        stylizedSelectCountry.removeClass('toggle-options');
        stylizedSelectVisa.removeClass('toggle-options-visa');
    }

    darkMask.on('click', hideStylizedSelectCountryAndVisa);

    initializesStylizedSelectCountryEvents();
    initializesSelectCountryEvents();
    initializesSelectVisaEvents();

});

