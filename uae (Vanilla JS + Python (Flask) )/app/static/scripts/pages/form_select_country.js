$(function () {
    const mainCountryForm = $('#mainCountryForm');
    const stylizedSelect = $('[aria-controls="select2-mainSelectCountry-container"]'); // Стилизованный селект
    const mainSelectCountryContainer = $('#select2-mainSelectCountry-container'); // Контейнер select2
    const selectButton = $('#selectButton'); // Кнопка селекта
    const mainSelectCountry = $('#mainSelectCountry'); // Селект
    const darkMask = $('#darkMask'); // Черная маска
    /**
     * Переключает анимацию селекта, показывает маску и скрывает кнопку
     */
    function toggleCustomSelectAndMaskHideButton() {
        mainSelectCountryContainer.toggleClass('main-toggle-options');
        selectButton.removeClass('show-button');
        darkMask.addClass('page-mask');
        mainCountryForm.css({
            'border-color': '#7bcbc3',
            transition: '0.2s'
        });
    }

    /**
     * Вешает обработчик событий на стилизованный селект
     */
    function initializesCustomSelectEvents() {
        stylizedSelect.on('click', toggleCustomSelectAndMaskHideButton);
    }

    /**
     * Закрывает селект, скрывает маску и показывает кнопку
     */
    function hideCustomSelectAndMaskShowButton() {
        mainSelectCountryContainer.removeClass(['main-toggle-options', 'placeholder']);
        darkMask.removeClass('page-mask');
        selectButton.addClass('show-button');
        mainCountryForm.css({
            'border-color': '#D6EFEC',
            transition: '0.2s'
        });
        mainSelectCountryContainer.addClass('margin');
    }

    /**
     * Вешает обработчик событий на селект
     */
    function initializesSelectEvents() {
        mainSelectCountry.on('change', hideCustomSelectAndMaskShowButton);
    }


    /**
     * Показывается кнопку
     */
    function showButton() {
        selectButton.addClass('--select-button');
        selectButton.html('<span class="loader"><span></span></span><p>PRÜFUNG LÄUFT</p>');
        setTimeout(() => {
            selectButton.attr('type', 'submit');
            selectButton.click();
        }, 5000);
    }

    /**
     * Вешает обработчик событий на кнопку
     */
    function initializesButtonEvents() {
        selectButton.on('click', showButton);
    }

    /**
     * Скрывает маску и option, изменяет текст у селекта
     */
    function hideMaskAndOptionsModificationTextSelect() {
        darkMask.removeClass('page-mask');
        mainSelectCountryContainer.removeClass('main-toggle-options');
        mainSelectCountryContainer.text('Staatsangehörigkeit auswählen');
        mainSelectCountryContainer.addClass('placeholder');
    }

    /**
     * Вешает обработчик событий на маску
     */
    function initializesMaskEvents() {
        darkMask.on('click', hideMaskAndOptionsModificationTextSelect);
    }

    initializesCustomSelectEvents();
    initializesSelectEvents();
    initializesButtonEvents();
    initializesMaskEvents();

});

function CustomizeCountryOption(state) {
    if (!state.id) {
        return state.text;
    }

    const iso2code = state.element.value.toLowerCase();
    return $(
        `<span id="spanFlag" class="remove-after">
                <img alt="${iso2code}" src="/static/libs/countries/${iso2code}.svg"
                 class="img-flag" />
                 ${state.text}
            </span>`
    );
}

$('#mainSelectCountry').select2({
    selectionCssClass: 'main__custom-select',
    placeholder: 'Staatsangehörigkeit auswählen',
    dropdownParent: $('#mainCountryForm'),
    dropdownCssClass: 'main__custom-option',
    templateResult: CustomizeCountryOption,
    templateSelection: CustomizeCountryOption,
    dropdownPosition: 'below'
});
