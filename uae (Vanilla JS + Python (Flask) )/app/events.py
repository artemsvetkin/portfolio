dp_world_tour = dict(
    name_class = 'dp_world_tour',
    url = '/event/dp_world_tour/',
    title = 'DP World Tour Championship',
    from_time = '17',
    to_time = '20',
    month = 'Nov',
    text = dict(
        start = 'Beim großem Finale der European Tour, der Dubai World Tour Championship, treten erneut die weltbesten Golfer gegeneinander an.',
        midle = 'Die besten 50 Spieler treten auf den Grüns der Earth Courses der Jumeirah Golf Estates gegeneinander an. Begehrte Trophäen und hohe Preisgelder erwarten den Gewinner bei diesem lang erwarteten Sportereignis, das seit über einem Jahrzehnt ein Highlight im Golfkalender von Dubai ist.',
        end = 'Doch nicht nur Teilnehmer, sondern auch Besucher können sich hier auf ein ganz besonderes Erlebnis freuen.'
    ),
    image = '/static/images/events/dp_world_tour_championship.jpeg'
)

downtown_design = dict(
    name_class = 'downtown_design',
    url = '/event/downtown_design/',
    title = 'Downtown Design',
    from_time = '09',
    to_time = '12',
    month = 'Nov',
    text = dict(
        start = 'Downtown Design ist die einzige Ausstellung im Nahen Osten, die sich innovativem, hochwertigem Design widmet. Die einwöchige Show zieht Designbegeisterte und Fachleute aus allen Lebensbereichen an, von Innenarchitektur und Architektur bis hin zu Einzelhandel und Gastgewerbe.',
        midle = 'Studenten, Studios und Unternehmen werden zeitgenössische Stücke in limitierter Auflage ausstellen und Besucher aus dem Nahen Osten, Afrika und Südasien einladen, gemeinsam zukünftige Designs zu feiern.',
        end = 'Darüber hinaus werden jedes Jahr neue Produkte bekannter Marken auf der Messe präsentiert, einschließlich eines beeindruckenden interaktiven Erlebnisses. Genießen Sie die vielen Exponate, die von Möbeln und Teppichen bis hin zu Beleuchtung und Haushaltswaren reichen.'
    ),
    image = '/static/images/events/downtown_design.jpeg'
)

international_boat_show = dict(
    name_class = 'international_boat_show',
    url = '/event/international_boat_show/',
    title = 'Dubai International Boat Show',
    from_time = '09',
    to_time = '13',
    month = 'Mar',
    text = dict(
        start = 'Seit ihrer Premiere vor 28 Jahren hat sich die Dubai International Boat Show zu einem der wichtigsten Events der Branche entwickelt. Die Messe zeigt Luxus- und Freizeityachten aus der ganzen Welt und bietet Käufern, Verkäufern und Yachtliebhabern die Möglichkeit, neue Entwicklungen und Highlights zu entdecken. ',
        midle = '2022 findet im brandneuen Dubai Harbour statt, der das größte Terminal in der Region sein wird. Hier haben Sie die Möglichkeit, faszinierende Schiffe zu bestaunen und sich über die neuesten Entwicklungen in Technik und Design zu informieren.',
        end = 'Egal, ob Sie ein Boot kaufen oder als Aussteller nach neuen Kunden suchen, die Dubai International Boat Show ist eine Veranstaltung, die Sie nicht verpassen sollten.'
    ),
    image = '/static/images/events/dubai_international_boat_show.jpg'
)

eid_al_fitr = dict(
    name_class = 'eid_al_fitr',
    url = '/event/eid_al_fitr/',
    title = 'Eid Al Fitr',
    from_time = '02',
    to_time = '05',
    month = 'May',
    text = dict(
        start = 'Das Eid al Fitr Fest markiert das Ende des Ramadan, während Muslime auf der ganzen Welt von Sonnenaufgang bis Sonnenuntergang fasten. An diesem Wochenende finden in der ganzen Stadt Feiern, Veranstaltungen und Erlebnisse statt, bei denen garantiert für jeden etwas dabei ist.',
        midle = 'Mit hellen Lichtern und Feuerwerken, die jede Ecke von Dubai erleuchten, versammeln sich Familie und Freunde zu einem köstlichen Festmahl.',
        end = 'Kaufen Sie Schnäppchen ein, buchen Sie unschlagbare Unterkunftspakete und genießen Sie die Festlichkeiten während der Feiertage.'
    ),
    image = '/static/images/events/eid_al_fitr.jpeg'
)

electric_vehicle_innovation_summit = dict(
    name_class = 'electric_vehicle_innovation_summit',
    url = '/event/electric_vehicle_innovation_summit/',
    title = 'Electric Vehicle Innovation Summit 2022',
    from_time = '23',
    to_time = '25',
    month = 'May',
    text = dict(
        start = 'Der Electric Vehicle Innovation Summit (EVIS) ist eine jährlich stattfindende Ausstellung und Konferenz für Elektrofahrzeuge.',
        midle = 'Mit Masdar als Nachhaltigkeitspartner und dem Fokus der Regierung auf erneuerbare Energien und saubere Verkehrstechnologien wird EVIS den Übergang der Region zu Elektrofahrzeugen lenken. Mehr als 5.000 Fachleute, die führende Unternehmen der Elektrofahrzeugbranche vertreten, werden an der EVIS teilnehmen, um die neuesten Technologien zu kommunizieren und zu präsentieren.',
        end = 'EVIS ist insofern einzigartig, als es vernetzte Technologien in einer Veranstaltung zusammenbringt und es den Teilnehmern ermöglicht, sich über Wertschöpfungsketten hinweg zu vernetzen und neue Möglichkeiten an der Schnittstelle der Elektrofahrzeugtechnologie zu erkunden.'
    ),
    image = '/static/images/events/electric_vehicle_summit_2022.jpeg'
)

emirates_airline_dubai_rugby_sevens = dict(
    name_class = 'emirates_airline_dubai_rugby_sevens',
    url = '/event/emirates_airline_dubai_rugby_sevens/',
    title = 'Emirates Airline Dubai Rugby Sevens',
    from_time = '01',
    to_time = '03',
    month = 'Dec',
    text = dict(
        start = 'Das Emirates Dubai Rugby Sevens ist eine der beliebtesten Sportveranstaltungen der Stadt. Die Veranstaltung bringt die besten Männer- und Frauenmannschaften des internationalen Rugby zusammen und wird jedes Jahr größer.',
        midle = 'Spitzenspieler aus Neuseeland, Kenia, Südafrika und anderen Ländern stellten ihr Können an drei Tagen intensiver Wettkämpfe unter Beweis. Aber auch abseits des Platzes wartet jede Menge Action auf dich. Genießen Sie köstliches Essen und nehmen Sie an familienfreundlichen Aktivitäten teil. Zu guter Letzt gibt es jedes Jahr ein Blockbuster-Live-Konzert, um die Veranstaltung zu einem gelungenen Abschluss zu bringen – 2019 kam beispielsweise Pop-Diva Kylie Minogue die Ehre zu.',
        end = 'Ob Sie ein Sportliebhaber sind, Live-Konzerte lieben oder einfach nur die perfekte Unterhaltung für ein erfolgreiches Wochenende suchen: Das Dubai Sevens ist der richtige Ort für Sie.'
    ),
    image = '/static/images/events/emirates_airline_dubai_rugby_sevens.jpeg'
)

hr_technology_summit = dict(
    name_class = 'hr_technology_summit',
    url = '/event/hr_technology_summit/',
    title = 'HR Technology Summit',
    from_time = '17',
    to_time = '19',
    month = 'Oct',
    text = dict(
        start = 'HRTech.abudhabi ist einzigartig darin, miteinander verbundene Technologien in einer Veranstaltung zusammenzubringen und den Teilnehmern die Möglichkeit zu bieten, sich über Wertschöpfungsketten hinweg zu vernetzen und neue Möglichkeiten an der Schnittstelle von IHRM-Technologien zu erkunden.',
        midle = 'Ziel der Kampagne ist es, internationale Investoren und qualifizierte Arbeitnehmer aus den Emiraten anzuziehen, um die Emiratisierungspolitik der Regierung von Abu Dhabi zu fördern.',
        end = 'Von den HR-Führungskräften von heute wird erwartet, dass sie Erfahrungen sammeln und eine Kultur fördern, die Innovation, Inklusion, Wohlbefinden, Leistung, Empowerment und vor allem das Engagement der Mitarbeiter vorantreibt.'
    ),
    image = '/static/images/events/hr_technology_summit_2022.jpeg'
)

international_indian_film_academy = dict(
    name_class = 'international_indian_film_academy',
    url = '/event/international_indian_film_academy/',
    title = 'International Indian Film Academy (IIFA) Weekend & Awards',
    from_time = '20',
    to_time = '21',
    month = 'May',
    text = dict(
        start = 'Nach 2 Jahren ist die International Indian Film Academy (IIFA) zurück! Die 22. IIFA findet mit Bollywood-Superstar Salman Khan auf Yas Island in Abu Dhabi statt.',
        midle = 'Erleben Sie die glamouröseste Preisverleihung, bei der die Stars zusammenkommen, um die Welt zum Strahlen zu bringen.',
        end = 'Machen Sie sich bereit für ein Wochenende voller Musik, Film, Mode und Kultur mit der indischen Filmindustrie. Die Veranstaltung wird zahlreiche Veranstaltungen und Aktivitäten beherbergen, um die indische Filmindustrie zu feiern und willkommen zu heißen.'
    ),
    image = '/static/images/events/international_indian_film_academy.jpeg'
)

adihex = dict(
    name_class = 'adihex',
    url = '/event/adihex/',
    title = 'ADIHEX 2022',
    from_time = '26',
    to_time = '02',
    from_month = 'Sap',
    to_month = 'Oct',
    text = dict(
        start = "Die Abu Dhabi International Hunting and Equestrian Exhibition (ADIHEX) ist eine jährliche Veranstaltung, die von Seiner Hoheit Scheich Hamdan Bin Zayed Al Nahyan, Vertreter des Herrschers der Region Al Dhafra und Vorsitzender des Emirates Falconers' Club, gesponsert wird. ",
        midle = 'Dies ist die größte Veranstaltung ihrer Art im Nahen Osten und in Afrika und hat sich zu einer angesehenen globalen Plattform entwickelt, die es den Teilnehmern ermöglicht, Händler und Partner zu finden, ihr Geschäft auszubauen und ihre neuesten Produkte zu präsentieren und zu bewerben.',

    ),
    image = '/static/images/events/adihex_2022.jpeg'
)
