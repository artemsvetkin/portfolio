from flask import Flask
# from .routes import set_app_routes
from app.routes import set_app_routes

app = Flask(__name__)
app.secret_key = "super secret key"
set_app_routes(app)

app.debug = True

