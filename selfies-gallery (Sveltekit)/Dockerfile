FROM oven/bun:1.0.26 AS base

WORKDIR /app

COPY app /app

RUN bun install
RUN bun run build

FROM oven/bun:1.0.26 AS runtime

WORKDIR /app

COPY --from=base /app/package.json /app/bun.lockb ./
RUN bun install --production

COPY --from=base /app/build ./build
COPY --from=base /app/.svelte-kit ./.svelte-kit
COPY --from=base /app/static ./static
COPY --from=base /app/.env ./

RUN addgroup --gid 13344 appuser && \
    adduser --disabled-password --gecos "" --uid 13344 --ingroup appuser appuser

USER appuser

EXPOSE 3000

CMD ["bun", "build/index.js"]
