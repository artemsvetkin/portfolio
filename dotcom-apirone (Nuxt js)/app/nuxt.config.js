const config = {
  // Global page headers: https://go.nuxtjs.dev/config-head
  target: 'static',
  head: {
    title: 'Apirone',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: ''},
      {name: 'format-detection', content: 'telephone=no'},
      {hid: 'og-type', property: 'og:type', content: 'website'},
      {hid: 'og-site_name', property: 'og:site_name', content: 'Apirone'},
      {
        hid: 'og-image',
        property: 'og:image',
        content: 'https://apirone.com/static/og/apirone_og.png'
      },
      {hid: 't-card', name: 'twitter:card', content: 'summary_large_image'},
      {hid: 't-image', name: 'twitter:image', content: 'https://apirone.com/static/og/apirone_twitter.jpg'},
      {hid: 't-image-src', name: 'twitter:image:src', content: 'https://apirone.com/static/og/apirone_twitter.jpg'}
    ],
    link: [
      {rel: 'stylesheet', href: '/css/variables.css'},
      {rel: 'stylesheet', href: '/css/button.css'},
      {rel: 'stylesheet', href: '/css/list.css'},
      {rel: 'stylesheet', href: '/css/container.css'},
      {rel: 'stylesheet', href: '/css/integrations.css'},
      {rel: 'stylesheet', href: '/css/partners.css'},
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
      /* {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css2?family=Noto+Sans:ital,wght@0,400;0,700;1,400;1,700&family=Quicksand:wght@300&display=swap",
      }, */
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'apricons/dist/apricons.css',
    '~assets/scss/app.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {src: '~plugins/aos.js', ssr: false},
    { src: '~/plugins/redirects.client.js', mode: 'client' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/eslint-module'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    ['bootstrap-vue/nuxt', {css: false}],
    ['nuxt-i18n', {
      locales: [
        /* {
          name: 'Русский',
          code: 'ru',
          iso: 'ru-RU',
          file: 'ru-RU.js'
        }, */
        {
          name: 'English',
          code: 'en',
          iso: 'en-US',
          file: 'en-US.js'
        }
      ],
      langDir: 'lang/',
      defaultLocale: 'en'
    }],
    ['@nuxtjs/axios'],
    '@nuxtjs/sitemap',
    ['@nuxtjs/robots']
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  axios: {
    // proxy: true
  },

  loading: {
    color: '#5d8ab9',
    height: '2px'
  },

  sitemap: {
    hostname: process.env.SITEMAP_HOSTNAME || 'https://apirone.com',
    gzip: true
  },

  // serverMiddleware: [
  //   '~/middleware/redirects.js'
  // ],

  //  router: {
  //    middleware: 'errors'
  //  },

  robots: {
    UserAgent: '*',
    Disallow: '/api/',
    Allow: '/',
    Disallow: [
      '/api/',
      '/btc/',
      '/dashboard/'
    ],
    Host: 'https://apirone.com',
    Sitemap: 'https://apirone.com/sitemap.xml',
  },

  env: {
    serviceUrl: process.env.SITEMAP_HOSTNAME || 'https://apirone.com'
  }
}

if (process.env.VUE_APP_ENVIROMENT === 'production') {
  config.plugins.push({src: '~plugins/google-analytics.js', mode: 'client'})
  config.modules.push(['@nuxtjs/yandex-metrika'], '@nuxtjs/gtm')
  config.publicRuntimeConfig = {
    yandexMetrika: {
      id: process.env.VUE_APP_YANDEX_METRIKA_ID || 'development',
      webvisor: true,
      clickmap: true,
      ecommerce: 'dataLayer',
      trackLinks: true,
      accurateTrackBounce: true
    }
  }
  config.gtm = {
    id: process.env.VUE_APP_GTM || 'development',
    pageTracking: true
  }
  config.head.meta.push({name: 'yandex-verification', content: '861cfd6a52bf0ce6'})
}

export default config;
