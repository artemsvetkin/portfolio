const data = [
  /* {
    name: 'Вадим Сайлов',
    heading: 'Очень хороший стабильный сервис',
    text: 'Очень хороший стабильный сервис, на все ваши вопросы грамотно отвечает тех. поддержка... Конечно же 5 звезд тут не хватает я бы добавил еще пару тысяч) <br> По моему опыту, я еще встречал такую поддержку как на данном сервисе. (Слов нет) <br> Спасибо что вы есть, надеюсь на процветание вашего проекта. И отдельное огромное спасибо технической поддержки.',
    stars: 5,
    img: 'https://user-images.trustpilot.com/5bb561a74de5666d34236cf9/73x73.png',
  }, */
  {
    name: 'Denis Mikhaylin',
    heading: 'Very usefull processing service',
    text: 'Very usefull processing service. Try to increase your income at your e-commerce shop. Earn more with cryptopayments at your web site. Apirone offer very easy plug and play installation way. It is under constant modification and getting better. Try!',
    stars: 5,
    img: 'https://user-images.trustpilot.com/5b76d3db4de5666d34ed431b/73x73.png'
  },
  {
    name: 'Simon',
    heading: 'Of the many services on the Internet!',
    text: 'Of the many services on the Internet - we chose this service for the ease of use of the plugin and excellent technical support. We are happy to work with these guys. Recommend!',
    stars: 5,
    img: 'default'
  },
  {
    name: 'Alex B.',
    heading: 'Excellent service!',
    text: 'Excellent service. The technical support team is very responsive, they promptly answered all my questions and wished a good day. <br> The only thing missing is support for some cryptocurrencies. Please add Ethereum support and everything will be just perfect!',
    stars: 4,
    img: 'default'
  }
]

export default function getData () {
  return data
}
