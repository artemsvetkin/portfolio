module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: '@babel/eslint-parser',
    requireConfigFile: false
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended'
  ],
  rules: {
    'vue/no-unused-components': 'off',
    'vue/component-definition-name-casing': 'off',
    'vue/no-v-html': 'off',
    'vue/html-self-closing': 'off',
    'vue/no-v-text-v-html-on-component': 'off',
    'vue/multi-word-component-names': 'off',
    'vue/no-deprecated-filter': 'off',
    'vue/no-reserved-component-names': 'off',
    'no-tabs': 'off',
    'new-cap': 'off',
    'space-before-function-paren': 'off',
    'object-curly-spacing': 'off',
    'vue/html-closing-bracket-spacing': 'off',
    semi: 'off'
  }
}
