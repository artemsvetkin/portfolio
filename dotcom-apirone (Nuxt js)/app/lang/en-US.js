export default {
  banner: {
    title: 'Сrypto Payment gateway',
    list: [
      {
        name: 'Isolated addresses and wallets',
        text: 'Your crypto funds are isolated from other customers. You receive and withdraw cryptocurrency within your personal wallet.'
      },
      {
        name: 'Effortless installation',
        text: 'Easy-to-use cryptocurrency processor with seamless integration provided by RESTful API'
      }
    ]
  },
  digital: {
    title: 'Digital ART',
    text: 'Visualization of the current blockchain state.'
  },
  docs: {
    full: 'Documentation',
    short: 'Docs'
  },
  accept_crypto: {
    title: 'Solution'
  },
  cases: {
    gaming: {
      title: 'Gaming & Gambling',
      text: 'Connect crypto to your services.'
    },
    adult: {
      title: 'Adult content',
      text: 'Safe with crypto instead of high risk card processing.'
    },
    marketing: {
      title: 'Marketing Agency',
      text: 'Accept payments for free and make bulk payouts to your advertisers.'
    },
    ecommerce: {
      title: 'E-Commerce',
      text: 'Use crypto forwarding for CMS store.'
    },
    forex: {
      title: 'Forex, Binary options',
      text: 'Ideal coin payment solution for the trading and gambling industry. Accept crypto and do mass payment.'
    },
    file: {
      title: 'File Sharing',
      text: 'Forward and split cryptocurrency to wallets.'
    },
    charity: {
      title: 'Charity & Donations',
      text: 'Collect money for free.'
    },
    messenger: {
      title: 'Telegram Bot & messenger',
      text: 'Build amazing apps using blockchain API.'
    }
  },
  bottom: {
    link1: 'Start Accepting Crypto today',
    link2: 'Explore Apirone API',
    link3: 'Get Started'
  },
  wallet: 'Dashboard',
  useCases: 'Typical use cases',
  feedback: 'Feedback',
  collaboration_integrators: 'Collaboration with API integrators',
  faq: {
    'FAQ: Signing up and Testing': [
      {
        id: 'register-in-system',
        title: 'What shall I do to register in the system?',
        text: 'When registering, it is enough to save your account/wallet ID and transfer_key. It is important to keep them in a safe place. You will use the account/wallet ID to create crypto addresses and invoices, and transfer_key to send payments. ' +
          'We do not need your mail, phone, documents, or selfies, they are not needed at all to use blockchains. ' +
          'Also, we do not need your transfer_key when troubleshooting, so please do not send it even to our support service.'
      },
      {
        id: 'no-access-to-settings',
        title: 'I logged in to my account, but I have no access to settings. Why?',
        text: 'To customize your settings you have to be not only logged but also authorized in your account. Authorization means you enter your transfer key. After this, you will have access to your settings'
      },
      {
        id: 'sandbox-testing',
        title: 'Is there a testing ground or any kind of sandbox?',
        text: 'Yes, there is. It is Bitcoin testnet. To use it in your account, top right, tick the “Show testnet” checkbox, then create a tbtc wallet or account.'
      },
      {
        id: 'get-coins-bitcoin-testnet',
        title: 'Where to get coins for Bitcoin testnet?',
        text: 'There are several websites where you can get test coins for free:' +
          '<br><a target="_blank" href="https://coinfaucet.eu/en/btc-testnet/?lid=apirone">Coinfaucet</a>' +
          '<br><a target="_blank" href="https://bitcoinfaucet.uo1.net?lid=apirone">Bitcoinfaucet</a>' +
          '<br><a target="_blank" href="https://testnet-faucet.com/btc-testnet/?lid=apirone">Testnet faucet</a>' +
          '<br><a target="_blank" href="https://kuttler.eu/en/bitcoin/btc/faucet/?lid=apirone">Kuttler</a>' +
          '<br><br>' +
          'The list may change because coins are free, and they are mined by blockchain enthusiasts. You can also write to us at <a href="mailto:support@apirone.com">support@apirone.com</a> and we will send you test coins. '
      },
      {
        id: 'handle-callbacks',
        title: 'How to handle callbacks? And why is it important to respond to requests?',
        text: 'With each confirmation of an incoming transaction, we make a callback via the link you provided when creating an <a target="_blank" href="https://apirone.com/docs/">address or wallet</a>. In the call, we convey transaction data (amount, hash, number of confirmations) and user parameters. If the amounts are small, then we recommend completing the payment on the 3rd confirmation, and for large amounts, as exchanges do, on the 6th confirmation. <br>' +
          'Never complete a payment on an unconfirmed transaction, users can make double-spending and you will never receive this payment. <br> ' +
          'As soon as you have completed the payment process on the side of your website, answer *ok* in the callback handler. So we note this completion and will no longer make a call on this transaction. Otherwise, you will continue to get them for 255 blocks after the first confirmation.' +
          '<br><img class="faq__img" src="/img/faq/faq-img3.png" alt="Faq3" />' +
          '<img class="faq__img" src="/img/faq/faq-img.png" alt="Faq" />' +
          '<img class="faq__img" src="/img/faq/faq-img2.png" alt="Faq2" /><br>' +
          'You can check the request to your website and the response to it by selecting the desired address in the dashboard and clicking the “Callback log” button. In this case, you must be authorized in the dashboard.'
      },
      {
        id: 'not-receiving-callbacks',
        title: 'I am not receiving callbacks, what should I do? ',
        text: 'To get started, check the link you provided when creating your crypto address. Open it in a browser and see what it returns. ' +
          '<ul class="faq__list"> <li>1. If your environment framework is on a personal computer and you have specified Localhost (127.0.0.1) or local network addresses, then it is technically impossible to make a callback. You need to open access to your resource from the global network. </li> ' +
          '<li>2. If you send coins between addresses inside a wallet, then we do not make callbacks, because we consider such transactions as an internal transfer (we also do not charge a service fee for transactions inside a wallet).</li> </ul> ' +
          'If you still do not get callbacks then write to us at <a href="mailto:support@apirone.com">support@apirone.com</a> and we will help you. '
      },
      {
        id: 'better-check-balance-changes',
        title: 'Why is it better to receive callbacks than to constantly check for balance changes?',
        text: 'Before the advent of crypto processing services, developers used to check address balance changes, thereby seeing that the payment had arrived. But this is a big mistake because as your project grows, the number of addresses increases and you need to make more and more requests. Thus, you overload your system, the API server, and your provider, who at some point begins to filter out these requests as an attack or spam. ' +
          '<br>We monitor several million addresses of our clients and as soon as we detect a transaction, we immediately inform your website about it. '
      },
      {
        id: 'bulk-payout',
        title: 'What is a bulk payout?',
        text: 'With the Apirone bulk payout mechanism, you can specify a list of recipients in one request, and we will complete the transfer. On the UTXO blockchain, we will combine the maximum number of outputs into a single transaction. And if this is not enough, then we will divide it into several transactions and send it ourselves. In this case, you will pay a fee once.'
      },
      {
        id: 'micro-payments-unprofitable',
        title: 'Why are micro payments so unprofitable? What is dust?',
        text: 'With the current cryptocurrency exchange rate and high network fees, sending a transaction becomes quite expensive and inefficient. For example, to send the equivalent of 50 cents, the network fee can be several dollars. Dust is the minimum allowed value of the cryptocurrency to send, declared in the blockchain protocol. The network simply will not accept the amount less.'
      }
    ],
    'FAQ: Cryptonomics': [
      {
        id: 'exchange-rate-constantly-change',
        title: 'Why does the exchange rate constantly change? How to mitigate losses?',
        text: 'The volatility of cryptocurrencies is the variation in their value, which can rise or fall by several percent in one trading session. There are no centralized regulators and only supply and demand matter on the open market. To mitigate losses from exchange rate fluctuations, you can add 3-5% when invoicing, and withdraw and convert to <a target="_blank" href="https://en.wikipedia.org/wiki/Fiat_money">fiat money</a> more often.'
      },
      {
        id: 'apirone-doesn\'t-convert-cryptocurrency-to-fiat',
        title: 'Why doesn\'t Apirone convert crypto to fiat?',
        text: 'Operating with cryptocurrency is allowed worldwide, however, each country has different legislation. It can include a ban on the purchase/sale, prohibitions on paying for goods and services in cryptocurrency, permission for all types of commercial activities, and various tax rates. We do not work with <a target="_blank" href="https://en.wikipedia.org/wiki/Fiat_money">fiat money</a>. You determine your business profile and choose a profitable exchange rate source for yourself.'
      },
      {
        id: 'how-to-switch-fixed-fees-to-percentage',
        title: 'How to switch fixed fees to percentage?',
        text: 'Click on Settings menu and switch fee policy. <br> ' +
          '<img style="max-width: 600px; width: 100%;" class="faq__img" src="/img/faq/faq-img4.png" alt="fixed fees to percentafe" />'
      }
    ],
    'FAQ: Transactions and Fees': [
      {
        id: 'mistake-transferred-litecoins-to-bitcoin-address',
        title: 'By mistake, I transferred litecoins to a bitcoin address. What can I do now?',
        text: 'Having a private key, you can import it into third-party applications and get access to coins of various blockchains. If this is our address, then write to the support service at <a href="mailto:support@apirone.com">support@apirone.com</a>, and we will help solve this problem. But if the address is not ours or you do not own its private key, then you need to contact the owner, otherwise, the money will be lost.'
      },
      {
        id: 'what-is-mempool',
        title: 'What is a mempool?',
        text: 'The mempool ("memory pool") contains unprocessed transactions that have not yet been included in the block. Although all transactions are equally distributed over the network, each Bitcoin node can have its set of transactions in the mempool. A node can remove transactions that are too heavy, with a low network fee, or the old ones from its list. It all depends on the settings.'
      },
      {
        id: 'fee-depend-on-transfer-amount',
        title: 'Does the fee depend on the transfer amount?',
        text: 'No, it doesn\'t. <br>' +
          'The balance of a Bitcoin address consists of the sum of incoming transactions (inputs). There can be several recipients in a transfer, i.e. outputs. Thus, a transaction consists of inputs and outputs, the so-called UTXO protocol (“Unspent Transaction Output”), which means the output of unspent transactions. You can make this simple formula: <br>' +
          '<b>(List of inputs + List of outputs + Transaction data and signature) * Network fee = Transaction fee </b>' +
          ' turns out that the size of the transfer amount almost does not affect the fee, since the number of inputs and outputs plays a key role.'
      },
      {
        id: 'transaction-fees-lower-in-other-wallet-services',
        title: 'Why are transaction fees lower in other wallet services?',
        text: 'This is a common misconception, as the cost of sending a transaction depends on two factors: its size in bytes and the cost of the network fee. <br>' +
          'If you had only one incoming transaction in your wallet and you sent the entire amount to another wallet, then the transaction will have 1 input and 1 output. The length of such a transaction will be minimal and when multiplied by the network fee, a small value will be obtained. <br>' +
          'However, if you received a lot of transactions every day, accumulated a huge number of them, and decided to transfer all the money, then the transaction would contain this entire huge list of coins, and the size of the transaction could reach up to several tens of kilobytes. So when multiplied by the network fee, you would get an expensive transaction.'
      },
      {
        id: 'transaction-fees-related-to-waiting-time',
        title: 'How are transaction fees related to waiting time?',
        text: 'A fee is a commission paid for transactions. It was introduced intentionally to prevent spam transactions that could slow down or clog the network. Users can control how quickly their transactions are processed by setting a fee higher than the current one. <br>' +
          'Miners receive a reward for the mined block + the network fee from each transaction. <br>' +
          'They prefer to choose easy transactions with a higher network fee so that a block would contain more of them to get the maximum income. This creates contention among transactions. The higher the network fee, the more priority your transaction will be with the miners and the sooner it will enter the next block.'
      },
      {
        id: 'can-network-fee-be-so-high',
        title: 'Why can the network fee be so high?',
        text: 'Sometimes there are situations in the blockchain when the network fee becomes unusually large. This does not happen often and is associated with non-trivial phenomena in the network, for example, the advent of BRC-20 standard tokens in May 2023, which led to an increase in the fee by several tens of times, when more than 500 thousand transactions got stuck in the mempool. Sooner or later the situation will stabilize and everything will be as before after such “hype” events, but sometimes it takes weeks to normalize.'
      },
      {
        id: 'why-is-the-service-fee-so-high',
        title: 'Why is the service fee so high? Can it be reduced?',
        text: 'We have a <a target="_blank" href="/pricing/">flexible fee policy</a>. If the service fee seems too expensive for you, it only means you have chosen the wrong fee plan. For example, if you usually transfer small amounts, the percentage fee plan suits you better than the fixed fee. You can choose the fee plan in your account in the Settings tab.'
      },
      {
        id: 'why-my-bitcoin-transactions-still-not-confirmed',
        title: 'Why are my Bitcoin transactions still not confirmed?',
        text: 'Bitcoin nodes recalculate the network fee based on the volume of raw transactions and their fees. It can both fall and grow at the moment (because users from all over the world constantly send new transfers), and it is extremely difficult to predict their change. There may be a surge in transactions on the network due to news about exchanges, hype events, or an increase in the price of crypto. <br>' +
          'You could send at one price, but a huge number of new transactions have appeared on the network that are more profitable for miners, and now yours is below priority and stuck for several hours or even days.'
      },
      {
        id: 'transaction-disappeared-from-the-blockchain-explorer',
        title: 'The transaction disappeared from the blockchain explorer, what should I do?',
        text: 'Explorers work directly with nodes. When there is a high load on the blockchain, transactions may disappear from the list of unconfirmed due to buffer settings in the node. Do not worry, the nodes constantly exchange messages and after a while, the entry will appear again. You can also rebroadcast transactions yourself through open services.'
      },
      {
        id: 'speed-up-the-writing-of-transaction-to-mempool',
        title: 'Can one do something extra to speed up the entry of the transaction into the mempool?',
        text: 'You don’t need to do anything additionally, we constantly re-send the transaction, so it remains only to wait patiently. The funds won’t be lost anywhere.'
      }
    ]

  },
  pricing: {
    top1: 'Forwarding transaction means one incoming transaction and one outgoing transaction.',
    top2: 'All technical APIs like Callback or Exchange rates are fully free.',
    top3: 'All incoming payments are totally free.',
    'fee-free-limit': 'Fee-free limit',
    'incoming-transactions': 'Incoming transactions',
    'regular-transactions': 'Regular',
    'forwarding-transactions': 'Forwarding'
  },
  features: [
    {
      icon: 'shuffle-solid.svg',
      title: 'Flexible fee policy',
      text: 'You can choose a convenient fee plan profitable for your payments (<a href="/pricing">percentage or fixed</a>).'
    },
    {
      icon: 'tower-broadcast-solid.svg',
      title: 'Permanent tracking of transactions',
      text: 'We permanently re-broadcast all your transactions to the mempool so the transactions will be confirmed sooner or later, and the coins will be sent to the recipient.'
    },
    {
      icon: 'shield-solid.svg',
      title: 'Safe and Secure',
      text: 'Our RESTful API uses SSL as a standard security technology for establishing an encrypted connection to Apirone’s payment gateway.'
    },
    {
      icon: 'user-secret-solid.svg',
      title: 'Anonymity',
      text: 'No need to worry about getting a merchant application declined or going through tedious signup processes. No email or phone required.'
    },
    {
      icon: 'tag-solid.svg',
      title: 'White label processing',
      text: 'There are no logos, cut-ins, external pages, or branded buttons. Use the API as if it’s your service, safe and stable.'
    },
    {
      icon: 'globe-solid.svg',
      title: 'Around the world',
      text: 'Apirone does business with a lot of customers around the world without any restrictions.'
    },
    {
      icon: 'list-ul-solid.svg',
      title: 'Mass payments',
      text: 'You can combine several receivers in one transfer to make mass payments.'
    },
    {
      icon: 'infinity-solid.svg',
      title: 'Unlimited number of addresses',
      text: 'Generate as many new cryptocurrency addresses as you wish.'
    },
    {
      icon: 'stopwatch-solid.svg',
      title: 'Instant Withdrawal',
      text: 'Cryptocurrencies deposited to the account or wallet are instantly ready for withdrawal.'
    },
    {
      icon: 'shield-halved-solid.svg',
      title: 'Isolated addresses and wallets',
      text: 'Your crypto funds are isolated from other customers. You can transfer coins from your desired addresses or withdraw them from your wallet.'
    },
    {
      icon: 'code-solid.svg',
      title: 'Free tech API',
      text: 'Callbacks, estimations, registration of new wallets/addresses, exchange rates, etc. are free. See our <a href="/docs/">documentation</a>.'
    },
    {
      icon: 'window-maximize-solid.svg',
      title: 'Dashboard',
      text: 'Create and manage a wallet using our convenient <a href="/dashboard">dashboard</a>.'
    }
  ],
  registered: 'Registred',
  blog: [
    {
      title: 'Child Pays For Parent',
      text: 'What the Child Pays For Parent (CPFP) method is all about.',
      link: 'cpfp'
    },
    {
      title: 'Forwarding process',
      text: 'Description of how the forwarding process works.',
      link: 'forwarding'
    },
    {
      title: 'Crypto Dictionary',
      text: 'Dictionary of blockchain and cryptocurrency words.',
      link: 'crypto-dictionary'
    },
    {
      title: 'Network fee',
      text: 'Why you need a network fee and recommendations on how to use it.',
      link: 'network-fee'
    },
    // {
    //   title: 'Donation Widget',
    //   text: 'The donation widget functionality is based on our API and uses all the power.',
    //   link: 'donation-widget',
    // },
    // {
    //   title: 'OpenCart plugin',
    //   text: 'Opencart is an open-source e-commerce platform that allows businesses to launch an online store in no time. OpenCart is a highly intuitive and well-structured solution based on PHP.',
    //   link: 'opencart',
    // },
    // {
    //   title: 'WooCommerce plugin',
    //   text: 'WooCommerce is an e-commerce system designed to help merchants launch an online shop easily and quickly using WordPress CMS.',
    //   link: 'woocommerce',
    // },
    {
      title: 'What is RESTful API?',
      text: 'A RESTful API is an application program interface (API) that uses HTTP requests to GET, PUT, POST and DELETE data.',
      link: 'restful-api'
    },
    {
      title: 'Cases of split payment',
      text: 'Using API forwarding V2, you can split income payment to different addresses by percent or fixed amounts.',
      link: 'cases-of-split-payment'
    },
    {
      title: 'Payment process',
      text: 'Let is look at a step-by-step detailed instruction on how to accept bitcoin payments in your own project.',
      link: 'payment-process'
    }
  ],
  /* integrations: [
    {
      title: 'Magento 2',
      link: 'magento-2',
    },
    {
      title: 'WooCommerce',
      link: 'woocommerce',
    },
    {
      title: 'Joomla Virtuemart',
      link: 'virtuemart',
    },
    {
      title: 'Opencart 3.x',
      link: 'opencart-3',
    },
    {
      title: 'Opencart 2.x',
      link: 'opencart-2',
    },
    {
      title: 'ocStore',
      link: 'ocstore',
    },
    {
      title: 'Simple Advertisement Board',
      link: 'bitcoin-simple-advertisement-board',
    },
    {
      title: 'Become a Supporter page/ Donations',
      link: 'become-a-supporter',
    },
  ], */
  integrations: [
    {
      title: 'WooCommerce',
      link: '/integrations/woocommerce'
    },
    {
      title: 'OpenCart',
      link: '/integrations/opencart'
    },
    {
      title: 'PrestaShop',
      link: '/integrations/prestashop'
    }
  ],
  pages: {
    index: {
      title: 'Home',
      description: 'Accept and withdraw crypto payments with easy-to-install solutions. The processor provides isolated wallets,  instant forwarding payments, and flexible fee policy.',
      keywords: 'Accept bitcoins, Crypto, forwarding, blockchain api v1, Processing, Acquiring, Payment Solutions, e-commerce, CMS, White label processing'
    },
    faq: {
      title: 'FAQ',
      description: 'Frequently Asked Questions',
      keywords: 'faq, f.a.q, problem with crypto, Bitcoin testnet, callback, '
    },
    pricing: {
      title: 'Pricing',
      description: 'Forwarding transaction implies one incoming transaction and one outgoing transaction. All technical APIs such as Callback or Exchange rates are fully free. All incoming payments are totally free.',
      keywords: 'pricing, accept bitcoin, ltc, bch, doge, api, fee, transfer, send, pay, masspayment, receive, crypto'
    },
    'how-to': {
      title: 'How to',
      description: 'Typical use cases of Apirone crypto service.',
      keywords: 'use cases, build apps, charity, donation, solution for the traiding, gambling industry, telegram bot'
    },
    features: {
      title: 'Features',
      description: 'Features of our product.',
      keywords: 'white label api, safe, secure, tor, anonym, callback, accept, crypto'
    },
    partners: {
      title: 'Partners',
      description: 'Our partners.',
      keywords: 'forex, binary options, file sharing, telegram bot, messenger, charity, donations, connect project, merchants, crypto catalog'
    },
    'about-us': {
      title: 'About us',
      description: 'Apirone is one of the simplest and most convenient services for receiving, processing, and sending payments in cryptocurrency. We have our own payment processing platform, which acts as the basis for the security and guarantees stability of your financial transactions.',
      keywords: 'simplest, services, processing, about apirone, Estonian, Europe, crypto, company, jurisdiction'
    },
    blog: {
      title: 'Blog',
      description: 'You can find information about blockchain and our products here.',
      keywords: 'RESTfull api, plugin, WooCommerce, HTTP request, wordpress, bitcoin payments, forwarding v2'
    },
    integrations: {
      'main-title': 'Plugins for CMS integration on website to accept crypto payments',
      title: 'Plugins for CMS integration',
      description: 'Plugins for seamless integration with crypto payment gateway on your website. Step-by-step installation instructions and 24/7 support ',
      keywords: 'crypto forwarding, account, wallet, cms plugins, cryptocurrency, integrations',
      text: '<a href="/blog/forwarding/">Forwarding payments</a>'
    },
    'using-cookies': {
      title: 'Using Cookies',
      description: 'Using Cookies policy. ',
      keywords: 'support cookies, browsers, the cookie-related information, secure'
    },
    charity: {
      title: 'Charity & Donations',
      description: 'Collect money for free.',
      keywords: 'charity, donations, crypto, donations in crypto'
    },
    'gaming-and-gambling': {
      title: 'Gaming & Gambling',
      description: 'Connecting cryptocurrency to gambling and gaming service',
      keywords: 'gambling, gaming, API, services'
    },
    'adult-content': {
      title: 'Adult content',
      description: 'Connecting cryptocurrency to adult content services',
      keywords: '18+, adult, adult content, API'
    },
    'marketing-agency': {
      title: 'Marketing Agency',
      description: 'Connection for marketing agencies',
      keywords: 'marketing, marketing agency, integration crypto in marketing agency, API'
    },
    'file-sharing': {
      title: 'File Sharing',
      description: 'Forward and split cryptocurrency to wallets.',
      keywords: 'create wallets, payment page, processing system, zero network confirmation,  the payment page. '
    },
    options: {
      title: 'Forex, Binary options',
      description: 'An ideal coin payment solution for the trading and gambling industry. Accept crypto and make mass payments.',
      keywords: 'Creating a wallet,  solution for the trading, mass payments, crypto, callback processing, create wallet, accept crypto'
    },
    telegram: {
      title: 'Telegram Bot & messenger',
      description: 'Build amazing apps using blockchain API.',
      keywords: 'messengers, bot, crypto, invoice for payment, cryptocurrency, saving wallet,  integration with bot, telegram, QR code for a customer'
    },
    'restful-api': {
      title: 'What is RESTful API?',
      description: 'A RESTful API is an application program interface (API) that uses HTTP requests to GET, PUT, POST, and DELETE data. The RESTful API is based on representational state transfer (REST) technology, an architectural style, and an approach to communications often used in web services development.',
      keywords: 'RESTful API, HTTP methods, HTTPS protocol, GET, PUT, POST, PATCH, HTTP status codes, response, JSON'
    },
    'forwarding-payments': {
      title: 'Forwarding payments',
      description: 'Information about forwarding payments.',
      keywords: 'forwarding, forwarding payments, API'
    },
    'cases-of-split-payment': {
      title: 'Cases of split payment',
      description: 'Examples of splitting payments.',
      keywords: 'splitting, payments, crypto, crypto payments, examples'
    },
    'network-fee': {
      title: 'Network fee',
      description: 'Why you need a network fee and recommendations on how to use it.',
      keywords: 'network fee, fee, crypto fee, bitcoin fee, apirone fee'
    },
    'payment-process': {
      title: 'Payment process',
      description: 'Let\'s look at a step- by - step detailed instruction on how to accept bitcoin payments in your project.',
      keywords: 'instruction, accept crypto payments, project, CMS e-commerce shop, bot messenger, new bitcoin address, blockchain, USD, EUR, telegram'
    },
    woocommerce: {
      'main-title': 'CMS integration on website for Woocommerce crypto payments',
      title: 'Woocommerce crypto payments',
      description: 'Accept payments in Wordpress. Step-by-step installation guide, 24/7 support, no signup, no country restrictions',
      keywords: 'e-commerce system, merchants, online shop, WordPress CMS, online businesses, plugin, accept cryptocurrencies, pay with cryptocurrency, QR code, multi cryptocurrency payments'
    },
    opencart: {
      'main-title': 'CMS integration on website for Opencart crypto payments',
      title: 'Opencart crypto payments',
      description: 'Accept payments in Opencart. Step-by-step installation guide, 24/7 support, no signup, no country restrictions',
      keywords: 'e-commerce system, merchants, online shop, WordPress CMS, online businesses, plugin, accept cryptocurrencies, pay with cryptocurrency, QR code, multi cryptocurrency payments'
    },
    prestashop: {
      title: 'Prestashop crypto payments',
      'main-title': 'CMS integration on website for PrestaShop crypto payments',
      description: 'Accept payments in PrestaShop.Step-by-step installation guide, 24/7 support, no signup, no country restrictions',
      keywords: 'e-commerce system, merchants, online shop, WordPress CMS, online businesses, plugin, accept cryptocurrencies, pay with cryptocurrency, QR code, multi cryptocurrency payments'
    },
    'privacy-policy': {
      title: 'Privacy policy',
      description: 'By visiting our website and accessing the information, resources, services, products, and tools we provide, you understand and agree to accept and adhere to the following terms and conditions as stated in this policy.',
      keywords: 'secure, accessing the information, email address, bitcoin address, log Information, device information, cookie'
    },
    gdpr: {
      title: 'Privacy policy (GDPR)',
      description: 'We are very delighted that you have shown interest in our enterprise. Data protection is of a particularly high priority for Apirone.',
      keywords: 'Data protection, website, content, blockchain network, personal data, data subject, collection of general data, registration on website'
    },
    'donation-widget': {
      title: 'Donation Widget',
      description: 'The donation widget functionality is based on Apirone API and uses all the power.',
      keywords: 'donation, widget, API, apirone, donation widget, apirone blog, how to',
      'text-case': 'An ultra simple vanilla JS based widget that can be used for any platform or service'
    },
    'crypto-dictionary': {
      title: 'Crypto Dictionary',
      description: 'Dictionary of blockchain and cryptocurrency words.',
      keywords: 'crypto, dictionary, API, apirone, crypto dictionary, apirone blog, how to'
    },
    'payments-button': {
      title: 'Payments button',
      description: 'Designer for creating a payment button.',
      keywords: 'crypto, constructor, API, apirone, payment button, payment, payment button API',
      'text-case': 'Make it easy for your customers to display a button on the product page to click and pay.',
      text: 'Use for <a target="_blank" href="https://www.squarespace.com/"><img style="padding: 0.5rem; width: auto; height: auto" class="payments-button__img" src="/img/payment-constructors/squarespace.svg" alt="Squarespace logo" /></a>' +
        '<a target="_blank" href="https://tilda.cc/"><img style="padding: 0.5rem; width: auto; height: auto" class="payments-button__img" src="/img/payment-constructors/tilda.svg" alt="Tilda logo" /></a>' +
        '<a target="_blank" href="https://webflow.com/"><img style="padding: 0.5rem; width: auto; height: auto" class="payments-button__img" src="/img/payment-constructors/webflow.svg" alt="Webflow logo" /></a>' +
        '<a target="_blank" href="https://squareup.com/us/en"><img style="padding: 0.5rem; width: auto; height: auto" class="payments-button__img" src="/img/payment-constructors/squareup.svg" alt="Squareup logo" /></a>' +
        '<a target="_blank" href="https://www.duda.co/"><img style="padding: 0.5rem; width: auto; height: auto" class="payments-button__img" src="/img/payment-constructors/duda.svg" alt="Duda logo" /></a>' +
        '<br> Or any <img style="padding: 0.5rem; width: 70px; height: auto" class="payments-button__img" src="/img/payment-constructors/htmlcssjs.png" alt="Html css js logo" />'
    },
    'payments-form': {
      title: 'Payments form',
      description: 'Designer for creating a payment form.',
      keywords: 'crypto, constructor, API, apirone, payment form, payment, payment form API',
      'text-case': 'Minimalistic and user-friendly method to accept crypto with customizable fields'
    },
    'mini-cart': {
      title: 'Mini Cart',
      description: 'Shopping widget.',
      keywords: 'crypto, constructor, API, apirone, shopping widget, mini cart, mini cart API',
      'text-case': 'A shortcut decision for making purchases online with enhanced functionality'
    },
    resources: {
      title: 'Resources',
      description: 'PHP API documentation',
      keywords: 'PHP, API, CMS, library'
    },
    brand: {
      title: 'Brand resources',
      description: 'All the information about our logo',
      keywords: 'Apirone logo, logo, brand, brand resources'
    },
    guidelines: {
      title: 'Guidelines',
      description: '',
      keywords: ''
    },
    cpfp: {
      title: 'Child Pays For Parent (CPFP)',
      description: 'What the Child Pays For Parent (CPFP) method is all about.',
      keywords: 'Apirone, btc, Child-Pays-for-Parent, CPFP, Apirone CPFP'
    }
  },
  'crypto-dictionary': [
    {
      id: 'altcoins',
      title: 'Altcoins',
      description: 'The term “altcoin” is shorthand for “alternative coins” and simply means cryptocurrencies other than Bitcoin. After Bitcoin, the nine most popular cryptocurrencies are as follows: Ethereum, XRP, Tether, Cardano, Polkadot, Stellar, Dogecoin, Litecoin…'
    },
    {
      id: 'blockchain-explorer',
      title: 'Blockchain explorer',
      description: 'A Blockchain Explorer is a tool or site that allows anyone to explore any public cryptocurrency\'s blockchain for blockchain network, transaction, block, addresses history, balance and other statistic information.'
    },
    {
      id: 'crypto-processing',
      title: 'Crypto processing',
      description: 'Crypto payment processing means processing transactions made in cryptocurrencies. Crypto processor is a service that allows merchants to accept payments in cryptocurrencies.'
    },
    {
      id: 'custodial-wallet',
      title: 'Custodial Wallet',
      description: 'A Custodial Wallet is defined as a wallet in which the private keys are held by a third party. It means the third party has full control over your funds while you only have to give permission to send or receive payments.'
    },
    {
      id: 'cold-storage',
      title: 'Cold storage',
      description: 'Cold storage is offline cryptocurrency storage. Any crypto wallet that\'s not connected to the internet is considered to be of the cold storage and is referred to as a cold wallet. The most common type of the cold wallet is a hardware wallet, which is typically a small device that connects to a computer.'
    },
    {
      id: 'cbdc',
      title: 'CBDC',
      description: 'Central Bank Digital Currencies are digital tokens, similar to cryptocurrency, issued by a central bank. They are linked to the value of that country\'s fiat currency.'
    },
    {
      id: 'double-spend',
      title: 'Double spend',
      description: 'Double Spending means to undertake, or attempt to undertake, two or more different transactions on the Bitcoin blockchain or any other digital asset network and spend the same account balance on each of the transactions. Usually, miners prefer more profitable transactions to add to the next block. After confirmation, other double spent transactions will be abandoned.',
      link: {
        title: 'wikipedia.org',
        url: 'https://en.wikipedia.org/wiki/Double-spending'
      }
    },
    {
      id: 'defi-tokens',
      title: 'DeFi tokens',
      description: 'A new world of cryptocurrency-based protocols that aim to reproduce traditional financial-system functions (lending and saving, insurance, trading) has emerged in recent years. These protocols issue tokens that perform a wide variety of functions but can also be traded or held like any other cryptocurrency.'
    },
    {
      id: 'faucet',
      title: 'Faucet',
      description: 'A Bitcoin faucet can be defined as a reward system from which small amounts of Bitcoin can be rewarded, known as Satoshi, the one-millionth unit of 1 Bitcoin (0.00000001 BTC). The reward system offers you Satoshis in exchange for solving captchas, clicking links or other simple tasks.'
    },
    {
      id: 'governance-tokens',
      title: 'Governance tokens',
      description: 'These are specialized DeFi tokens that give holders a say in the future of a protocol or app, which (being decentralized) don’t have boards of directors or any other central authority. The popular savings protocol Compound, for example, issues all users a token called COMP. This token gives holders a vote on how the Compound is upgraded. The more COMP tokens you have, the more votes you get.'
    },
    {
      id: 'private-key',
      title: 'Private key',
      description: 'A private key is a secret number used in cryptography, similar to a password. In cryptocurrency, private keys are also used to sign transactions and prove ownership of a blockchain address.'
    },
    {
      id: 'public-key',
      title: 'Public key',
      description: 'The public key is a unique personal address that is shared in the blockchain. A public key is a cryptographic code that is created using asymmetric-key encryption algorithms and is used to convert a message into an unreadable format. As the bitcoin public key is made up of an extremely long string of numbers, it is compressed and shortened to form the public address. If an owner loses their public key, it is possible to recreate it using the private key.',
      link: {
        title: 'oreilly.com',
        url: 'https://www.oreilly.com/library/view/mastering-bitcoin-2nd/9781491954379/ch04.html'
      }
    },
    {
      id: 'stablecoins',
      title: 'Stablecoins',
      description: 'Stablecoins are cryptocurrencies whose value is pegged, or tied, to that of another currency, commodity, or financial instrument. Stablecoins aim to provide an alternative to the high volatility of the most popular cryptocurrencies, including Bitcoin (BTC), which has made crypto investments less suitable for common transactions.'
    },
    {
      id: 'security-tokens',
      title: 'Security tokens',
      description: 'Security tokens are a new class of assets that aim to be the crypto equivalent of traditional securities like stocks and bonds. Their main use case is to sell shares in a company (very much like the shares or fractional shares sold via conventional markets) or other enterprises (for instance, real estate) without the need for a broker. Major companies and startups have been reported to be investigating security tokens as a potential alternative to other methods of fundraising.'
    },
    {
      id: 'smart-contract',
      title: 'Smart contract',
      description: 'A smart contract is a computer program or a transaction protocol that is intended to automatically execute, control, or document legally-relevant events and actions according to the terms of a contract or an agreement. The objectives of smart contracts are the reduction of the need for trusted intermediators, arbitration costs, and fraud losses, as well as the reduction of malicious and accidental exceptions. Smart contracts are commonly associated with cryptocurrencies, and the smart contracts introduced by Ethereum are generally considered a fundamental building block for decentralized finance (DeFi) and NFT applications.',
      link: {
        title: 'wikipedia.org',
        url: 'https://en.wikipedia.org/wiki/Smart_contract'
      }
    },
    {
      id: 'nft',
      title: 'Non-Fungible Tokens (NFTs)',
      description: 'NFTs represent ownership rights to a unique digital or real-world asset. They can be used to make it more difficult for digital creations to be copied and shared (an issue anyone who has ever visited a Torrent site full of the latest movies and video games understands). They’ve also been used to issue a limited number of digital artworks or sell unique virtual assets like rare items in a video game.'
    },
    {
      id: 'testnet',
      title: 'Testnet',
      description: 'In blockchain technology, a testnet is an instance of a blockchain powered by the same or a newer version of the underlying software, to be used for testing and experimentation without risk to real funds or the main chain.',
      link: {
        title: 'wikipedia.org',
        url: 'https://en.wikipedia.org/wiki/Testnet'
      }
    },
    {
      id: 'wallet',
      title: 'Wallet',
      description: 'A cryptocurrency wallet is a device, physical medium, program, or service which stores the public and/or private keys for cryptocurrency transactions. In addition to this basic function of storing the keys, a cryptocurrency wallet more often also offers the functionality of encrypting and/or signing information. Signing can for example result in executing a smart contract, a cryptocurrency transaction, identification or legally signing a \'document\'.',
      link: {
        title: 'wikipedia.org',
        url: 'https://en.wikipedia.org/wiki/Cryptocurrency_wallet'
      }
    },
    {
      id: 'mining',
      title: 'Mining',
      description: 'It is a competitive process that verifies and adds new transactions to the blockchain for a cryptocurrency that uses the proof-of-work (PoW) method. The miner that wins the competition is rewarded with some amount of the currency and/or transaction fees. The ways mining is performed are as follows.'
    },
    {
      id: 'network-fee',
      title: 'Network fee',
      description: 'Network Fees are paid to miners or validators of a blockchain network to adding transactions into the block and confirm transaction. Transactions compete with each other because miners or validators choose more profitable transactions in the first place. Network Fee increase and decrease algorithmical as stated in a blockchain protocol.'
    }
  ]
}
