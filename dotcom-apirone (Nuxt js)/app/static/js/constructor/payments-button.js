if (document.getElementById('__apr-constructor')) {
  init()
}

function init() {
  const args = (() => {
    const scriptTag = document.getElementById('__apr-constructor_script')
    const query = scriptTag.src.replace(/^[^?]+\??/, '')
    const vars = query.split('&')
    const args = {}
    for (let i = 0; i < vars.length; i++) {
      const pair = vars[i].split('=')
      args[pair[0]] = decodeURI(pair[1]).replace(/,/g, ' ')
    }
    return args
  })()

  let colorButton = '#FFFFFF'

  const baseUrl = args.baseUrl

  if (args.primaryColor === '#ffffff') {
    colorButton = '#000000'
  }

  args.additionalInfo = JSON.parse(decodeURIComponent(args.additionalInfo))

  const additionalArray = []
  for (const items in args.additionalInfo) {
    args.additionalInfo[items].qty = args.additionalInfo[items].qty.replace(/ /g, '')
    args.additionalInfo[items].name = args.additionalInfo[items].name.replace(/ /g, '')
    args.additionalInfo[items].cost = args.additionalInfo[items].cost.replace(/ /g, '')
    args.additionalInfo[items].total = args.additionalInfo[items].total.replace(/ /g, '')
    if (args.additionalInfo[items].qty !== '' || args.additionalInfo[items].cost !== '' || args.additionalInfo[items].name !== '' || args.additionalInfo[items].total !== '') {
      additionalArray.push(args.additionalInfo[items])
    }
  }
  args.additionalInfo = additionalArray

  const cryptoIcons = {
    tbtc: '<svg clip-rule="evenodd" fill-rule="evenodd" image-rendering="optimizeQuality" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" viewBox="0 0 4091.27 4091.73"><g fill-rule="nonzero"><path d="m4030.06 2540.77c-273.24 1096.01-1383.32 1763.02-2479.46 1489.71-1095.68-273.24-1762.69-1383.39-1489.33-2479.31 273.12-1096.13 1383.2-1763.19 2479-1489.95 1096.06 273.24 1763.03 1383.51 1489.76 2479.57l.02-.02z" fill="#a5a5a5"/><path d="m2947.77 1754.38c40.72-272.26-166.56-418.61-450-516.24l91.95-368.8-224.5-55.94-89.51 359.09c-59.02-14.72-119.63-28.59-179.87-42.34l90.16-361.46-224.36-55.94-92 368.68c-48.84-11.12-96.81-22.11-143.35-33.69l.26-1.16-309.59-77.31-59.72 239.78s166.56 38.18 163.05 40.53c90.91 22.69 107.35 82.87 104.62 130.57l-104.74 420.15c6.26 1.59 14.38 3.89 23.34 7.49-7.49-1.86-15.46-3.89-23.73-5.87l-146.81 588.57c-11.11 27.62-39.31 69.07-102.87 53.33 2.25 3.26-163.17-40.72-163.17-40.72l-111.46 256.98 292.15 72.83c54.35 13.63 107.61 27.89 160.06 41.3l-92.9 373.03 224.24 55.94 92-369.07c61.26 16.63 120.71 31.97 178.91 46.43l-91.69 367.33 224.51 55.94 92.89-372.33c382.82 72.45 670.67 43.24 791.83-303.02 97.63-278.78-4.86-439.58-206.26-544.44 146.69-33.83 257.18-130.31 286.64-329.61l-.07-.05zm-512.93 719.26c-69.38 278.78-538.76 128.08-690.94 90.29l123.28-494.2c152.17 37.99 640.17 113.17 567.67 403.91zm69.43-723.3c-63.29 253.58-453.96 124.75-580.69 93.16l111.77-448.21c126.73 31.59 534.85 90.55 468.94 355.05z" fill="#fff"/></g></svg>',
    trx: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 610 610" width="2500" height="2500"><circle cx="305" cy="305" r="305" fill="#ff060a"/><path d="M505.4 214.7c-17.3-12.1-35.8-25-53.9-37.8-.4-.3-.8-.6-1.3-.9-2-1.5-4.3-3.1-7.1-4l-.2-.1c-48.4-11.7-97.6-23.7-145.2-35.3-43.2-10.5-86.3-21-129.5-31.5-1.1-.3-2.2-.6-3.4-.9-3.9-1.1-8.4-2.3-13.2-1.7-1.4.2-2.6.7-3.7 1.4l-1.2 1c-1.9 1.8-2.9 4.1-3.4 5.4l-.3.8v4.6l.2.7c27.3 76.5 55.3 154.1 82.3 229.2 20.8 57.8 42.4 117.7 63.5 176.5 1.3 4 5 6.6 9.6 7h1c4.3 0 8.1-2.1 10-5.5l79.2-115.5c19.3-28.1 38.6-56.3 57.9-84.4 7.9-11.5 15.8-23.1 23.7-34.6 13-19 26.4-38.6 39.7-57.7l.7-1v-1.2c.3-3.5.4-10.7-5.4-14.5m-92.8 42.1c-18.6 9.7-37.6 19.7-56.7 29.6 11.1-11.9 22.3-23.9 33.4-35.8 13.9-15 28.4-30.5 42.6-45.7l.3-.3c1.2-1.6 2.7-3.1 4.3-4.7 1.1-1.1 2.3-2.2 3.4-3.5 7.4 5.1 14.9 10.3 22.1 15.4 5.2 3.7 10.5 7.4 15.9 11.1-22 11.2-44 22.7-65.3 33.9m-47.8-4.8c-14.3 15.5-29.1 31.4-43.8 47.1-28.5-34.6-57.6-69.7-85.8-103.6-12.8-15.4-25.7-30.9-38.5-46.3l-.1-.1c-2.9-3.3-5.7-6.9-8.5-10.3-1.8-2.3-3.7-4.5-5.6-6.8 11.6 3 23.3 5.8 34.8 8.5 10.1 2.4 20.6 4.9 30.9 7.5 58 14.1 116.1 28.2 174.1 42.3-19.3 20.6-38.7 41.5-57.5 61.7m-50.3 194.9c1.1-10.5 2.3-21.3 3.3-31.9.9-8.5 1.8-17.2 2.7-25.5 1.4-13.3 2.9-27.1 4.1-40.6l.3-2.4c1-8.6 2-17.5 2.6-26.4 1.1-.6 2.3-1.2 3.6-1.7 1.5-.7 3-1.3 4.5-2.2 23.1-12.1 46.2-24.2 69.4-36.2 23.1-12 46.8-24.4 70.3-36.7-21.4 31-42.9 62.3-63.7 92.8-17.9 26.1-36.3 53-54.6 79.5-7.2 10.6-14.7 21.4-21.8 31.8-8 11.6-16.2 23.5-24.2 35.4 1-12 2.2-24.1 3.5-35.9M175.1 155.6c-1.3-3.6-2.7-7.3-3.9-10.8 27 32.6 54.2 65.4 80.7 97.2 13.7 16.5 27.4 32.9 41.1 49.5 2.7 3.1 5.4 6.4 8 9.6 3.4 4.1 6.8 8.4 10.5 12.5-1.2 10.3-2.2 20.7-3.3 30.7-.7 7-1.4 14-2.2 21.1v.1c-.3 4.5-.9 9-1.4 13.4-.7 6.1-2.3 19.9-2.3 19.9l-.1.7c-1.8 20.2-4 40.6-6.1 60.4-.9 8.2-1.7 16.6-2.6 25-.5-1.5-1.1-3-1.6-4.4-1.5-4-3-8.2-4.4-12.3l-10.7-29.7C242.9 344.2 209 250 175.1 155.6" fill="#fff"/></svg>',
    'usdt@trx': '<svg width="512" height="512" viewBox="0 0 512 512" fill="none" xmlns="http://www.w3.org/2000/svg"><g clip-path="url(#clip0_1494_347)"><rect x="108" y="107" width="306" height="298" fill="white"/><path d="M389.7 248.7C389.6 261.2 345.3 271.6 286 274.2C284.3 274.3 275.6 274.8 256.3 274.8C240.8 274.8 230 274.3 226.1 274.1C196.4 272.8 170.4 269.5 151.9 265.1C133.4 260.7 122.3 255 122.3 248.7C122.3 236.3 166.7 226 226.1 223.3V263.7C230 264 241.1 264.7 256.5 264.7C275 264.7 284.2 263.9 286 263.7V223.3C345.5 226 389.7 236.3 389.7 248.7ZM512 256C512 397.4 397.4 512 256 512C114.6 512 0 397.4 0 256C0 114.6 114.6 0 256 0C397.4 0 512 114.6 512 256ZM403.7 251.7C403.7 235.7 353.3 222.4 286 219.3V183.2H368.7V128H143.4V183.2H226.1V219.3C158.9 222.4 108.3 235.7 108.3 251.6C108.3 267.6 158.9 280.9 226.1 284V400H286V284.1C353.3 281 403.7 267.7 403.7 251.7Z" fill="#76C5B8"/></g><defs><clipPath id="clip0_1494_347"><rect width="512" height="512" fill="white"/></clipPath></defs></svg>',
    'usdc@trx': '<svg data-name="86977684-12db-4850-8f30-233a7c267d11" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2000 2000"><path d="M1000 2000c554.17 0 1000-445.83 1000-1000S1554.17 0 1000 0 0 445.83 0 1000s445.83 1000 1000 1000z" fill="#2775ca"/><path d="M1275 1158.33c0-145.83-87.5-195.83-262.5-216.66-125-16.67-150-50-150-108.34s41.67-95.83 125-95.83c75 0 116.67 25 137.5 87.5 4.17 12.5 16.67 20.83 29.17 20.83h66.66c16.67 0 29.17-12.5 29.17-29.16v-4.17c-16.67-91.67-91.67-162.5-187.5-170.83v-100c0-16.67-12.5-29.17-33.33-33.34h-62.5c-16.67 0-29.17 12.5-33.34 33.34v95.83c-125 16.67-204.16 100-204.16 204.17 0 137.5 83.33 191.66 258.33 212.5 116.67 20.83 154.17 45.83 154.17 112.5s-58.34 112.5-137.5 112.5c-108.34 0-145.84-45.84-158.34-108.34-4.16-16.66-16.66-25-29.16-25h-70.84c-16.66 0-29.16 12.5-29.16 29.17v4.17c16.66 104.16 83.33 179.16 220.83 200v100c0 16.66 12.5 29.16 33.33 33.33h62.5c16.67 0 29.17-12.5 33.34-33.33v-100c125-20.84 208.33-108.34 208.33-220.84z" fill="#fff"/><path d="M787.5 1595.83c-325-116.66-491.67-479.16-370.83-800 62.5-175 200-308.33 370.83-370.83 16.67-8.33 25-20.83 25-41.67V325c0-16.67-8.33-29.17-25-33.33-4.17 0-12.5 0-16.67 4.16-395.83 125-612.5 545.84-487.5 941.67 75 233.33 254.17 412.5 487.5 487.5 16.67 8.33 33.34 0 37.5-16.67 4.17-4.16 4.17-8.33 4.17-16.66v-58.34c0-12.5-12.5-29.16-25-37.5zM1229.17 295.83c-16.67-8.33-33.34 0-37.5 16.67-4.17 4.17-4.17 8.33-4.17 16.67v58.33c0 16.67 12.5 33.33 25 41.67 325 116.66 491.67 479.16 370.83 800-62.5 175-200 308.33-370.83 370.83-16.67 8.33-25 20.83-25 41.67V1700c0 16.67 8.33 29.17 25 33.33 4.17 0 12.5 0 16.67-4.16 395.83-125 612.5-545.84 487.5-941.67-75-237.5-258.34-416.67-487.5-491.67z" fill="#fff"/></svg>',
    btc: '<svg clip-rule="evenodd" fill-rule="evenodd" image-rendering="optimizeQuality" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" viewBox="0 0 4091.27 4091.73"><g fill-rule="nonzero"><path d="m4030.06 2540.77c-273.24 1096.01-1383.32 1763.02-2479.46 1489.71-1095.68-273.24-1762.69-1383.39-1489.33-2479.31 273.12-1096.13 1383.2-1763.19 2479-1489.95 1096.06 273.24 1763.03 1383.51 1489.76 2479.57l.02-.02z" fill="#f7931a"/><path d="m2947.77 1754.38c40.72-272.26-166.56-418.61-450-516.24l91.95-368.8-224.5-55.94-89.51 359.09c-59.02-14.72-119.63-28.59-179.87-42.34l90.16-361.46-224.36-55.94-92 368.68c-48.84-11.12-96.81-22.11-143.35-33.69l.26-1.16-309.59-77.31-59.72 239.78s166.56 38.18 163.05 40.53c90.91 22.69 107.35 82.87 104.62 130.57l-104.74 420.15c6.26 1.59 14.38 3.89 23.34 7.49-7.49-1.86-15.46-3.89-23.73-5.87l-146.81 588.57c-11.11 27.62-39.31 69.07-102.87 53.33 2.25 3.26-163.17-40.72-163.17-40.72l-111.46 256.98 292.15 72.83c54.35 13.63 107.61 27.89 160.06 41.3l-92.9 373.03 224.24 55.94 92-369.07c61.26 16.63 120.71 31.97 178.91 46.43l-91.69 367.33 224.51 55.94 92.89-372.33c382.82 72.45 670.67 43.24 791.83-303.02 97.63-278.78-4.86-439.58-206.26-544.44 146.69-33.83 257.18-130.31 286.64-329.61l-.07-.05zm-512.93 719.26c-69.38 278.78-538.76 128.08-690.94 90.29l123.28-494.2c152.17 37.99 640.17 113.17 567.67 403.91zm69.43-723.3c-63.29 253.58-453.96 124.75-580.69 93.16l111.77-448.21c126.73 31.59 534.85 90.55 468.94 355.05z" fill="#fff"/></g></svg>',
    bch: '<svg enable-background="new 0 0 788 788" viewBox="0 0 788 788"><circle cx="394" cy="394" fill="#0ac18e" r="394"/><path d="m516.9 261.7c-19.8-44.9-65.3-54.5-121-45.2l-17.9-69.4-42.2 10.9 17.6 69.2c-11.1 2.8-22.5 5.2-33.8 8.4l-17.6-68.8-42.2 10.9 17.9 69.4c-9.1 2.6-85.2 22.1-85.2 22.1l11.6 45.2s31-8.7 30.7-8c17.2-4.5 25.3 4.1 29.1 12.2l49.2 190.2c.6 5.5-.4 14.9-12.2 18.1.7.4-30.7 7.9-30.7 7.9l4.6 52.7s75.4-19.3 85.3-21.8l18.1 70.2 42.2-10.9-18.1-70.7c11.6-2.7 22.9-5.5 33.9-8.4l18 70.3 42.2-10.9-18.1-70.1c65-15.8 110.9-56.8 101.5-119.5-6-37.8-47.3-68.8-81.6-72.3 21.1-18.7 31.8-46 18.7-81.7zm-20.3 165.5c8.4 62.1-77.9 69.7-106.4 77.2l-24.8-92.9c28.6-7.5 117-39 131.2 15.7zm-52-126.5c8.9 55.2-64.9 61.6-88.7 67.7l-22.6-84.3c23.9-5.9 93.2-34.5 111.3 16.6z" fill="#fff"/></svg>',
    doge: '<svg height="65" viewBox="0 0 64 65" width="64"><g fill="none"><circle cx="32.2" cy="32.4" fill="#f8bf1a" r="31.7"/><path d="m14.6 50.2v-8.4h4.9v-19.2h-4.9v-8.1h20.5c1.4 0 2.8.4 4.1.9 2.6 1 4.9 2.5 6.7 4.7 1.6 1.9 2.7 4.1 3.3 6.6 1 4.2 1 8.5-.2 12.7-1.6 5.5-5 9.2-10.8 10.5-1 .2-2.1.3-3.1.4h-19.7c-.3-.1-.5-.1-.8-.1zm14.9-8.4c1.1 0 2.2.1 3.3 0 1.4-.1 2.7-.6 3.6-1.8.8-1 1.5-2.1 1.9-3.3.8-2.7.5-5.4-.1-8.1-.3-1.6-1.1-3.1-2.1-4.3-.6-.7-1.4-1.5-2.3-1.6-1.4-.3-2.9-.1-4.3-.1z" fill="#fff"/></g></svg>',
    ltc: '<svg viewBox="0 0 508.96 508.96"><circle cx="254.48" cy="254.48" fill="#fff" r="226.94"/><path d="m256.38 2c-140.54 0-254.48 114-254.48 254.52s113.94 254.48 254.48 254.48 254.49-113.93 254.49-254.48c.4-140.14-112.87-254.07-253-254.52zm4.32 263.11-26.5 89.34h141.72a7.15 7.15 0 0 1 7.4 6.89v2.34l-12.32 42.57a9.18 9.18 0 0 1 -9.24 6.78h-216.9l36.35-123.85-40.67 12.32 9.25-28.34 40.66-12.33 51.15-173.76a9.3 9.3 0 0 1 9.24-6.78h54.84a7.15 7.15 0 0 1 7.39 6.9v2.35l-43.13 146.65 40.67-12.33-8.61 29.58z" fill="#345d9d" transform="translate(-1.9 -2.04)"/></svg>'
  }

  function transformCryptoCurrency(crypto) {
    return cryptoIcons[crypto.toLowerCase()] || crypto
  }

  const constructor = document.getElementById('__apr-constructor')
  const constructorCode = document.getElementById('__constructor-code')
  const copyCode = document.getElementById('copyCode')
  const copyCodeInput = copyCode.querySelector('input')

  const apironeConstructor = {
    constructor: '',
    constructorCode: '',

    create: () => {
      const form = initElement.create('form', {
        class: '__apr-constructor-form'
      })

      form.append(initElement.create('span', {
        class: '__apr-constructor-error'
      }))

      const containerForInputAndButton = initElement.create('div', {
        class: '__apr-constructor-container'
      })

      if (args.amountIndicatedCustomer === 'true') {
        const containerAmount = initElement.create('div', {
          class: '__apr-constructor-container-input'
        })

        const fiatAmountIcon = initElement.create('span', {
          class: '__apr-constructor-fiat-icon'
        })

        fiatAmountIcon.innerHTML = args.fiat

        const inputAmount = initElement.create('input', {
          type: 'text',
          class: '__apr-constructor-input',
          name: 'apr_amount'
        })

        inputAmount.addEventListener('input', amountTotal)
        containerAmount.append(inputAmount, fiatAmountIcon)
        containerForInputAndButton.append(containerAmount)
      }

      const button = initButton.create({
        type: 'button',
        class: '__apr-constructor-button'
      })
      button.addEventListener('click', createInvoice)
      containerForInputAndButton.append(button)

      form.append(containerForInputAndButton)

      const containerAmountTotal = initElement.create('div', {
        class: '__apr-constructor-container-total-amount'
      })

      const cryptoAmountTotal = initElement.create('span', {
        class: '__apr-constructor-crypto-total-amount'
      })

      const total = initElement.create('span', {
        class: '__apr-constructor-total-amount'
      })

      containerAmountTotal.append(cryptoAmountTotal, total)
      form.append(containerAmountTotal)

      addStyles.create()
      apironeScript.create()

      const constructorCode = initElement.create('div', {
        class: '__apr-constructor-code'
      })

      addStyles.insert(constructorCode)
      constructorCode.append(form)
      apironeScript.insert(constructorCode)
      apironeConstructor.constructorCode = constructorCode.outerHTML.replace(/\s+/g, ' ');
      apironeConstructor.constructor = form
      setTimeout(() => {
        copyCodeInput.setAttribute('value', apironeConstructor.constructorCode)
      }, 1000)
    },

    insert: () => {
      constructorCode.innerHTML = ''
      constructor.innerHTML = ''
      addStyles.create()
      constructorCode.append(apironeConstructor.constructorCode)
      constructor.append(addStyles.styles, apironeConstructor.constructor)
    }
  }

  const apironeScript = {
    script: '',
    create: () => {
      const script = initElement.create('script')
      script.append(`const args = ${JSON.stringify(args)}; const baseUrl = '${baseUrl}';`)
      script.append(getWallets)
      script.append(amountTotal, 'amountTotal();')
      script.append(initEvent, 'initEvent();')
      script.append(keydownEventAmount, 'keydownEventAmount();')
      script.append(fade)
      script.append(createInvoice)
      script.append(keydownEventAmount)
      apironeScript.script = script
    },

    insert: (where) => {
      where.append(apironeScript.script)
    }
  }

  const initButton = {
    create: (options = {}) => {
      const button = document.createElement('button')
      let nameButton
      if (args.amountIndicatedCustomer === 'true' && args.switchCurrency === 'false') {
        nameButton = `${transformCryptoCurrency(args.crypto)} <span style="margin-left: 1px;"></span> `
      } else if (args.amountIndicatedCustomer === 'false' && args.switchCurrency === 'false') {
        nameButton = `${transformCryptoCurrency(args.crypto)} ${args.fiat} ${args.amount} <span></span> `
      } else if (args.switchCurrency === 'true') {
        nameButton = `${transformCryptoCurrency(args.crypto)}${args.amount} <span></span> `
      }

      button.innerHTML = nameButton
      button.append(args.nameButton)

      for (const option in options) {
        button.setAttribute(option, options[option])
      }

      return button
    }
  }

  const initElement = {
    create: (type, options = {}) => {
      const element = document.createElement(type)

      for (const option in options) {
        element.setAttribute(option, options[option])
      }

      return element
    }
  }

  async function amountTotal() {
    const getExchange = await fetch(`${baseUrl}/api/v2/ticker?currency=${args.crypto.toLowerCase()}`);
    const result = await getExchange.json();
    const amountTotal = document.querySelector('.__apr-constructor-total-amount');
    const containerAmountTotal = document.querySelector('.__apr-constructor-container-total-amount');
    const cryptoAmountTotal = document.querySelector('.__apr-constructor-crypto-total-amount');
    const currencies = await getWallets();
    let toFixed;
    currencies.forEach((currency) => {
      if (currency.abbr === args.crypto.toLowerCase()) {
        toFixed = currency.abbr.includes('usdt') || currency.abbr.includes('usdc') ? 2 : (1 / currency['units-factor']).toString().length - 1;
      }
    });

    let total;
    if (args.switchCurrency === 'false' && args.amountIndicatedCustomer === 'false') {
      total = args.amount / result[args.fiat.toLowerCase()];

      if (args.amount !== '0' && args.amount !== '') {
        amountTotal.innerText = total.toFixed(toFixed);
        cryptoAmountTotal.textContent = `${args.crypto} `;
        fade(containerAmountTotal, 300, 'in');
      } else {
        fade(containerAmountTotal, 300, 'out');
      }
    } else if (args.switchCurrency === 'false' && args.amountIndicatedCustomer === 'true') {
      const amountInput = document.querySelector("input[name='apr_amount']").value;
      total = amountInput / result[args.fiat.toLowerCase()];

      if (amountInput !== '') {
        amountTotal.innerText = total.toFixed(toFixed);
        cryptoAmountTotal.textContent = `${args.crypto} `;
        fade(containerAmountTotal, 300, 'in');
      } else {
        fade(containerAmountTotal, 300, 'out');
      }
    } else if (args.switchCurrency === 'true') {
      total = Number(args.amount);
      fade(containerAmountTotal, 0, 'out');
      amountTotal.innerText = total.toFixed(toFixed);
    }
    return total;
  };

  function fade(element, duration, direction) {
    let startOpacity, endOpacity;

    if (direction === 'in') {
      startOpacity = 0;
      endOpacity = 1;
      element.style.display = 'block';
    } else if (direction === 'out') {
      startOpacity = 1;
      endOpacity = 0;
    }

    let startTime = null;

    function animate(currentTime) {
      if (!startTime) {
        startTime = currentTime;
      }

      const elapsedTime = currentTime - startTime;
      let progress = elapsedTime / duration;

      if (progress > 1) {
        progress = 1;
      }

      element.style.opacity = startOpacity + (endOpacity - startOpacity) * progress;

      if (progress < 1) {
        requestAnimationFrame(animate);
      } else if (direction === 'out') {
        element.style.display = 'none';
      }
    }

    requestAnimationFrame(animate);
  };

  function initEvent() {
    const button = document.querySelector('.__apr-constructor-button');
    const input = document.querySelector('.__apr-constructor-input');
    input.addEventListener('change', amountTotal);
    button.addEventListener('click', createInvoice);
  };

  async function getWallets() {
    const response = await fetch(`${baseUrl}/api/v2/wallets`, {method: 'OPTIONS'});
    const result = await response.json();
    return result.currencies;
  }

  async function createInvoice() {
    const totalAmountValue = document.querySelector('.__apr-constructor-total-amount').innerText;
    const currencies = await getWallets();
    let float;
    currencies.forEach((currency) => {
      if (currency.abbr === args.crypto.toLowerCase()) {
        float = 1 / parseFloat(currency['units-factor']);
      }
    });
    const options = {
      currency: args.crypto.toLowerCase(),
      amount: Math.round(Number(totalAmountValue * float) || 0),
      lifetime: Number(args.lifetime)
    };

    if (args.additionalInfo.length) {
      options['user-data'] = {items: args.additionalInfo};
    }

    const response = await fetch(`${baseUrl}/api/v2/accounts/${args.account || 'BLANK_FIELD'}/invoices`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(options)
    });

    const result = await response.json();

    if (response.status === 200) {
      const link = document.createElement('a');
      link.href = result['invoice-url'];
      link.target = '_blank';
      link.click();
    } else {
      const error = document.querySelector('.__apr-constructor-error');

      if (args.account === '') {
        error.textContent = 'Enter your account ID';
        fade(error, 300, 'in');
      } else if (result.message === 'Amount should be a positive integer.') {
        error.textContent = 'Amount should be positive.';
        fade(error, 300, 'in');
      } else {
        error.textContent = result.message;
        fade(error, 300, 'in');
      }

      setTimeout(() => {
        fade(error, 300, 'out');
      }, 5000);
    }
  };

  async function checkChangeCurrenciesCoinsAndAmount() {
    const newAmount = args.amount;
    const newFiat = args.fiat;
    const newCrypto = args.crypto;
    const newSwitchCurrency = args.switchCurrency;

    if (newAmount !== sessionStorage.amount ||
      newFiat !== sessionStorage.fiat ||
      newCrypto !== sessionStorage.crypto ||
      newSwitchCurrency !== sessionStorage.switchCurrency) {
      sessionStorage.amount = newAmount;
      sessionStorage.fiat = newFiat;
      sessionStorage.crypto = newCrypto;
      sessionStorage.switchCurrency = newSwitchCurrency;
      sessionStorage.totalButton = await amountTotal();
    } else {
      setTimeout(() => {
        const amountTotal = document.querySelector('.__apr-constructor-total-amount');
        const containerAmountTotal = document.querySelector('.__apr-constructor-container-total-amount');
        const cryptoAmountTotal = document.querySelector('.__apr-constructor-crypto-total-amount');
        const total = sessionStorage.totalButton || 0
        cryptoAmountTotal.textContent = args.crypto + ' '
        amountTotal.textContent = Number(total).toFixed(8);
        fade(containerAmountTotal, 300, 'in');
      }, 1);
    }
  }

  checkChangeCurrenciesCoinsAndAmount()

  function keydownEventAmount() {
    const amount = document.querySelector("input[name='apr_amount']");
    if (amount) {
      amount.addEventListener('keydown', (event) => {
        if (((event.metaKey || event.ctrlKey) && [67, 86, 88, 65].includes(event.keyCode)) ||
          (event.keyCode === 8 || event.keyCode === 9 || event.keyCode === 46 || event.key === '.')) {
          return true;
        } else if (event.keyCode === 46 || !/^[0-9]$/.test(event.key)) {
          event.preventDefault();
        }
      });
    }
  };

  // setTimeout(keydownEventAmount, 1);

  const addStyles = {
    styles: '',

    create: () => {
      const styles = document.createElement('style')
      styles.setAttribute('scoped', 'scoped')
      styles.innerHTML = `
      .__apr-constructor-error {
        display: none;
        margin-bottom: 12px;
        color: #ff0000;
        border: 1px solid #ff0000;
        padding: 8px 12px;
        border-radius: 8px;
        background-color: #f9e8e8;
      }

      .__apr-constructor-container-input {
        display: inline-block;
        margin-right: 16px;
        position: relative;
        margin-top: 6px
      }

      .__apr-constructor-fiat-icon {
        position: absolute;
        right: 6px;
        top: 50%;
        transform: translateY(-50%);
        font-size: 16px;
        color: ${args.primaryColor === '#ffffff' ? '#000000' : args.primaryColor};
      }

      .__apr-constructor-form {
        max-width: 400px;
        width: 100%;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", "Liberation Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji", sans-serif;
      }

      .__apr-constructor-container {
        display: flex;
        flex-wrap: wrap;
      }

      .__apr-constructor-button {
        background-color: ${args.primaryColor};
        color: ${colorButton};
        border: 1px solid;
        border-color: ${args.primaryColor === '#ffffff' ? '#000000' : args.primaryColor};
        padding: 0 16px 0 12px;
        border-radius: 8px;
        cursor: pointer;
        height: 36px;
        font-family: inherit;
        font-size: 16px;
        font-weight: 500;
        margin-top: 6px;
        display: flex;
        align-items: center;
      }

      .__apr-constructor-button svg {
        width: 20px;
        margin-right: 8px;
      }

      .__apr-constructor-button span {
        width: 0.5px;
        background: ${args.primaryColor === '#ffffff' ? '#000000' : '#ced4da'};
        height: 100%;
        display: block;
        margin: 0 8px;
      }

      .__apr-constructor-input {
        width: 200px;
        height: 36px;
        outline: none;
        border-radius: 8px;
        border: 1px solid #ced4da;
        transition: 0.2s;
        padding-left: 12px;
        padding-right: 42px;
        font-family: inherit;
      }

      .__apr-constructor-input:focus {
        border: 1px solid ${args.primaryColor === '#ffffff' ? '#000000' : args.primaryColor};
      }

      .__apr-constructor-container-total-amount {
        display: none;
        text-align: start;
        margin-top: 12px;
      }
    `.replace(/\n\s+/g, ' ')
      addStyles.styles = styles
    },
    insert: (where) => {
      where.prepend(addStyles.styles)
    }
  }
  apironeConstructor.create()
  apironeConstructor.insert()
}
