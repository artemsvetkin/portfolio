export default {
  data() {
    return {
      currencies: {}
    }
  },
  async created() {
    await this.getWallets()
  },
  methods: {
    async getWallets() {
      const response = await fetch(`${process.env.serviceUrl}/api/v2/wallets`, {
        method: 'OPTIONS'
      })
      const result = await response.json()
      this.currencies = result.currencies
    }
  }
}
