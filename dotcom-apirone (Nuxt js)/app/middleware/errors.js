export default function ({redirect, route}) {
  const { name } = route
  if (name !== 'technical-works___en') {
    redirect('/technical-works')
  }
}
