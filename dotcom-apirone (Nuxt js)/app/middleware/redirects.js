export default function (req, res, next) {
  const redirects = [
    {
      from: '/ru/integrations/virtuemart',
      to: '/integrations'
    },
    {
      from: '/ru',
      to: '/'
    },
    {
      from: '/integrations/virtuemart',
      to: '/integrations'
    },
    {
      from: '/ru/integrations/woocommerce',
      to: '/blog/woocommerce'
    },
    {
      from: '/ru/integrations/',
      to: '/integrations'
    },
    {
      from: '/ru/integrations/opencart-3',
      to: '/blog/opencart'
    },
    {
      from: '/ru/docs/exchange-rate',
      to: '/docs/rate/'
    },
    {
      from: '/ru/index',
      to: '/'
    },
    {
      from: '/ru/',
      to: '/'
    },
    {
      from: '/ru/integrations/become-a-supporter',
      to: '/integrations'
    },
    {
      from: '/fr/integrations/become-a-supporter',
      to: '/integrations'
    },
    {
      from: '/fr',
      to: '/'
    },
    {
      from: '/fr/integrations/virtuemart',
      to: '/integrations'
    },
    {
      from: '/fr/integrations/woocommerce',
      to: '/blog/woocommerce'
    },
    {
      from: '/fr/integrations/',
      to: '/integrations'
    },
    {
      from: '/fr/integrations/opencart-3',
      to: '/blog/opencart'
    },
    {
      from: '/fr/docs/exchange-rate',
      to: '/docs/rate/'
    },
    {
      from: '/fr/index',
      to: '/'
    },
    {
      from: '/ru/integrations/virtuemart',
      to: '/integrations'
    },
    {
      from: '/fr/integrations/virtuemart',
      to: '/integrations'
    },
    {
      from: '/ru/gdpr',
      to: '/gdpr'
    },
    {
      from: '/fr/gdpr',
      to: '/gdpr'
    },
    {
      from: '/ru/privacy-policy',
      to: '/privacy-policy'
    },
    {
      from: '/ru/integrations/magento-2',
      to: '/integrations'
    },
    {
      from: '/fr/privacy-policy',
      to: '/privacy-policy'
    },
    {
      from: '/ru/using-cookies',
      to: '/using-cookies'
    },
    {
      from: '/fr/using-cookies',
      to: '/using-cookies'
    },
    {
      from: '/ru/dashboard',
      to: '/dashboard'
    },
    {
      from: '/fr/dashboard',
      to: '/dashboard'
    },
    {
      from: '/fr/about-us',
      to: '/'
    },
    {
      from: '/fr/blog/',
      to: '/'
    },
    {
      from: '/fr/changelog',
      to: '/'
    },
    {
      from: '/fr/charity',
      to: '/'
    },
    {
      from: '/fr/features',
      to: '/'
    },
    {
      from: '/fr/options',
      to: '/'
    },
    {
      from: '/fr/pricing',
      to: '/'
    },
    {
      from: '/fr/promotion-images',
      to: '/'
    },
    {
      from: '/fr/support-us',
      to: '/'
    },
    {
      from: '/fr/telegram',
      to: '/'
    },
    {
      from: '/fr/wallet/',
      to: '/'
    },
    {
      from: '/index',
      to: '/'
    },
    {
      from: '/ru/docs/',
      to: '/docs/'
    },
    {
      from: '/fr/docs/',
      to: '/docs/'
    },
    {
      from: '/ru/integrations',
      to: '/integrations'
    },
    {
      from: '/fr/integrations',
      to: '/integrations'
    },
    {
      from: '/ru/charity',
      to: '/charity'
    },
    {
      from: '/fr/charity',
      to: '/charity'
    },
    {
      from: '/ru/file-sharing',
      to: '/file-sharing'
    },
    {
      from: '/fr/file-sharing',
      to: '/file-sharing'
    },
    {
      from: '/fr/',
      to: '/'
    },
    {
      from: '/de',
      to: '/'
    },
    {
      from: '/de/',
      to: '/'
    },
    {
      from: '/bitcoin-forwarding-api',
      to: '/docs/forwardingv1/'
    },
    {
      from: '/account/',
      to: '/dashboard/'
    },
    {
      from: '/ru/filesharing',
      to: '/file-sharing'
    },
    {
      from: '/fr/docs/service-info',
      to: '/docs/service/'
    },
    {
      from: '/fr/docs/balance',
      to: '/docs/wallet/#wallet-balance'
    },
    {
      from: '/de/press',
      to: '/'
    },
    {
      from: '/de/about-us',
      to: '/'
    },
    {
      from: '/de/blog/',
      to: '/'
    },
    {
      from: '/de/changelog',
      to: '/'
    },
    {
      from: '/de/docs/',
      to: '/'
    },
    {
      from: '/de/features',
      to: '/'
    },
    {
      from: '/de/docs/transfer',
      to: '/docs/wallet/#transfer'
    },
    {
      from: '/de/file-sharing',
      to: '/'
    },
    {
      from: '/de/gdpr',
      to: '/'
    },
    {
      from: '/de/how-to',
      to: '/'
    },
    {
      from: '/de/options',
      to: '/'
    },
    {
      from: '/de/pricing',
      to: '/'
    },
    {
      from: '/de/privacy-policy',
      to: '/'
    },
    {
      from: '/de/promotion-images',
      to: '/'
    },
    {
      from: '/de/support-us',
      to: '/'
    },
    {
      from: '/de/telegram',
      to: '/'
    },
    {
      from: '/de/docs/settings',
      to: '/docs/settings/#settings'
    },
    {
      from: '/de/docs/restful-api',
      to: '/blog/restful-api'
    },
    {
      from: '/de/docs/receiving-callbacks-v2',
      to: '/docs/wallet/#address-callback-info'
    },
    {
      from: '/de/docs/network-fee',
      to: '/docs/fee/#fee-types-strategies'
    },
    {
      from: '/de/integrations/opencart-3',
      to: '/blog/opencart'
    },
    {
      from: '/de/docs/balance',
      to: '/docs/wallet/#wallet-balance'
    },
    {
      from: '/de/docs/address',
      to: '/docs/wallet/#generate-address'
    },
    {
      from: '/de/blog/payment-process',
      to: '/blog/payment-process'
    },
    {
      from: '/docs/address',
      to: '/'
    },
    {
      from: '/docs/forwarding-wallet',
      to: '/'
    },
    {
      from: '/docs/how-it-works',
      to: '/'
    },
    {
      from: '/en/',
      to: '/'
    },
    {
      from: '/en/using-cookies',
      to: '/'
    },
    {
      from: '/btc',
      to: '/'
    },
    {
      from: '/ru/docs/bitcoin-forwarding-api',
      to: '/docs/forwardingv1/'
    },
    {
      from: '/messenger',
      to: '/'
    },
    {
      from: '/fr/press',
      to: '/'
    },
    {
      from: '/fr/docs/transfer',
      to: '/docs/wallet/#transfer'
    },
    {
      from: '/fr/docs/settings',
      to: '/docs/settings/#settings'
    },
    {
      from: '/fr/docs/restful-api',
      to: '/blog/restful-api'
    },
    {
      from: '/fr/docs/receiving-callbacks-v2',
      to: '/docs/wallet/#address-callback-info'
    },
    {
      from: '/fr/docs/network-fee',
      to: '/docs/fee/#fee-types-strategies'
    },
    {
      from: '/fr/integrations/opencart-3',
      to: '/blog/opencart'
    },
    {
      from: '/fr/docs/balance',
      to: '/docs/wallet/#wallet-balance'
    },
    {
      from: '/fr/docs/address',
      to: '/docs/wallet/#generate-address'
    },
    {
      from: '/ru/press',
      to: '/'
    },
    {
      from: '/ru/docs/transfer',
      to: '/docs/wallet/#transfer'
    },
    {
      from: '/ru/docs/settings',
      to: '/docs/settings/#settings'
    },
    {
      from: '/ru/docs/restful-api',
      to: '/blog/restful-api'
    },
    {
      from: '/ru/docs/receiving-callbacks-v2',
      to: '/docs/wallet/#address-callback-info'
    },
    {
      from: '/ru/docs/network-fee',
      to: '/docs/fee/#fee-types-strategies'
    },
    {
      from: '/ru/integrations/opencart-3',
      to: '/blog/opencart'
    },
    {
      from: '/ru/docs/balance',
      to: '/docs/wallet/#wallet-balance'
    },
    {
      from: '/ru/docs/address',
      to: '/docs/wallet/#generate-address'
    },
    {
      from: '/fr/docs/get-qr-code',
      to: '/docs/'
    },
    {
      from: '/_nuxt/static/1697155090',
      to: '/'
    },
    {
      from: '/_nuxt/static/1699462328',
      to: '/'
    },
    {
      from: '/blog/donation-widget',
      to: '/'
    },
    {
      from: '/blog/donation-widget',
      to: '/'
    },
    {
      from: '/blog/donation-widget/',
      to: '/'
    },
    {
      from: '/changelog',
      to: '/'
    },
    {
      from: '/press',
      to: '/'
    },
    {
      from: '/roadmap',
      to: '/'
    },
    {
      from: '/ru/about-us',
      to: '/'
    },
    {
      from: '/ru/docs',
      to: '/'
    },
    {
      from: '/ru/docs/get-qr-code',
      to: '/'
    },
    {
      from: '/ru/docs/wallet',
      to: '/'
    },
    {
      from: '/ru/features',
      to: '/'
    },
    {
      from: '/ru/how-to',
      to: '/'
    },
    {
      from: '/ru/partners',
      to: '/'
    },
    {
      from: '/ru/pricing',
      to: '/'
    },
    {
      from: '/support-us',
      to: '/'
    },
    {
      from: '/blog/restful-api',
      to: '/'
    }
  ]
  /* const redirects = [
    {
      from: new RegExp('/ru'),
        to: '/'
    },
    {
      from: new RegExp('/fr'),
        to: '/'
    },
    {
      from: new RegExp('/de'),
        to: '/'
    },
  ] */
  const redirect = redirects.find(r => r.from === req.url)
  if (req.url.includes('btc')) {
    res.writeHead(301, { Location: '/' })
    res.end()
  }
  if (redirect) {
    res.writeHead(301, { Location: redirect.to })
    res.end()
  } else {
    next()
  }
}
