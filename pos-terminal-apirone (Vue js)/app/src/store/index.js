import { createStore } from 'vuex'

export default createStore({
  state: {
    accountId: localStorage['login'],
    accessToken: localStorage['access-token'],
    refreshToken: localStorage['refresh-token'],
    fiatCurrency: localStorage['fiat'] ? localStorage['fiat'] : 'USD',
    cryptoCurrency: localStorage['crypto'] ? localStorage['crypto'] : 'BTC',
    // serviceUrl: 'https://apirone.com'
    serviceUrl: process.env.VUE_APP_SERVICE_URL
  },
  getters: {
  },
  mutations: {
    setToken(state, data) {
      state.accessToken = data
    },
    setFiat(state, data) {
      state.fiatCurrency = data
    },
    setCrypto(state, data) {
      localStorage['crypto'] = data
      state.cryptoCurrency = data
    }
  },
  actions: {
  },
  modules: {
  }
})
