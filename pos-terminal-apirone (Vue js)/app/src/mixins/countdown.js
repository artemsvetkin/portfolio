export default {
    data() {
        return {
            expire: null
        }
    },
    methods: {
        countdown(expireRes) {
            const interval = setInterval(async () => {
                if (this.showInvoice) {
                    const countDownDate = new Date(`${expireRes}+00:00`).getTime();
                    const now = new Date().getTime();
                    const distance = countDownDate - now;
                    const days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    const seconds = Math.floor((distance % (1000 * 60)) / 1000);
                    this.expire = (days > 0 ? `${days}d ` : ' ')
                        + (hours > 0 ? `${hours}h ` : ' ')
                        + (minutes > 0 ? `${minutes}m ` : ' ')
                        + `${seconds}s`;
                    if (distance < 0) {
                        clearInterval(interval);
                        this.expire = null;
                        this.notification.title = 'Lifetime'
                        this.notification.message = 'The invoice lifetime has expired, you will be returned to the main window after 5 seconds.'
                        this.notification.show = true
                        setTimeout(this.clickHome, 5000)
                        await this.getHistory(0)
                    }
                } else {
                    clearInterval(interval)
                    this.expire = null
                }
            }, 1000);
        },
    }
}