export default {
  data() {
    return {
      debugMode: false,
      dropdownHeader: false,
    }
  },
  methods: {
    closeDropdownOnOutsideClick(event) {
      if (this.dropdownHeader) {
        const dropdownMobile = this.$el.querySelector('.header__mobile-bar');
        if (dropdownMobile && !dropdownMobile.contains(event.target)) {
          this.dropdownHeader = false;
        }
      }
    },
    toggleDropdown() {
      this.dropdownHeader = !this.dropdownHeader;
    },
  }
}