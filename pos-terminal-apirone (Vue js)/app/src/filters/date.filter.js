export default function dateFilter(value) {
    return new Date(`${value}+00:00`).toLocaleString()
}
