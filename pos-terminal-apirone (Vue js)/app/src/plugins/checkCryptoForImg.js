export function checkCryptoForImg(value) {
  let obj = {}
  if (value.includes('@')) {
    const splitVal = value.split('@')
    obj.network = splitVal[1].toLowerCase()
    obj.main = splitVal[0].toLowerCase()
  } else {
    obj.main = value.toLowerCase()
  }

  return obj
}