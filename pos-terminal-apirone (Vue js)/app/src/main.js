import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueQrcode from 'vue-qrcode'
import dateFilter from '@/filters/date.filter'
import VueQRCodeComponent from 'vue-qr-generator';
import 'primeicons/primeicons.css';
import Copy from '@/components/Copy.vue';
import Logo from '@/components/icons/Logo.vue'
import Backspace from '@/components/icons/Backspace.vue'
import Delete from '@/components/icons/Delete.vue'
import ArrowRight from '@/components/icons/ArrowRight.vue'
import Unlock from '@/components/icons/Unlock.vue'
import Logout from '@/components/icons/Logout.vue'
import Arrow from "@/components/icons/ArrowPagination.vue";
import SuccessIcon from "@/components/icons/SuccessIcon.vue";
import {checkCryptoForImg} from "@/plugins/checkCryptoForImg";

router.beforeEach(async (to, from, next) => {
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    const token = localStorage['access-token'];
    if (requiresAuth && (!token || token === 'undefined')) {
        delete localStorage['access-token']
        delete localStorage['refresh-token']
        delete localStorage['login']
        next('/auth');
    } else {
        next();
    }
})

const app = createApp(App)

app.config.globalProperties.$filters = {
    dateFilter: dateFilter,
    checkCryptoForImg: checkCryptoForImg
}
app
    .component('Logo', Logo)
    .component('Logout', Logout)
    .component('Backspace', Backspace)
    .component('Delete', Delete)
    .component('ArrowRight', ArrowRight)
    .component('Unlock', Unlock)
    .component('Arrow', Arrow)
    .component('SuccessIcon', SuccessIcon)
    .component('vue-qrcode', VueQrcode)
    .component('qr-code', VueQRCodeComponent)
    .component('w-copy', Copy)
    .use(store)
    .use(router)
    .mount('#app')
