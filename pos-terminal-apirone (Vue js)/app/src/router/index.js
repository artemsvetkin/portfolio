import {createRouter, createWebHistory} from 'vue-router'

import Home from '@/pages/home'
import Auth from '@/pages/auth'

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/auth/',
        name: 'auth',
        component: Auth
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
