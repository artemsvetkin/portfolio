const { defineConfig } = require('@vue/cli-service')
const publicPath = process.env.VUE_APP_PUBLIC_PATH === '/' ? '/' : `/${process.env.VUE_APP_PUBLIC_PATH}`;

module.exports = defineConfig({
  publicPath,
  transpileDependencies: true
})
