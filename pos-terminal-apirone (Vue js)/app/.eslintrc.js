module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: '@babel/eslint-parser',
    requireConfigFile: false
  },
  extends: [
    'eslint:recommended',
    'plugin:vue/vue3-recommended',
  ],
  rules: {
    'vue/no-unused-components': 'off',
    'vue/component-definition-name-casing': 'off',
    'vue/no-v-html': 'off',
    'vue/html-self-closing': 'off',
    'vue/no-v-text-v-html-on-component': 'off',
    'vue/multi-word-component-names': 'off',
    'vue/no-deprecated-filter': 'off',
    'vue/no-reserved-component-names': 'off',
    'no-tabs': 'off',
    'new-cap': 'off',
    'space-before-function-paren': 'off',
    'object-curly-spacing': 'off',
    'semi': 'off',
    'vue/require-default-prop': 'off',
    'vue/require-prop-types': 'off'
  }
}
