/**
 * Handles the scroll event and applies the paroller effect based on the window width.
 */
$(document).ready(function () {
  const header = $('#header');
  const blueSection = $('.integrations');
  const integrationsList = $('.integrations__list');
  const integrationsPhone = $('.integrations__phone');
  let parollerApplied = false;
  let paroller;

  /**
   * Event handler for the window scroll event.
   */
  $(window).on('scroll', function () {
    const scrollTop = $(window).scrollTop();
    const headerHeight = header.outerHeight();

    if ($(window).width() > 600) {
      handleScroll(blueSection.offset().top, integrationsPhone.offset().top, integrationsPhone.outerHeight() / 3);
    } else {
      handleScroll(integrationsList.offset().top - headerHeight, integrationsPhone.offset().top, integrationsPhone.outerHeight() - 200);
    }
  });

  /**
   * Handles the scroll position and applies or removes the paroller effect accordingly.
   * @param {number} start - The starting position for applying the effect.
   * @param {number} end - The ending position for applying the effect.
   * @param {number} offset - The offset to adjust the ending position.
   */
  function handleScroll(start, end, offset) {
    if (isInView(start, end)) {
      if (!parollerApplied) {
        paroller = $('.my-paroller').paroller();
        parollerApplied = true;
      }
    } else {
      paroller?.destroy();
      parollerApplied = false;
      header.removeClass('header__blue-bg');
    }
  }

  /**
   * Checks if the scroll position is within the specified range.
   * @param {number} start - The starting position of the range.
   * @param {number} end - The ending position of the range.
   * @returns {boolean} - True if the scroll position is within the range, false otherwise.
   */
  function isInView(start, end) {
    const scrollTop = $(window).scrollTop();
    return scrollTop > start && scrollTop < end;
  }
});
