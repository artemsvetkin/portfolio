function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function hideRandomItem() {
  const container = document.querySelector('.digital-process__list');
  const items = container.querySelectorAll('.item');

  const randomIndex = getRandomInt(items.length);
  const randomItem = items[randomIndex];

  randomItem.classList.add('hide');

  setTimeout(() => {
    randomItem.classList.remove('hide');
  }, 2000);
}

function startAnimation() {
  setInterval(hideRandomItem, 2000);
}

startAnimation();
