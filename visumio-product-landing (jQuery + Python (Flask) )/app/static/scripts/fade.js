export function fade(element, duration, direction) {
  let startOpacity, endOpacity;

  if (direction === 'in') {
    startOpacity = 0;
    endOpacity = 1;
    element.style.display = 'block';
  } else if (direction === 'out') {
    startOpacity = 1;
    endOpacity = 0;
  }

  let startTime = null;

  function animate(currentTime) {
    if (!startTime) {
      startTime = currentTime;
    }

    const elapsedTime = currentTime - startTime;
    let progress = elapsedTime / duration;

    if (progress > 1) {
      progress = 1;
    }

    element.style.opacity = startOpacity + (endOpacity - startOpacity) * progress;

    if (progress < 1) {
      requestAnimationFrame(animate);
    } else if (direction === 'out') {
      element.style.display = 'none';
    }
  }

  requestAnimationFrame(animate);
};