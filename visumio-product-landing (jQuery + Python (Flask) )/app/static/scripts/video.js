import { fade } from '/static/scripts/fade.js'

const closeVideo = document.getElementById('closeVideo');
const modalVideo = document.getElementById('modalVideo');
const openVideo = document.getElementById('openVideo');
const maskVideo = document.getElementById('maskVideo');
const video = document.getElementById('video');


function toggleModal(direction) {
  fade(modalVideo, 300, direction)
}

openVideo.addEventListener('click', event => {
  toggleModal('in')
  video.play();
})
maskVideo.addEventListener('click', event => {
  toggleModal('out')
  video.pause()
})
closeVideo.addEventListener('click', event => {
  toggleModal('out')
  video.pause()
})
