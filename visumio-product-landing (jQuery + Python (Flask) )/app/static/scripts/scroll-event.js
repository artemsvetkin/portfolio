/**
 * Applies scroll-related styles to the page elements.
 */
$(document).ready(function () {
  const header = $('#header');
  const blueSection = $('.integrations');

  /**
   * Event handler for the window scroll event.
   */
  $(window).on('scroll', function () {
    const blueSectionTop = blueSection.offset().top;
    const blueSectionBottom = blueSectionTop + blueSection.outerHeight();
    const headerHeight = header.outerHeight();
    const scrollTop = $(window).scrollTop();

    if (isInView(blueSectionTop - headerHeight, blueSectionBottom, scrollTop)) {
      handleInView();
    } else {
      handleOutOfView();
    }
  });

  /**
   * Checks if the scroll position is within the specified range.
   * @param {number} start - The starting position of the range.
   * @param {number} end - The ending position of the range.
   * @param {number} scrollTop - The current scroll position.
   * @returns {boolean} - True if the scroll position is within the range, false otherwise.
   */
  function isInView(start, end, scrollTop) {
    return scrollTop > start && scrollTop < end;
  }

  /**
   * Handles styles when the blue section is in view.
   */
  function handleInView() {
    $('body').addClass('body__blue');
    $('.integrations').addClass('integrations__hide-after');
    $('.digital-process').addClass('digital-process__blue');
    header.addClass('header__blue-bg');
  }

  /**
   * Handles styles when the blue section is out of view.
   */
  function handleOutOfView() {
    $('body').removeClass('body__blue');
    $('.integrations').removeClass('integrations__hide-after');
    $('.digital-process').removeClass('digital-process__blue');
    header.removeClass('header__blue-bg');
  }
});
