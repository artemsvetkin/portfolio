from flask import Flask, render_template


app = Flask(__name__, template_folder='templates', static_folder='static')


if __name__ == '__main__':
    app.run()


@app.route('/')
def index():
    return render_template('pages/index.html')

@app.route('/data-protection/')
def data_protection():
    return render_template('pages/data_protection.html')

@app.route('/imprint/')
def imprint():
    return render_template('pages/imprint.html')

@app.route('/agb/')
def agb():
    return render_template('pages/agb.html')