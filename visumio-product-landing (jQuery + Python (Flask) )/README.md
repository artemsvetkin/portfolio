# Visum.IO Product landing

## Quickstart

### Зависимости

- Python 3.9

### Развертывание на локалке

- Создайте виртуальное окружение

```shell
python3.9 -m venv venv
```

- Установите зависимости

```shell
pip install -r requirements.txt
```

- Откройте проект в PyCharm
- Зайдите в Preferences -> Project -> Project Structure и установите директорию 
  **app/** как Sources (иконка с синей папкой).
- Запустите приложение командой:
```shell
flask run
```
