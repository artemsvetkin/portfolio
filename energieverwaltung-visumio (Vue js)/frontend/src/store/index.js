import Vue from 'vue'
import Vuex from 'vuex'
import user from "@/store/user";
import {isArray} from "lodash";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    counterIdWhenShowPopup: null,
    isSidebar: true,
    staticUrl: process.env.VUE_APP_STATIC_URL,
    telegramMessages: []
  },
  getters: {
  },
  mutations: {
    setCounterId (state, data) {
      state.counterIdWhenShowPopup = data
    },
    setSidebar(state, data) {
      state.isSidebar = data
    },
    setTelegramMessages(state, data) {
      if (isArray(data)) {
        state.telegramMessages = data
      } else {
        state.telegramMessages.push(data)
      }
    }
  },
  actions: {
  },
  modules: {
    user
  },
})
