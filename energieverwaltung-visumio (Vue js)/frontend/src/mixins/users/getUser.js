export default {
    data() {
        return {
            user: null
        }
    },
    created() {
        this.user = this.$store.getters["user/getUser"]
    }
}