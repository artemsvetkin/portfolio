import http from "$lib/http.js";

export async function checkStatusFinished(slot) {
  const pathname = window.location.pathname;
  const parts = pathname.split('/');
  const customerKey = parts[3];
  const responseSlot = await http.callMethod(`/customers/${customerKey}/slots/${slot}`)
  const resultSlot = responseSlot.result;
  if (resultSlot.session) {
    const responseSession = await http.callMethod(`/sessions/${resultSlot.session}`)
    resultSlot.session_info = responseSession.result
  }
  return resultSlot
}