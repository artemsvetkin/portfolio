export function _removeBase64Prefix(base64Data) {
  const parts = base64Data.split(',');
  if (parts.length > 1) {
    return parts[1];
  }
  return base64Data;
}