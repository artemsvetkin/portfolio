import moment from "moment";

export function _transformDateInHours(date) {
  return moment.utc(date).local().format('HH:mm');
}