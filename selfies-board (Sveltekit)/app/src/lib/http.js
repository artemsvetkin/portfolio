class Http {
  constructor() {
    this.url = import.meta.env.VITE_BACKEND_URL
  }

  async callMethod(pathUrl, options = {}) {
    const response = await fetch(this.url + pathUrl, options)
    const contentType = response.headers.get('Content-Type');

    if (contentType && contentType.includes('json')) {
      return {response, result: await response.json()};
    } else {
      return {response, result: await response.text()};
    }
  }

}

export default new Http()
