export function _checkStatusSlot(slot) {
  if (slot.is_available) {
    return 'Available'
  } else if (!slot.is_available && slot.is_reserved) {
    return 'Reserved'
  } else {
    return 'Unavailable'
  }
}