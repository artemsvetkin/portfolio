import moment from "moment";
export function _transformArraySlot(responseData, merge = true) {
  return responseData.map(deviceData => {
    if (merge) {
      let combinedSlots = [];

      Object.values(deviceData.slots).forEach(slotArray => {
        combinedSlots = combinedSlots.concat(slotArray);
      });

      const slotsGroupedByHour = groupSlotsByHour(combinedSlots);

      return {
        device: deviceData.device,
        slots: slotsGroupedByHour
      };
    } else {
      const slotsByCategoryGroupedByHour = Object.keys(deviceData.slots).reduce((acc, category) => {
        acc[category] = groupSlotsByHour(deviceData.slots[category]);
        return acc;
      }, {});

      return {
        device: deviceData.device,
        slots: slotsByCategoryGroupedByHour
      };
    }
  });
}

function groupSlotsByHour(slotsArray) {
  return slotsArray.reduce((acc, slot) => {
    const hour = moment.utc(slot.start).local().format('HH');
    const hourKey = `${hour}:00`;
    if (!acc[hourKey]) {
      acc[hourKey] = [];
    }
    acc[hourKey].push(slot);
    return acc;
  }, {});
}