export function _checkDeviceParams(slots) {
  const params = new URLSearchParams(window.location.search)
  if (params.has('device')) {
    return slots.find(item => item.device === params.get('device')).slots
  } else {
    return slots[0].slots
  }
}