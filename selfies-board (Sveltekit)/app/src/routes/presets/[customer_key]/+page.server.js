export async function load({fetch, params}) {
  const responsePresets = await fetch(`${process.env.VITE_BACKEND_URL}/customers/${params.customer_key}/presets`)
  const resultPresets = await responsePresets.json()
  return {
    presets: resultPresets
  }
}