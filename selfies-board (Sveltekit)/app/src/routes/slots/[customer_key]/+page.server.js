export async function load({fetch, params}) {
  const response = await fetch(`${process.env.VITE_BACKEND_URL}/customers/${params.customer_key}/settings`);
  const responseDevice = await fetch(`${process.env.VITE_BACKEND_URL}/customers/${params.customer_key}/devices`)
  return {
    settings: await response.json(),
    devices: await responseDevice.json()
  }
}