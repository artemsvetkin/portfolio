export async function load({fetch, params}) {
  const response = await fetch(`${process.env.VITE_BACKEND_URL}/customers/${params.customer_key}/settings`);
  return await response.json()
}