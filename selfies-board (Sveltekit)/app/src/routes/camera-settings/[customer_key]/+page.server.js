export async function load({fetch, params}) {
  const responseDevice = await fetch(`${process.env.VITE_BACKEND_URL}/customers/${params.customer_key}/camera_settings`)
  const result = await responseDevice.json()
  return {
    cameraSettings: result
  }
}