import Vue from 'vue'
// @ts-ignore
import { Services } from 'definitions/services.js';

declare module 'vue/types/vue' {
    interface Vue {
        $services: Services,
    }
}
