import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from '@/pages/login.vue'
import Home from '@/pages/home.vue'

import AdminLayout from '@/layouts/admin-layout'
import AdminUsers from '@/pages/admin/users'
import AdminRights from '@/pages/admin/rights'
import AdminModules from '@/pages/admin/modules'

import three_companyLayout from '@/layouts/three_company-layout'
import three_companyOrders from '@/pages/three_company/orders'
import three_companyOrder from '@/pages/three_company/order'
import three_companyPlatesCount from '@/pages/three_company/plates_count'
import three_companyTemplates from '@/pages/three_company/templates'
import three_companyPrices from '@/pages/three_company/prices'
import three_companyProductsNumbers from '@/pages/three_company/products_numbers'
import three_companySiteContent from '@/pages/three_company/site-content'
import three_companyCsv from '@/pages/three_company/csv'

import two_companyLayout from '@/layouts/two_company-layout'
import two_companyOrders from '@/pages/two_company/orders'
import two_companyOrder from '@/pages/two_company/order'
import two_companyTemplates from '@/pages/two_company/templates'
import two_companyPrices from '@/pages/two_company/prices'
import two_companyEvbSettings from '@/pages/two_company/evb_settings'
import two_companySiteContent from '@/pages/two_company/site-content'
import two_companyResellerStatistics from '@/pages/two_company/reseller_statistics'
import two_companyLogs from '@/pages/two_company/logs'
import two_companyAds from '@/pages/two_company/ads'
import two_companyUsers from '@/pages/two_company/users'
import two_companyResellers from '@/pages/two_company/resellers'


import one_companyLayout from '@/layouts/one_company-layout'
import one_companyOrders from '@/pages/one_company/orders'
import one_companyOrder from '@/pages/one_company/order'
import one_companyPrices from '@/pages/one_company/prices'
import one_companyTemplates from '@/pages/one_company/templates'
import one_companyDisabledDates from '@/pages/one_company/disabled_dates'
import one_companyResellers from '@/pages/one_company/resellers'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'login',
        component: Login
    },
    {
        path: '/home/',
        name: 'home',
        component: Home
    },
    {
        path: '/admin/',
        name: 'admin',
        component: AdminLayout,
        children: [
            { path: '', component: AdminUsers },
            { path: 'users', name: 'users', component: AdminUsers },
            { path: 'rights', name: 'rights', component: AdminRights },
            { path: 'modules', name: 'modules', component: AdminModules },
        ]
    },
    {
        path: '/three_company/',
        name: 'three_company',
        component: three_companyLayout,
        children: [
            { path: '', component: three_companyOrders },
            { path: 'orders', name: 'orders', component: three_companyOrders },
            { path: 'orders/:id', name: 'order', component: three_companyOrder },
            { path: 'plates_count', name: 'plates_count', component: three_companyPlatesCount },
            { path: 'templates', name: 'templates', component: three_companyTemplates },
            { path: 'prices', name: 'prices', component: three_companyPrices },
            { path: 'products_numbers', name: 'products_numbers', component: three_companyProductsNumbers },
            { path: 'site_content', name: 'site_content', component: three_companySiteContent },
            { path: 'csv', name: 'csv', component: three_companyCsv },
        ]
    },
    {
        path: '/two_company/',
        name: 'two_company',
        component: two_companyLayout,
        children: [
            { path: '', component: two_companyOrders },
            { path: 'orders', name: 'two_company_orders', component: two_companyOrders },
            { path: 'orders/:id', name: 'two_company_order', component: two_companyOrder },
            { path: 'templates', name: 'templates', component: two_companyTemplates },
            { path: 'prices', name: 'prices', component: two_companyPrices },
            { path: 'site_content', name: 'site_content', component: two_companySiteContent },
            { path: 'evb_settings', name: 'current_provider', component: two_companyEvbSettings },
            { path: 'reseller_statistics', name: 'reseller_statistics', component: two_companyResellerStatistics },
            { path: 'logs', name: 'logs', component: two_companyLogs },
            { path: 'ads', name: 'ads', component: two_companyAds },
            { path: 'users', name: 'users', component: two_companyUsers },
            { path: 'resellers', name: 'resellers', component: two_companyResellers },
        ],
    },
    {
        path: '/one_company/',
        name: 'one_company',
        component: one_companyLayout,
        children: [
            { path: '', component: one_companyOrders },
            { path: 'orders', name: 'one_company_orders', component: one_companyOrders },
            { path: 'orders/:id', name: 'one_company_order', component: one_companyOrder },
            { path: 'prices', name: 'one_company_prices', component: one_companyPrices },
            { path: 'emails_templates', name: 'one_company_templates', component: one_companyTemplates },
            { path: 'disabled_dates', name: 'one_company_disabled_dates', component: one_companyDisabledDates },
            { path: 'resellers', name: 'one_company_resellers', component: one_companyResellers },
        ],
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
