import Vue from "vue";

export const backendUrl = 'http://localhost:4000/call_method'

export async function pluginFactory(method, data = Object) {
    let body = undefined
    if (data) {
        body = JSON.stringify({method, data})
    }
    const headers = {'Content-Type': 'application/json'}

    const response = await fetch(backendUrl, {
        method: 'POST',
        headers,
        body
    })

    return await response.json()

}

export default {
    install(Vue) {
        Vue.prototype.$services = pluginFactory
    }
}