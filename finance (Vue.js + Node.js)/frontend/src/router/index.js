import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/pages/main_page.vue'
import store from '@/store/index'
import Login from '@/pages/auth/login'
import Registration from '@/pages/auth/registration'

import FinanceMainPage from '@/pages/finance (Vue.js + Node.js)/finance (Vue.js + Node.js)-main_page.vue'
import FinanceAnalysisPage from '@/pages/finance (Vue.js + Node.js)/finance (Vue.js + Node.js)-analysis_page'
import FinanceHistoryPage from '@/pages/finance (Vue.js + Node.js)/finance (Vue.js + Node.js)-history_page'
import FinancePlannedPage from '@/pages/finance (Vue.js + Node.js)/finance (Vue.js + Node.js)-planned_page'
import FinanceLayout from '@/layouts/finance (Vue.js + Node.js)-layout'
import FinanceAddOperation from '@/pages/finance (Vue.js + Node.js)/finance (Vue.js + Node.js)-add-operation_page'
import FinanceOperationPage from '@/pages/finance (Vue.js + Node.js)/finance (Vue.js + Node.js)-operation_page'

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/login/',
    name: 'login',
    component: Login
  },
  {
    path: '/registration/',
    name: 'registration',
    component: Registration
  },
  {
    path: '/finance (Vue.js + Node.js)/',
    name: 'finance',
    component: FinanceLayout,
    children: [
      {path: '', name: 'finance_main', component: FinanceMainPage},
      {path: 'analysis', name: 'finance_analysis', component: FinanceAnalysisPage},
      {path: 'history', name: 'finance_history', component: FinanceHistoryPage},
      {path: 'history/:id', name: 'finance_operation', component: FinanceOperationPage},
      {path: 'edit/:id', name: 'finance_edit', component: FinanceAddOperation},
      {path: 'planned', name: 'finance_planned', component: FinancePlannedPage},
      {path: 'add_operation', name: 'finance_add-operation', component: FinanceAddOperation},
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  if (!store.state.isAuth && to.name === 'finance_main') {
    console.log(to)
  }
  else next()
})
export default router
