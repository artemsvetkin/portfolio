export const FORM_ERROR_MESSAGE = {
    required: 'Данное поле является обязательным',
    email: 'Введите корреткный email',
    minLength(num = 0) { return `Поле должно содержать не менее ${num} символов` },
    maxLength(num = 0) { return `Поле должно содержать не более ${num} символов` },
    rangeLength(minNum = 0, maxNum = 0) { return `Поле должно содержать не менее ${minNum} и не более ${maxNum} символов` },
}