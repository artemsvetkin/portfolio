import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vuelidate from "vuelidate";

import dateFilter from "@/filters/date.filter";

import transport from "@/plugins/transport";

Vue.use(Vuelidate)

Vue.use(transport)

Vue.config.productionTip = false
Vue.filter('date', dateFilter)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
