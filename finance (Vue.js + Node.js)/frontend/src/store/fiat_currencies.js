export default {
    state: {
        fiatCurrencies: {},
        toCurrencies: ['USD', 'EUR', 'GEL', 'KZT'],
        isoListFiatCurrencies: {},
        baseFiatCurrency: 'RUB'
    },
    actions: {
        async requestFiatCurrencies(context) {
            try {
                const options = {method: 'GET', headers: {accept: 'application/json'}};
                const token = process.env.VUE_APP_FIAT_TOKEN
                const toCurrencies = context.state.toCurrencies.join(',')
                const baseFiatCurrency = context.state.baseFiatCurrency
                const response = await fetch(`https://api.fastforex.io/fetch-multi?from=${baseFiatCurrency}&to=${toCurrencies}&api_key=${token}`, options)
                const result = await response.json()
                context.commit('setFiatCurrencies', result)
            } catch (e) {
                console.error(e)
            }
        },
        async requestIsoCodeFiatCurrencies(context) {
            try {
                const options = {method: 'GET', headers: {accept: 'application/json'}};
                const token = process.env.VUE_APP_FIAT_TOKEN
                const response = await fetch(`https://api.fastforex.io/currencies?api_key=${token}`, options)
                let result = await response.json()
                result = Object.keys(result.currencies)
                context.commit('setIsoFiatCurrencies', result)
            } catch (e) {}
        }
    },
    mutations: {
        setFiatCurrencies(state, data) {
            state.fiatCurrencies = data.results
        },
        setBaseFiatCurrencies(state, data) {
            state.baseFiatCurrency = data.base || data
        },
        updateToCurrencies(state, data) {
            state.toCurrencies.push(data)
            return [...new Set(state.toCurrencies)]
        },
        setIsoFiatCurrencies(state, data) {
            state.isoListFiatCurrencies = data
        }
    },
    getters: {
        fiatCurrencies: state => state.fiatCurrencies,
        baseFiatCurrency: state => state.baseFiatCurrency,
        isoListFiatCurrencies: state => state.isoListFiatCurrencies
    },
    namespaced: true
}