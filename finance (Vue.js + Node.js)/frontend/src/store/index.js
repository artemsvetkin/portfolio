import Vue from 'vue'
import Vuex from 'vuex'
import fiat_currencies from "./fiat_currencies";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isAuth: true
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    fiat_currencies
  }
})
