export default {
    data() {
        return {
            sidebarLinks: {
                finance: [
                    {
                        nameLink: 'Общий счет',
                        nameRoute: 'finance_main',
                        path: '/finance (Vue.js + Node.js)/',
                        exact: true
                    },
                    {
                        nameLink: 'История',
                        nameRoute: 'finance_history',
                        path: '/finance (Vue.js + Node.js)/history/'
                    },
                    {
                        nameLink: 'Детальный анализ',
                        nameRoute: 'finance_analysis',
                        path: '/finance (Vue.js + Node.js)/analysis/'
                    },
                    {
                        nameLink: 'Планирование бюджета',
                        nameRoute: 'finance_planned',
                        path: '/finance (Vue.js + Node.js)/planned/'
                    },
                ],
                news: [
                    {
                        nameLink: 'NR',
                        nameRoute: '/news'
                    }
                ]
            }
        }
    }
}