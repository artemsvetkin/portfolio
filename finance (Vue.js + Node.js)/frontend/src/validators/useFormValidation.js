import { ref, computed } from 'vue'
import { validators } from './validators'

export function useFormValidation(validationSchema) {
  const formValues = ref({})
  const errors = ref({})
  const touched = ref({})

  function validateField(name) {
    const rules = validationSchema[name]
    const value = formValues.value[name]

    if (!rules) return true

    for (const rule of rules.split('|')) {
      const [ruleName, ruleValue] = rule.split(':')
      const validator = validators[ruleName]

      if (!validator) continue

      const isValid = validator(value, ruleValue)

      if (!isValid) {
        errors.value[name] = validator(value, ruleValue)
        return false
      }
    }

    errors.value[name] = ''
    return true
  }

  function validate() {
    let isValid = true

    for (const field in validationSchema) {
      const fieldIsValid = validateField(field)
      if (!fieldIsValid) isValid = false
    }

    return isValid
  }

  function getFieldProps(name) {
    return {
      name,
      value: formValues.value[name],
      onInput(event) {
        formValues.value[name] = event.target.value
        validateField(name)
      },
      onBlur() {
        touched.value[name] = true
      },
    }
  }

  function resetForm() {
    formValues.value = {}
    errors.value = {}
    touched.value = {}
  }

  const isFormValid = computed(() => {
    for (const field in validationSchema) {
      if (errors.value[field]) return false
    }
    return true
  })

  return {
    formValues,
    errors,
    touched,
    validateField,
    validate,
    getFieldProps,
    resetForm,
    isFormValid,
  }
}