export const validators = {
    required: (val) => !!val || 'Обязательно полн',
    email: (val) => {
        const emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/
        return emailRegex.test(val) || 'Введите корректный email'
    }
}