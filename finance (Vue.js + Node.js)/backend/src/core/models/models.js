const Sequelize = require('sequelize')
const sequelize = new Sequelize('finance (Vue.js + Node.js)-crm', 'artemsvetkin', 'postgres', {
    dialect: 'postgres',
    host: 'localhost'
})

const Users = sequelize.define('users', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    login: {
        type: Sequelize.STRING,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false
    }
})

const Orders = sequelize.define('order', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    typeOperation: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    category: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    source: {
        type: Sequelize.STRING,
        allowNull: false
    },
    operationDate: {
        type: Sequelize.DATE,
        allowNull: false
    },
    amount: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    currency: {
        type: Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: Sequelize.STRING,
        allowNull: false
    }
})

Users.hasMany(Orders)

sequelize.sync().then(result => {
  console.log('==================');
  console.log('It\'s okay. Complete BD! 👍👍👍');
  console.log('==================');
})
.catch(err=> console.log(err));

module.exports = {sequelize, Users, Orders}

// {force: true}