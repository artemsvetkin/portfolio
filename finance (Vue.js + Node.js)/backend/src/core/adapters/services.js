const models = require('../models/models')

const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')


function getUser(req, res) {
    if (req.id === null) return
    models.Users.findByPk(req.id)
        .then(user => {
            res.json(user)
        })
        .catch(err => {
            console.log(err)
        })
}

function createOrders(req, res) {
    models.Orders.create({
        typeOperation: req.typeOperation,
        category: req.category,
        source: req.source,
        operationDate: req.operationDate,
        amount: req.amount,
        currency: req.currency,
        description: req.description
    }).then(res => {})
        .catch(err => {
            console.log(err)})
}

function getOrders(req, res) {
    models.Orders.findAll({raw: true})
        .then(orders => {
            res.json(orders)
        })
        .catch(err => {
            console.log(err)})
}

function updateOrder(req, res) {
    models.Orders.update(req, {
        where: {
            id: req.id
        }
    })
}

function getOrder(req, res) {
    models.Orders.findByPk(req.id)
        .then(order => {
            res.json(order)
        })
}

function deleteOrder(req, res) {
    models.Orders.destroy({
        where: {
            id: req
        }
    })
}

function registration(req, res) {
    const salt = bcrypt.genSaltSync(10)
    req.password = bcrypt.hashSync(req.password, salt)
    models.Users.create({
        login: req.login,
        password: req.password,
        name: req.name,
        email: req.email
    })
}

async function authorizeUser(req, res) {
    try {
        const user = await models.Users.findOne({email: req.email})
        const token = jwt.sign(req, 'private_key')
        if (!user || !await bcrypt.compare(req.password, user.password)) {
            return res.status(401).json({ error: 'Invalid email or password' });
        }

        res.json({
            message: 'Login successful',
            token: token,
        });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal server error' });
    }
}

let methods = {}
methods.getUser = getUser
methods.createOrders = createOrders
methods.getOrders = getOrders
methods.getOrder = getOrder
methods.updateOrder = updateOrder
methods.deleteOrder = deleteOrder
methods.registration = registration
methods.authorizeUser = authorizeUser

module.exports = methods