const express = require('express')
const bodyParser = require('body-parser');

const services = require('./core/adapters/services')

const main = express();

main.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
  next();
});


main.use(bodyParser.json());

main.post('/call_method', function (req, res) {
    const method = req.body.method
    const data = req.body.data
    console.log(req.body)
    services[method](data, res)
})

main.listen(4000, function () {
    console.log('Server activate: http://localhost:4000/')
})