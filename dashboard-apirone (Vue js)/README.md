# dashboard

## Project setup
```
cd app
```
```
yarn
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for stage
```
yarn run build-stage
```

### Compiles and minifies for production
```
yarn run build
```
