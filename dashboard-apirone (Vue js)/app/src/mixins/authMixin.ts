import {jwtDecode} from 'jwt-decode';
import Axios from 'axios';
import store from '../store';

export default {
  methods: {
    async checkToken(id: string, serviceUrl: string) {
      if (localStorage[id]) {
        const token = JSON.parse(localStorage[id])['access-token'];
        const refreshToken = JSON.parse(localStorage[id])['refresh-token'];
        if (token && refreshToken && token !== '' && refreshToken !== '') {
          const decodedToken: any = jwtDecode(token);
          const decodedRefreshToken: any = jwtDecode(refreshToken);
          if (Date.now() < decodedToken.exp * 10 ** 3) {
            return true;
          }
          if (Date.now() < decodedRefreshToken.exp * 10 ** 3) {
            store.commit('setRefreshingToken', true);
            await Axios
              .post(`${serviceUrl}/api/v2/auth/refresh-token`, {}, {
                headers: {
                  Authorization: `Bearer ${refreshToken}`,
                },
              })
              .then((response) => {
                const newData = response.data;
                localStorage[newData.login] = JSON.stringify(newData);
              })
              /* .catch(() => {
                store.commit('logout');
              }) */
              .then(() => {
                store.commit('setRefreshingToken', false);
              });
            return true;
          }
        }
        return false;
      }
      return false;
    },
  },
};
