import getEnv from '@/utils/env';
import Logo from "@/components/Logo.vue";

export default {
  methods: {
    getImgSrc(currency: string): string {
      const prefixUrl = getEnv('VITE_PUBLIC_PATH');
      const prefix = prefixUrl === '/' ? '' : `/${prefixUrl}`;
      let src = '';
      const imgsListEnv = getEnv('VITE_CURRENCY_IMGS_LIST');
      if (imgsListEnv) {
        const imgsList = JSON.stringify(imgsListEnv).replace(/[\[\]"]/g, "").split(',');
        imgsList.forEach((imgsListItem: string) => {
          if (imgsListItem === currency) {
            src = `${prefix}/img/currencies/${currency}.svg`;
          }
        });
      }
      if (src === '') {
        src = `${prefix}/img/currencies/placeholder.svg`;
      }
      return src;
    },
  },
};
