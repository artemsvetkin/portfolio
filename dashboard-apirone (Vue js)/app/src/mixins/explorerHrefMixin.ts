export default {
  methods: {
    getExplorerHref(currency: string, type: string, value: string, abbr = ''): string {
      let explorer = 'blockchair.com';
      let currencyValue = currency;
      if (currency.includes('tron') || currency.includes('trc20')) {
        currencyValue = '#';
        if (currency.includes('testnet') || abbr.includes('ttrx')) {
          explorer = 'shasta.tronscan.org';
        } else {
          explorer = 'tronscan.org';
        }
      }
      const href = `https://${explorer}/${currencyValue}/${type}/${value}?from=apirone`;
      return href;
    },
  },
};
