export default {
  methods: {
    getTime(date: Date) {
      const hours = this.getFullNumber(date.getHours());
      const minutes = this.getFullNumber(date.getMinutes());
      const seconds = this.getFullNumber(date.getSeconds());
      return `${hours}:${minutes}:${seconds}`;
    },
    getFullNumber(number: number) {
      let string = number.toString();
      if (string.length === 1) {
        string = `0${string}`;
      }
      return string;
    },
  },
};
