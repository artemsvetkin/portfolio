export default {
  methods: {
    formatCurrency(currency: string): string {
      let string = currency;
      if (typeof currency === 'string') {
        string = currency.toUpperCase();
        if (string.includes('@')) {
          return string.split('@')[0];
        }
      }
      return string;
    },
  },
};
