import { createStore } from 'vuex';
import getEnv from '@/utils/env';

export default createStore({
  state: {
    serviceUrl: getEnv('VITE_SERVICE_URL'),
    prefixUrl: getEnv('VITE_PUBLIC_PATH'),
    showNonCustodial: getEnv('VITE_SHOW_NON_CUSTODIAL'),
    refreshFlag: false,
    shorterFlag: false,
    id: '',
    currency: null,
    authorized: false,
    refreshingToken: false,
    account: null,
    wallet: null,
    destinations: null,
    policy: null,
    pricing: {
      data: null,
      show: false,
    },
    balance: null,
    fiatItems: ['USD', 'EUR', 'RUB', 'GBP'],
    ticker: {
      current: null,
      list: [],
    },
    fee: {
      list: [],
      current: null,
    },
    showTestnet: false,
    idsListKey: null,
    watchOnly: false,
    paidAddresses: null,
  },
  mutations: {
    setShorterFlag(state, flag) {
      state.shorterFlag = flag;
    },
    setId(state, data) {
      state.id = data;
    },
    login(state) {
      state.authorized = true;
    },
    logout(state) {
      state.authorized = false;
      if (state.id !== '') {
        Object
          .keys(localStorage).filter((x) => x.includes(state.id))
          .forEach((x) => {
            if (!x.includes('ticker-')) localStorage.removeItem(x);
          });
      }
    },
    setRefreshingToken(state, flag) {
      state.refreshingToken = flag;
    },
    setAccount(state, data) {
      state.account = data;
    },
    setWallet(state, data) {
      state.wallet = data;
    },
    setDestinations(state, data) {
      state.destinations = data;
    },
    setPolicy(state, data) {
      state.policy = data;
    },
    setCurrency(state, data) {
      state.currency = data;
    },
    setBalance(state, data) {
      state.balance = data;
    },
    setCurrentTicker(state, data) {
      state.ticker.current = data;
    },
    setTickerList(state, list) {
      state.ticker.list = list;
    },
    setFeeList(state, list) {
      state.fee.list = list;
    },
    setCurrentFee(state, current) {
      state.fee.current = current;
    },
    setShowTestnet(state, flag) {
      state.showTestnet = flag;
    },
    updateIdsListKey(state, key) {
      state.idsListKey = key;
    },
    setWatchOnly(state, flag) {
      state.watchOnly = flag;
    },
    setPaidAddresses(state, addresses) {
      state.paidAddresses = addresses;
    },
    setPricing(state, data) {
      state.pricing.data = data;
    },
  },
  actions: {
  },
  modules: {
  },
});
