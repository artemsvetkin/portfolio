declare global {
  interface Window {
    configs: any;
  }
}
export default function getEnv(name: string) {
  return import.meta.env[name] || window?.configs?.[name];
}
