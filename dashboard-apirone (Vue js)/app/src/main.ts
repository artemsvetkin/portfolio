/// <reference types="vite/client" />

import {createApp} from 'vue';
import axios from 'axios';
import PrimeVue from 'primevue/config';
import Dropdown from 'primevue/dropdown';
import Avatar from 'primevue/avatar';
import Menu from 'primevue/menu';
import Badge from 'primevue/badge';
import BadgeDirective from 'primevue/badgedirective';
import Dialog from 'primevue/dialog';
import Button from 'primevue/button';
import TabMenu from 'primevue/tabmenu';
import PanelMenu from 'primevue/panelmenu';
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';
import Calendar from 'primevue/calendar';
import InputText from 'primevue/inputtext';
import InputNumber from 'primevue/inputnumber';
import Paginator from 'primevue/paginator';
import Password from 'primevue/password';
import ToastService from 'primevue/toastservice';
import Toast from 'primevue/toast';
import Skeleton from 'primevue/skeleton';
import Steps from 'primevue/steps';
import InputSwitch from 'primevue/inputswitch';
import QRCode from 'qrcode.vue';
import Checkbox from 'primevue/checkbox';
import Tooltip from 'primevue/tooltip';
import SplitButton from 'primevue/splitbutton';
import Accordion from 'primevue/accordion';
import AccordionTab from 'primevue/accordiontab';
import AutoComplete from 'primevue/autocomplete';
import RadioButton from 'primevue/radiobutton';
import VueGtag from 'vue-gtag';
import {initYandexMetrika} from 'yandex-metrika-vue3';
import App from './App.vue';
import router from './router';
import store from './store';
import authMixin from './mixins/authMixin';
import imgSrcMixin from './mixins/imgSrcMixin';
import timeMixin from './mixins/timeMixin';
import localeDateStringMixin from './mixins/localeDateStringMixin';
import currencyFormatMixin from './mixins/currencyFormatMixin';
import explorerHrefMixin from './mixins/explorerHrefMixin';

import './assets/_theme.scss';
import 'primevue/resources/primevue.min.css';
import 'primeicons/primeicons.css';


const app = createApp(App);

app.mixin(authMixin);
app.mixin(imgSrcMixin);
app.mixin(timeMixin);
app.mixin(localeDateStringMixin);
app.mixin(currencyFormatMixin);
app.mixin(explorerHrefMixin);

app.config.globalProperties.$axios = axios;
axios.defaults.baseURL = store.state.serviceUrl;
axios.interceptors.request.use(async (config) => {
  if (config.url?.slice(-13) !== 'refresh-token' && !store.state.refreshingToken && store.state.id !== '') {
    await authMixin.methods.checkToken(store.state.id, store.state.serviceUrl)
      .then((authorized) => {
        if (authorized) {
          store.commit('login');
        } else {
          store.commit('logout');
        }
      })
      .catch(() => {
        store.commit('logout');
      });
    if (config.url?.slice(-8) !== 'transfer' && store.state.authorized && !config?.headers?.Authorization) {
      Object.assign(config, {
        headers: {
          Authorization: `Bearer ${JSON.parse(localStorage[store.state.id])['access-token']}`,
        },
      });
    }
  }
  return config;
});
axios.interceptors.response.use((response) => response, (error) => {
  const message = error?.response?.data?.message;
  const messageType = typeof message;
  if (messageType === 'string' && (message.includes('Invalid token') || message.includes('Authorization required'))) {
    store.commit('logout');
  }
  return Promise.reject(error);
});

app.use(PrimeVue);
app.component('Dropdown', Dropdown);
app.component('Avatar', Avatar);
app.component('Menu', Menu);
app.component('Badge', Badge);
app.directive('badge', BadgeDirective);
app.component('Dialog', Dialog);
app.component('Button', Button);
app.component('TabMenu', TabMenu);
app.component('PanelMenu', PanelMenu);
app.component('DataTable', DataTable);
app.component('Column', Column);
app.component('Calendar', Calendar);
app.component('InputText', InputText);
app.component('InputNumber', InputNumber);
app.component('Paginator', Paginator);
app.component('Password', Password);
app.component('Toast', Toast);
app.component('Skeleton', Skeleton);
app.component('Steps', Steps);
app.component('InputSwitch', InputSwitch);
app.component('QRCode', QRCode);
app.component('Checkbox', Checkbox);
app.component('SplitButton', SplitButton);
app.component('Accordion', Accordion);
app.component('AccordionTab', AccordionTab);
app.component('AutoComplete', AutoComplete);
app.component('RadioButton', RadioButton);
app.use(ToastService);
app.directive('tooltip', Tooltip);

app.use(store);
app.use(router);

if (import.meta.env.VITE_ENVIROMENT === 'production') {
  app.use(VueGtag, {
    appName: 'dashboard-apirone (Vue js)',
    pageTrackerScreenviewEnabled: true,
    config: {
      id: import.meta.env.VITE_GTM || 'development',
    },
  }, router);

  app.use(initYandexMetrika, {
    id: import.meta.env.VITE_YANDEX_METRIKA_ID || 'development',
    router,
    env: import.meta.env.MODE,
    clickmap: true,
    trackLinks: true,
    accurateTrackBounce: true,
    webvisor: true,
    ecommerce: 'dataLayer',
  })
}
console.warn(`[ENVIRONMENT] - ${import.meta.env.MODE}`);

app.mount('#app');
