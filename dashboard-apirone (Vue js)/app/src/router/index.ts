import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import Home from '../views/Home.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: { title: 'Apirone Dashboard - Home' },
  },
  {
    path: '/create',
    name: 'Create',
    component: () => import('@/views/Create.vue'),
    meta: { title: 'Apirone Dashboard - Create' },
  },
  {
    path: '/account/:accountId',
    component: () => import('@/views/account/Account.vue'),
    children: [
      {
        path: '',
        name: 'Account Wallets',
        component: () => import('@/views/account/Wallets.vue'),
        meta: { title: 'Apirone Account - Wallets' },
      },
      {
        path: 'history',
        name: 'Account History',
        component: () => import('@/views/account/History.vue'),
        meta: { title: 'Apirone Account - History' },
      },
      {
        path: 'invoices',
        name: 'Account Invoices',
        component: () => import('@/views/account/Invoices.vue'),
        meta: { title: 'Apirone Account - Invoices' },
      },
    ],
  },
  {
    path: '/account/:accountId/:currency',
    component: () => import('@/views/account/wallet/Wallet.vue'),
    children: [
      {
        path: 'history',
        alias: '',
        name: 'Account Wallet History',
        component: () => import('@/views/account/wallet/History.vue'),
        meta: { title: 'Apirone Account Wallet - History' },
      },
      {
        path: 'history/payments/:historyItemId',
        name: 'Account Wallet Payment',
        component: () => import('@/views/account/wallet/Payment.vue'),
        meta: { title: 'Apirone Account Wallet - Payment' },
      },
      {
        path: 'history/receipts/:historyItemId',
        name: 'Account Wallet Receipt',
        component: () => import('@/views/account/wallet/Receipt.vue'),
        meta: { title: 'Apirone Account Wallet - Receipt' },
      },
      {
        path: 'transfer',
        name: 'Account Wallet Transfer',
        component: () => import('@/views/account/wallet/Transfer.vue'),
        meta: { title: 'Apirone Account Wallet - Transfer' },
      },
      {
        path: 'addresses',
        name: 'Account Wallet Addresses',
        component: () => import('@/views/account/wallet/Addresses.vue'),
        meta: { title: 'Apirone Account Wallet - Addresses' },
      },
      {
        path: 'addresses/:addressId',
        name: 'Account Wallet Address',
        component: () => import('@/views/account/wallet/Address.vue'),
        meta: { title: 'Apirone Account Wallet - Address' },
      },
      {
        path: 'addresses/:addressId/callback-log',
        name: 'Account Wallet Callback log',
        component: () => import('@/views/account/wallet/CallbackLog.vue'),
        meta: { title: 'Apirone Account Wallet - Callback log' },
      },
      {
        path: 'settings',
        name: 'Account Wallet Settings',
        component: () => import('@/views/account/wallet/Settings.vue'),
        meta: { title: 'Apirone Account Wallet - Settings' },
      },
    ],
  },
  {
    path: '/wallet/:walletId',
    component: () => import('@/views/wallet/Wallet.vue'),
    children: [
      {
        path: 'history',
        alias: '',
        name: 'Wallet History',
        component: () => import('@/views/wallet/History.vue'),
        meta: { title: 'Apirone Wallet - History' },
      },
      {
        path: 'history/payments/:historyItemId',
        name: 'Wallet Payment',
        component: () => import('@/views/wallet/Payment.vue'),
        meta: { title: 'Apirone Wallet - Payment' },
      },
      {
        path: 'history/receipts/:historyItemId',
        name: 'Wallet Receipt',
        component: () => import('@/views/wallet/Receipt.vue'),
        meta: { title: 'Apirone Wallet - Receipt' },
      },
      {
        path: 'transfer',
        name: 'Wallet Transfer',
        component: () => import('@/views/wallet/Transfer.vue'),
        meta: { title: 'Apirone Wallet - Transfer' },
      },
      {
        path: 'addresses',
        name: 'Wallet Addresses',
        component: () => import('@/views/wallet/Addresses.vue'),
        meta: { title: 'Apirone Wallet - Addresses' },
      },
      {
        path: 'addresses/:addressId',
        name: 'Wallet Address',
        component: () => import('@/views/wallet/Address.vue'),
        meta: { title: 'Apirone Wallet - Address' },
      },
      {
        path: 'addresses/:addressId/callback-log',
        name: 'Wallet Callback log',
        component: () => import('@/views/wallet/CallbackLog.vue'),
        meta: { title: 'Apirone Wallet - Callback log' },
      },
      {
        path: 'settings',
        name: 'Wallet Settings',
        component: () => import('@/views/wallet/Settings.vue'),
        meta: { title: 'Apirone Wallet - Settings' },
      },
    ],
  },
  {
    path: '/:catchAll(.*)',
    name: '404',
    component: () => import('@/views/404.vue'),
    meta: { title: 'Apirone Dashboard - 404' },
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

export default router;
