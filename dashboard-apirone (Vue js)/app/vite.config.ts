import {defineConfig, loadEnv, ConfigEnv} from 'vite';
import vue from '@vitejs/plugin-vue';
import path from 'path';
import fs from 'fs';
import {VitePWA} from 'vite-plugin-pwa';
import * as process from "process";

const pkg = require('./package.json') as { version: string };

export default ({mode, command}: ConfigEnv) => {
  const env = loadEnv(mode, process.cwd());

  const currencyImgsList = fs.readdirSync('./public/img/currencies/')
    .map(file => file.slice(0, -4));

  const publicPath = env.VITE_PUBLIC_PATH === '/' ? '/' : `/${env.VITE_PUBLIC_PATH}/`;

  return defineConfig({
    base: publicPath,
    plugins: [
      vue(),
      VitePWA({
        includeAssets: ['favicon.ico'],
        registerType: 'autoUpdate',
        outDir: 'dist',
        manifest: {
          name: 'Apirone Dashboard',
          short_name: 'Dashboard',
          background_color: '#ffffff',
          theme_color: '#5d8ab9',
          display: 'standalone',
          start_url: publicPath,
          icons: [
            {
              "src": `${publicPath}img/icons/android-chrome-144x144.png`,
              "sizes": "144x144",
              "type": "image/png"
            },
            {
              "src": `${publicPath}/img/icons/android-chrome-192x192.png`,
              "sizes": "192x192",
              "type": "image/png"
            },
            {
              "src": `${publicPath}/img/icons/android-chrome-512x512.png`,
              "sizes": "512x512",
              "type": "image/png"
            }
          ],
        },
        workbox: {
          globDirectory: 'dist/',
          globPatterns: ['**/*.{js,css,html,png,jpg,jpeg,gif,svg,eot,ttf,woff,woff2}'],
          swDest: 'dist/sw.js'
        },
      })
    ],
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src'),
      },
    },
    define: {
      'import.meta.env.VITE_CURRENCY_IMGS_LIST': JSON.stringify(currencyImgsList),
      'import.meta.env.VITE_VERSION': JSON.stringify(pkg.version),
    },
    build: {
      chunkSizeWarningLimit: 1000,
      rollupOptions: {
        output: {
          manualChunks(id) {
            if (id.includes('node_modules')) {
              return id.toString().split('node_modules/')[1].split('/')[0].toString();
            }
          }
        }
      },
      cssCodeSplit: true
    }
  });
};

