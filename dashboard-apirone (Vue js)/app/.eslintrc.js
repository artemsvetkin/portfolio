module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  extends: [
    'plugin:vue/vue3-essential',
    'eslint:recommended',
    '@vue/typescript/recommended',
  ],
  rules: {
    'vue/no-unused-components': 'off',
    'vue/component-definition-name-casing': 'off',
    'vue/no-v-html': 'off',
    'vue/html-self-closing': 'off',
    'vue/no-v-text-v-html-on-component': 'off',
    'vue/multi-word-component-names': 'off',
    'vue/no-deprecated-filter': 'off',
    'vue/no-reserved-component-names': 'off',
    'no-tabs': 'off',
    'new-cap': 'off',
    'space-before-function-paren': 'off',
    'semi': 'off',
    'object-curly-spacing': 'off',
    'max-len': 'off',
    '@typescript-eslint/no-unused-vars': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/ban-ts-comment': 'off',
    '@typescript-eslint/no-var-requires': 'off'
  }
}
